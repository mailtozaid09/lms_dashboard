import {
    SET_IS_LOADING,
    SET_USER_ROLE,
  } from './actionsTypes';
  
export const setIsLoading = param => {
    return {
        type: SET_IS_LOADING,
        payload: param,
    };
};


export const setUserRole = param => {
    return {
        type: SET_USER_ROLE,
        payload: param,
    };
};

