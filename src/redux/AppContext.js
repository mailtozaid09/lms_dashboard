import React, { useReducer } from 'react'


const APP_CONTEXT = React.createContext({});

const initState = {
    isLoading : false,
    login : {
        name,
        typeOfuser : null,
        isError,
    }
}


const reducer = (state, {type, payload}) => {

    
    switch(type){
        case USER_LOGIN_REQUEST : {
            return  {...state, isLoading : true}
        }
        case USER_LOGIN_SUCCESS : {
            return  {...state, isLoading : false, login : payload}
        }
        case USER_LOGIN_FAIL : {
            return  {...state, isLoading : false, login : {...state.login, isError : payload}}
        }

    }
}

const AppContext = () => {

    const [state, dispatch] = useReducer(reducer, initState);

  return (
    <div>AppContext</div>
  )
}

export default AppContext