import {createStore} from 'redux'

import {
    SET_IS_LOADING,
    SET_USER_ROLE
} from './actionsTypes';

const initialState = {
    users: [],
    userRole: 'ADMIN',
    isLoading: false,
}




const reducer = (state = initialState, action) => {
    switch(action.type){
        case "LOGIN" : {
            return  {
                ...state, 
                users : action.payload
            }
        }

        case SET_IS_LOADING : {
            return  {
                ...state, 
                isLoading : action.payload
            }
        }

        case SET_USER_ROLE : {
            return  {
                ...state, 
                userRole : action.payload
            }
        }

        case "LOGOUT" : {
            return  {
                ...state, 
                users : null
            }
        }
        
        
        default: return state;
    }
}


export default createStore(reducer)

