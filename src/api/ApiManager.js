import { APP_URL } from './ApiConstants'
import commonFetch from './commanApi'

{/* APP CONTROLLER */}

export function userLogin (data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/app/signIn`,
            data: data,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}

export function userSignup (data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/app/signup`,
            data: data,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}





{/* USER CONTROLLER */}


// Add User
export function addNewUser (data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/user/addNewUser`,
            data: data,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}

// Get All User
export function getAllUsers (data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/user/getAllUsers`,
            data: data,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}

// Get User Details
export function getUserDetail (data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/user/getUserDetail`,
            data: data,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}

// Update User Details
export function updateUserDetail (data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/user/updateUserDetail`,
            data: data,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}





{/* TEST CONTROLLER */}

// Create New Test
export function createNewTest (data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/test/createTest`,
            data: data,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}

// Get My Test
export function getAllTests (data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/test/getMyTests`,
            data: data,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}

// Get My Users Test
export function getMyUsersTests (data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/test/getMyUsersTests`,
            data: data,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}

// Get Test Details
export function getTestDetails (id) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/test/getTestDetails/${id}`,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}

// Delete Test Details
export function deleteTest (id) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/test/deleteTest/${id}`,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}





{/* ASSIGNED TEST CONTROLLER */}

// Assign  Test
export function assignTest (data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/assignedtest/assignTest`,
            data: data,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}

// Get All Assigned  Test
export function getAllAssignedTests (data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/assignedtest/getAllAssignedTests`,
            data: data,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}

// View Assigned Test
export function viewAssignedTest (data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/assignedtest/viewTest`,
            data: data,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}

// Delete Assigned Test
export function deleteAssignedTest (data, id) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/assignedtest/deleteTest/${id}`,
            data: data,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}

// Submit Assigned Test Response
export function submitAssignedTestResponse (data) {
    return new Promise((success, failure) => {
        commonFetch({
            url: `${APP_URL}/assignedtest/submitResponse`,
            data: data,
            method: 'POST'
        })
        .then((res) => {
            res.json().then((result) => {
                success(result)
            })
        })
        .catch((err) => {
            failure(err)
        })
    })
}