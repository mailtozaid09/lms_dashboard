import { initializeApp } from 'firebase/app';
import { getFirestore, collection, getDocs } from 'firebase/firestore/lite';

const firebaseConfig = {
    apiKey: "AIzaSyCgDk8_1Ubtux2KnDdHq5OKqF1e64AQv8g",
    authDomain: "lmsdashboard-bc04b.firebaseapp.com",
    projectId: "lmsdashboard-bc04b",
    storageBucket: "lmsdashboard-bc04b.appspot.com",
    messagingSenderId: "462544445852",
    appId: "1:462544445852:web:670e314728a6d77da7a859",
    measurementId: "G-CNXNZK4BZR"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export default db