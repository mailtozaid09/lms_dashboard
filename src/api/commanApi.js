import React from 'react';

var auth_token = localStorage.getItem('auth_token');
console.log("auth_token => ", auth_token);

var dummy_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmVjYjViMjNhZTA0OTczM2YzNGZmZDMiLCJuYW1lIjoidmlrYXMiLCJlbWFpbCI6InZpa2FzMTk5QGdtYWlsLmNvbSIsIm1vYmlsZSI6Ijg5MDYxNzkyMzQiLCJyb2xlIjpbIkFETUlOIl0sImlhdCI6MTY1OTk0ODQ2NywiZXhwIjoxNjYyNTQwNDY3fQ.srRMmG_PwtSLnQGJgGExO4yFSWWnwlXItbSTq1BBk9k'

const commonFetch = (data) => {
  return fetch(`${data.url}`, {
    method: data.method,
    headers: {
      Accept: 'application/json',
      Authorization: dummy_token,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data.data),
  });
};
export default commonFetch;
