import React, {useState, useEffect} from 'react';

import '././App.css';

import {
    BrowserRouter,
    Routes,
    Route,
    Link,
    Navigate
} from "react-router-dom";



import Navigator from './screens/navigator/Navigator';
import { useSelector } from 'react-redux';

import {collection, getDoc, getDocs} from 'firebase/firestore'

import db from './api/Firebase';

const App = () => {

    console.log("db =>>> ", db);

    async function getschoolData(db) {
        const schoolCol = collection(db, 'school');
        const schoolSnapshot = await getDocs(schoolCol);
        const schoolList = schoolSnapshot.docs.map(doc => doc.data());
        return schoolList;
    }

    useEffect(() => {
        getschoolData()

    }, [])

    const [isLogin, setIsLogin] = useState(true);

    const users = useSelector(state=> state.users)

    const isLoading = useSelector(state=> state.isLoading)
    console.log("isLoading => ", isLoading);

    //users && users.length !=0 ? console.log("user present") : console.log("no userss");
    //console.log("users => ", users);


    return (
        // <Provider store={Store}>
        //     <Navigator />
        // </Provider> 
        <>
            {isLoading
            ?
            <div>LOADING</div>
            :
            <Navigator />
            }
           
        </>
    )
}

export default App  

