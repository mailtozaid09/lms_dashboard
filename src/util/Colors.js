export const Colors = {
    WHITE: '#fff',
    BLACK: '#000',
    RED: '#E02B1D',
    PRIMARY: '#7152EB',
    GREY: '#E9EBEF',
    YELLOW: '#E4D86F',
    GREEN: '#009262',
    MUSTURD: '#F6A429',
    SECONDARY: '#D9BBFF',
    DARKGREY: '#716E6E',
}