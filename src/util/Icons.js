export const icons = {
    online_class: require('../assets/icons/dashboard/online-class.png'),
    online_classA: require('../assets/icons/dashboard/online-classA.png'),

    profile: require('../assets/icons/dashboard/user_profile.png'),
    profileA: require('../assets/icons/dashboard/user_profileA.png'),

    attendance: require('../assets/icons/attendance.png'),
    attendanceA: require('../assets/icons/attendanceA.png'),

    user: require('../assets/icons/dashboard/user.png'),
    userA: require('../assets/icons/dashboard/userA.png'),

    test_details: require('../assets/icons/dashboard/testDetails.png'),
    test_detailsA: require('../assets/icons/dashboard/testDetailsA.png'),

    all_test: require('../assets/icons/dashboard/allTest.png'),
    all_testA: require('../assets/icons/dashboard/allTestA.png'),


    settings: require('../assets/icons/dashboard/settings.png'),
    settingsA: require('../assets/icons/dashboard/settingsA.png'),

    concept: require('../assets/icons/dashboard/concept.png'),
    conceptA: require('../assets/icons/dashboard/conceptA.png'),

    invoice: require('../assets/icons/dashboard/invoice.png'),
    invoiceA: require('../assets/icons/dashboard/invoiceA.png'),


    //profile user
    tutor_profile: require('../assets/icons/user/tutor_profile.png'),

    //socials

    email: require('../assets/icons/socials/email.png'),
    whatsapp: require('../assets/icons/socials/whatsapp.png'),
    camera: require('../assets/icons/socials/camera.png'),
    linkedin: require('../assets/icons/socials/linkedin.png'),


    //interest

    honest: require('../assets/icons/interest/honest.png'),
    confident: require('../assets/icons/interest/confident.png'),
    brave: require('../assets/icons/interest/brave.png'),
    yoga: require('../assets/icons/interest/yoga.png'),
    game: require('../assets/icons/interest/game.png'),
    cooking: require('../assets/icons/interest/cooking.png'),
    subject: require('../assets/icons/interest/subject.png'),
    biology: require('../assets/icons/interest/biology.png'),
    physics: require('../assets/icons/interest/physics.png'),



    //login
    studentActive: require('../assets/icons/login/studentActive.png'),
    parentActive: require('../assets/icons/login/parentActive.png'),
    studentInactive: require('../assets/icons/login/studentInactive.png'),
    parentInactive: require('../assets/icons/login/parentInactive.png'),


    //icons

    expert: require('../assets/icons/carousel/expert.png'),
    expertProfile: require('../assets/icons/carousel/expertProfile.png'),
    upload: require('../assets/icons/upload.png'),
    cancel_circle: require('../assets/icons/cancel.png'),
    plus_circle: require('../assets/icons/plus-circle.png'),
    document: require('../assets/icons/document.png'),
    
    left_arrow: require('../assets/icons/left-arrow.png'),
    left_arrow_grey: require('../assets/icons/left-arrow-grey.png'),


    right_arrow: require('../assets/icons/right-arrow.png'),
    right_arrow_purple: require('../assets/icons/right-arrow-purple.png'),
    
    edit: require('../assets/icons/edit.png'),
    edit_grey: require('../assets/icons/edit-grey.png'),
    edit_purple: require('../assets/icons/edit-purple.png'),
    
    plus_purple: require('../assets/icons/plus_purple.png'),

    attachment: require('../assets/icons/attachment.png'),
    download_grey: require('../assets/icons/download_grey.png'),

    school_logo: require('../assets/icons/Jai_Tulsi_Vidya_Vihar_logo.jpg'),


    class_bg: require('../assets/icons/class_bg.jpeg'),
    class_back: require('../assets/icons/class_back.png'),
    class_icon: require('../assets/icons/class_icon.png'),
    gform: require('../assets/icons/gform.png'),

};