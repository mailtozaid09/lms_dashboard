import moment from 'moment'

export const showDate = (date, format) => {
  return moment(date).format('DD/MM/YYYY')
}
