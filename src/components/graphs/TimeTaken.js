
import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { faker } from '@faker-js/faker';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);


const TimeTaken = () => {


const options = {
    responsive: true,
    plugins: {
      legend: {
        display: false
      },
      title: {
        display: true,
        text: 'Time Taken',
      },
      labels: {
        display: false
      }
    },
  };
  
const labels = ['Question Number'];
  
const data = {
  labels,
  datasets: [
    {
      label: 'Dataset 1',
      data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
      backgroundColor: '#8E76ED',
    },
    {
      label: 'Dataset 2',
      data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
      backgroundColor: '#8ADCFF',
    },
    {
        label: 'Dataset 1',
        data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
        backgroundColor: '#8E76ED',
    },
    {
    label: 'Dataset 2',
    data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
    backgroundColor: '#8ADCFF',
    },
    {
    label: 'Dataset 1',
    data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
    backgroundColor: '#8E76ED',
    },
    {
    label: 'Dataset 2',
    data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
    backgroundColor: '#8ADCFF',
    },
    {
    label: 'Dataset 1',
    data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
    backgroundColor: '#8E76ED',
    },
    {
    label: 'Dataset 2',
    data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
    backgroundColor: '#8ADCFF',
    },
    {
    label: 'Dataset 1',
    data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
    backgroundColor: '#8E76ED',
    },
    {
    label: 'Dataset 2',
    data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
    backgroundColor: '#8ADCFF',
    },
  ],
};

  return (
    <div style={{marginTop: 40, alignItems: 'center', backgroundColor: 'white', borderRadius: 20, padding: 20, paddingLeft: 40, paddingRight: 40, paddingBottom: 40, width: '70%'}} >
      <Bar options={options} data={data} />
    </div>
  )
}

export default TimeTaken
