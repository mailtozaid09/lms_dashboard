import React from 'react';
import {
  Chart as ChartJS,
  LinearScale,
  PointElement,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bubble } from 'react-chartjs-2';
import { faker } from '@faker-js/faker';

ChartJS.register(LinearScale, PointElement, Tooltip, Legend);

const ConceptAnalytics = (props) => {


const options = {
    responsive: true,
    plugins: {
      legend: {
        display: false
      },
      title: {
        display: true,
        text: 'Concept Analytics',
      },
      labels: {
        display: false
      }
    },
    scales: {
      y: {
        beginAtZero: true,
      },
    },
  };
  
const labels = ['Question Number'];
  
const data = {
  labels,
  datasets: [
    {
      label: 'Red dataset',
      data: [{
        x: 1,
        y: 10,
        r: 20
      }],
      backgroundColor: 'rgba(77, 51, 233, 0.6)',
    },
    {
      label: 'Red dataset',
      data: [{
        x: 2,
        y: 5,
        r: 50
      }],
      backgroundColor: 'rgba(255, 99, 132, 0.5)',
    },
    {
      label: 'Red dataset',
      data: [{
        x: 3,
        y: 7,
        r: 30
      }],
      backgroundColor: '#EBEF37',
    },
    {
      label: 'Red dataset',
      data: [{
        x: 4,
        y: 4,
        r: 21
      }],
      backgroundColor: 'rgba(218, 51, 233, 0.6)',
    },
    {
      label: 'Red dataset',
      data: [{
        x: 5,
        y: 9,
        r: 33
      }],
      backgroundColor: 'rgba(51, 233, 146, 0.6)',
    },
    {
      label: 'Red dataset',
      data: [{
        x: 6,
        y: 8,
        r: 36
      }],
      backgroundColor: 'rgba(51, 189, 233, 0.6)',
    },
    {
      label: 'Red dataset',
      data: [{
        x: 7,
        y: 1,
        r: 40
      }],
      backgroundColor: '#F1848A',
    },
  ],
};

  return (
    <div style={{marginTop: 40, alignItems: 'center', backgroundColor: 'white', borderRadius: 20, padding: 20, paddingLeft: 40, paddingRight: 40, paddingBottom: 40, width: props.graphWidth ? props.graphWidth : '70%'}} >
      <Bubble options={options} data={data} />
    </div>
  )
}

export default ConceptAnalytics

