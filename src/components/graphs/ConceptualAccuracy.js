import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { faker } from '@faker-js/faker';
import { Colors } from '../../util/Colors';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);


const ConceptualAccuracy = () => {

  
const labels = ['Algerba', 'Adv. Math', 'Geomtry', 'Trigonometry', 'Statistics', 'Probability', 'Calculus'];
  
const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'bottom',
      },
      title: {
        display: true,
        text: 'Conceptual Accuracy',
      },
      
    },
};
  
const data = {
    labels,
    datasets: [
        {
        label: 'Marks',
        data: labels.map(() => faker.datatype.number({ min: 0, max: 16 })),
        backgroundColor: Colors.PRIMARY,
        },
    ],
};
  return (
    <div style={{marginTop: 40, alignItems: 'center', backgroundColor: 'white', borderRadius: 20, padding: 20, paddingLeft: 40, paddingRight: 40, paddingBottom: 40, width: '70%'}} >
      <Bar options={options} data={data} />
    </div>
  )
}

export default ConceptualAccuracy
