import React,{useState} from 'react'
import './input.scss'

import show from '../../assets/showPassword.png';
import hide from '../../assets/showPassword.png';

const Input = (props) => {

    const { placeholder, onChange, type, icon } = props;

    const [showPass, setShowPass] = useState(false);

    const onChangeIcon =  () => {

    }


    return (    
        <div className='input-container'>
            <input
                placeholder={placeholder}
                onChange={onChange}
                className='input-style'
                security={true}
                type={icon ? !showPass ? 'password' : 'email' : 'email'}
            />
            {icon
            ?
            <div>
                {showPass
                ?
                <img className='password-icon' onClick={() => setShowPass(!showPass)} src={show} width="24" height="24" />
                :
                <img className='password-icon' onClick={() => setShowPass(!showPass)} src={show} width="24" height="24" />
                }
            </div>
            :
            null
            } 
            
        </div>
    )
}

export default Input