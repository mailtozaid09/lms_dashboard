import React from 'react'
import IconButton from '@mui/material/IconButton';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import SearchIcon from '@mui/icons-material/Search';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

const TextInput = (props) => {

    const {label, value, onChange, error, style, password, showPassword, handleClickShowPassword } = props;
    
    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    return (
        <div>
            <FormControl style={style ? style : {width: 250}} sx={{ m: 1,  }} variant="outlined">
            <InputLabel >{label}</InputLabel>
                {!password
                ?
                <OutlinedInput
                    type={'text'}
                    value={value}
                    onChange={onChange}
                    label={label}
                />
                :
                <OutlinedInput
                    id="outlined-adornment-password"
                    type={showPassword ? 'text' : 'password'}
                    label={label}
                    value={value}
                    onChange={onChange}
                    endAdornment={
                    <InputAdornment position="end">
                        <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={(event) => handleMouseDownPassword(event)}
                        edge="end"
                        >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                    </InputAdornment>
                    }
                /> 
                }

            </FormControl>
            {error ? <div className='error_style ml-10 mb-20' >{error}</div> : null}
        </div>
    )
}

export default TextInput