import React, {useState} from 'react';
import Box from '@mui/material/Box';

import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { Colors } from '../../../util/Colors';
import Button from '../../button/Button';

import  './addNewUserModal.scss'
import DropDown from '../../dropdown/DropDown';
import SecondaryButton from '../../button/SecondaryButton';
import SecondaryIconButton from '../../button/SecondaryIconButton';

import {FiUpload}from "react-icons/fi"
import { icons } from '../../../util/Icons';
import TextInput from '../../input/TextInput';
import { addNewUser } from '../../../api/ApiManager';

import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";


const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 650,
  bgcolor: 'background.paper',
  border: '0.5px solid #000',
  boxShadow: 10,
  p: 4,
};

export default function AddNewUserModal({handleClose, handleOpen}) {


    const [value, setValue] = React.useState('');
    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});
    const [buttonLoader, setButtonLoader] = useState(false);

    const handleChange = (event) => {
        console.log("event.target.value => " , event.target.value);
        setErrors({})
        setValue(event.target.value);
        var role = (event.target.value).toUpperCase()
        setForm({ ...form, ['role']: role});
        
    };

    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const notify = (text, type) => {
        if(type == 'error'){
            toast.error(text, {
                position: toast.POSITION.TOP_RIGHT,
                className: "toast",
            });
        }else if(type == 'success'){
            toast.success(text, {
                position: toast.POSITION.TOP_RIGHT,
                className: "toast",
            });
        }     
    };



    const addUserButton = (e) => {
        e.preventDefault();
        //setButtonLoader(true)
        console.log("-----SUBMIT----");

        var isEmailValid = false;
        var isNameValid = false;

        if (!form.password) {
            setButtonLoader(false)
            console.log("Please enter a valid password");

            setErrors((prev) => {
                return { ...prev, password: "Please enter a valid password" };
            });
        }

        if (!form.role) {
            setButtonLoader(false)
            console.log("Please enter a valid role");

            setErrors((prev) => {
                return { ...prev, role: "Please enter a valid role" };
            });
        }

        if (!form.email) {
            setButtonLoader(false)
            console.log("Please enter a valid email");

            setErrors((prev) => {
                return { ...prev, email: "Please enter a valid email" };
            });
            } else {
            // isEmailValid = validateEmail(form.email)
        }

        if (form.password && form.email && form.role) {
            console.log("-------- USER DETAILS --------");
            console.log(form);
            console.log(form.email);
            console.log(form.password);
            console.log(form.role);

            var data = {
                users: [
                    {
                        email: form.email,
                        role: form.role,
                    }
                ]
            }

            console.log("data => > ", data);

            addNewUser(data)
            .then((resp) => {
                console.log("addNewUser resp => ", resp);
                console.log("status => ", resp.status);
                if(resp.status == '200'){
                    handleClose()
                    notify(resp.message, 'success')
                }else{
                    notify(resp.message, 'error')
                }
                
                setButtonLoader(false)
            })
            .catch((err) => {
                console.log("err = ", err);
                setButtonLoader(false)
            })
        }
    };



    return (
        <div className='container'>
           
        <Modal
            open={true}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>

                <div  onClick={handleClose} className='close-icon' style={{position: 'absolute', right: 30, }} >
                    <img src={icons.cancel_circle} style={{height: 24, width: 24}} />
                </div>

                <Typography className='heading' id="modal-modal-title" variant="h6" component="h2">
                    <div className='heading-text' >Add a New User</div>
                </Typography>

                <div className='row-style' >
                    <div className='margin-right' >
                        <div className="input_label ml-10">Email</div>
                        <TextInput 
                            style={{ width: "100%" }} 
                            label="Email" 
                            value={form.email}
                            error={errors.email}
                            onChange={event => onChange({name: 'email', value: event.target.value})}
                        />
                    </div>
                    <div className='margin-left' >
                        <div className="input_label ml-10">Password</div>
                        <TextInput 
                            style={{ width: "100%" }} 
                            label="Password" 
                            value={form.password}
                            error={errors.password}
                            onChange={event => onChange({name: 'password', value: event.target.value})}
                        />
                    </div>
                </div>

                <div className='row-style ml-10' >
                    <div className='margin-right' >
                    <div className='label-style' >User Type</div>
                    <DropDown 
                        style={{width: 250}}
                        label="User Type"
                        error={errors.email}
                        handleChange={(val) => handleChange(val)}
                        data={[{value: 'Student', title: 'Student'}, {value: 'Parent', title: 'Parent'}]}
                    />
                     <div className='error_style mt-10' >{errors.role} </div>
                    </div>
                </div>
                
                <div  className='center-container' style={{marginTop: 20}} >
                    <div className='margin-right' >
                        <SecondaryButton 
                            title="Cancel"
                            onClick={handleClose}
                        />
                    </div>
                    <div className='margin-left' >
                        <Button
                            primary 
                            title="Add"
                            onClick={addUserButton}
                        />
                    </div>
                </div>
            </Box>
         </Modal>
         <ToastContainer
                hideProgressBar={true}
                newestOnTop={false}
                closeButton={false}
            />
        </div>
    );
}

