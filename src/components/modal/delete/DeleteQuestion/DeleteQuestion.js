import * as React from 'react';
import Box from '@mui/material/Box';

import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { Colors } from '../../../../util/Colors';
import Button from '../../../button/Button';

import  './deleteQuestion.scss'
import Dropdown from '../../../accordian/Dropdown';
import Input from '../../../textfield/Input';
import SecondaryButton from '../../../button/SecondaryButton';
import SecondaryIconButton from '../../../button/SecondaryIconButton';

import {FiUpload}from "react-icons/fi"
import { icons } from '../../../../util/Icons';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  bgcolor: 'background.paper',
  border: '0.5px solid #000',
  boxShadow: 10,
  p: 4,
};

export default function DeleteQuestion({handleDelete, handleClose, handleOpen}) {

    return (
        <div className='container'>
        <Modal
            open={true}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            
            <Box sx={style}>
                <div className='delete-container'>
                    <div style={{ width: 300, fontSize: 24, color: '#25335A', alignItems: 'center', justifyItems: 'center', textAlign: 'center'}} >Are you sure you want to delete the test ?</div>
                </div>
                
                <div  className='center-container' style={{marginTop: 20}} >
                    <div className='margin-right' >
                        <SecondaryButton 
                            title="Cancel"
                            onClick={handleClose}
                        />
                    </div>
                    <div className='margin-left' >
                        <Button
                            textStyle={{color: 'white'}}
                            containerStyle={{backgroundColor: '#CA3A2F', }} 
                            title="Delete Test"
                            onClick={handleDelete}
                        />
                    </div>
                </div> 
            </Box>
        </Modal>
        </div>
    );
}

