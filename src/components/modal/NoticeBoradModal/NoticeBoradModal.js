import * as React from 'react';
import Box from '@mui/material/Box';

import Modal from '@mui/material/Modal';


import Button from '../../button/Button';

import  './noticeBoradModal.scss'

import SecondaryButton from '../../button/SecondaryButton';

import { icons } from '../../../util/Icons';
import { Colors } from '../../../util/Colors';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  bgcolor: 'background.paper',
  border: '0.5px solid #000',
  boxShadow: 10,
  p: 4,
};


const noticeboard = [
    {
        id: 1,
        title: 'Holidays',
        subTitle: 'This Saturday School had to remain shut on 5 December 2022, due to heavy rainfail and as ordered by the centeral govenment',
    },
    {
        id: 1,
        title: 'Admissions',
        subTitle: 'Jai Tulsi Vidhya Vihar: Admission for e-classes (Grade 1-3) now open. Few seats left! Register your ward on jaiTulsiVidhyaVihar.edu/admissions23.', 
       
    },
    {
        id: 1,
        title: 'New classes/courses/opportunities' ,
        subTitle: 'Jai Tulsi Vidhya Vihar is starting classes for musical instruments this year 🎼. Reply GUITAR/VIOLIN/DRUMS to register. Hurry! Few seats left.'
    },
    {
        id: 1,
        title: 'Holidays',
        subTitle: 'Dear parent, Jai Tulsi Vidhya Vihar will remain CLOSED on 15 & 16 Dec 2020 due to heavy snowfall. School to resume with regular timings on 17 Dec. Regards, Admin office.'
    },
    {
        id: 1,
        title: 'Results',
        subTitle: 'Dear students, results for Class 6-8 out now! Check out your grades on jaiTulsiVidhyaVihar.edu/results. All the best'
    }
]

export default function NoticeBoardModal({handleClose, handleOpen, handleSubmit, }) {

    const d = new Date();
    let todaysDay = d.getDay();

    return (
        <div className='container'>
        <Modal
            open={true}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <div  onClick={handleClose} className='close-icon' style={{position: 'absolute', right: 30, }} >
                    <img src={icons.cancel_circle} style={{height: 24, width: 24}} />
                </div>

                <div style={{ alignItems: 'center', justifyContent: 'center', display: 'flex', flexDirection: 'column'}} >   
                    <div className='todays_title' >Notice Board</div>

                    <div>
                        {noticeboard.map((item) => (
                            <div style={{display: 'flex', flexDirection: 'row', marginBottom: 20}} >
                                <div style={{height: 40, width: 40, borderRadius: 20, background: Colors.PRIMARY, alignItems: 'center', justifyContent: 'center', display: 'flex',  marginRight: 20}} >
                                    <img src={icons.all_test} style={{width: 20, height: 20, }} />
                                </div>
                                
                                <div>
                                    <div style={{fontSize: 20, fontWeight: 'bold', width: 600}} >{item.title}</div>
                                    <div style={{fontSize: 16,  width: 600}} >{item.subTitle}</div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>

                {/* <div  className='center-container' style={{marginTop: 20}} >
                    <div className='margin-right' >
                        <SecondaryButton 
                            title="Cancel"
                            onClick={handleClose}
                        />
                    </div>
                    <div className='margin-left' >
                        <Button
                            primary 
                            title="Submit"
                            onClick={handleSubmit}
                        />
                    </div>
                </div> */}
            </Box>
        </Modal>
        </div>
    );
}

