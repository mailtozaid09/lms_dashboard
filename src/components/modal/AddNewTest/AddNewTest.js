import React, {useState} from 'react';
import Box from '@mui/material/Box';

import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { Colors } from '../../../util/Colors';
import Button from '../../button/Button';

import  './newTest.scss'
import SecondaryButton from '../../button/SecondaryButton';
import SecondaryIconButton from '../../button/SecondaryIconButton';

import {FiUpload}from "react-icons/fi"
import { icons } from '../../../util/Icons';
import DropDown from '../../dropdown/DropDown';
import TextInput from '../../input/TextInput';


import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";
import DatePicker from '../../dateTimePicker/DatePicker';
import IconButton from '../../button/iconButton/IconButton';
import FileUploadButton from '../../button/fileUploadButton/FileUploadButton';
import { createNewTest } from '../../../api/ApiManager';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 650,
  bgcolor: 'background.paper',
  border: '0.5px solid #000',
  boxShadow: 10,
  p: 4,
};

export default function AddNewTest({handleFetchDetails, handleClose, handleOpen}) {


    const [value, setValue] = React.useState('');
    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});
    const [buttonLoader, setButtonLoader] = useState(false);
    
    const [fileName, setFileName] = useState('');
    const [fileDetails, setFileDetails] = useState('');
    const [dueDate, setDueDate] = useState(new Date());

    const handleChange = (event) => {
        console.log("event.target.value => " , event.target.value);
        setErrors({})
        setValue(event.target.value);
        setForm({ ...form, ['testType']: event.target.value });
        
    };

    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const notify = (text, type) => {
        if(type == 'error'){
            toast.error(text, {
                position: toast.POSITION.TOP_RIGHT,
                className: "toast",
            });
        }else if(type == 'success'){
            toast.success(text, {
                position: toast.POSITION.TOP_RIGHT,
                className: "toast",
            });
        }     
    };

    const chooseFile = (file) => {
        setErrors({})
        console.log("file => ", file); 
        setFileName(file.name)

        
        setForm({ ...form, ['docs']: 
        [
            {
                name: file.name,
                url: file.name
            }
        ] });
    }



    const handleCreate = (e) => {
        e.preventDefault();
        //setButtonLoader(true)
        console.log("-----SUBMIT----");

        var isEmailValid = false;
        var isNameValid = false;

        console.log("form => >>  ", form);

        if (!form.testName) {
            setButtonLoader(false)
            console.log("Please enter a valid test name");

            setErrors((prev) => {
                return { ...prev, testName: "Please enter a valid test name" };
            });
        }

        if (!form.testType) {
            setButtonLoader(false)
            console.log("Please enter a valid test type");

            setErrors((prev) => {
                return { ...prev, testType: "Please enter a valid test type" };
            });
        }

        if (!form.docs) {
            setButtonLoader(false)
            console.log("Please upload a valid file");

            setErrors((prev) => {
                return { ...prev, docs: "Please upload a valid file" };
            });
        }

        if (!dueDate) {
            setButtonLoader(false)
            console.log("Please enter a valid dueDate");

            setErrors((prev) => {
                return { ...prev, email: "Please enter a valid due date" };
            });
            } else {
            // isEmailValid = validateEmail(form.email)
        }

        if (form.testName && form.testType && form.docs && dueDate) {
            console.log("-------- USER DETAILS --------");

            var data = {
                name: form.testName,
                type: form.testType,
                docs: form.docs,
                dueDate: dueDate
            }

            console.log("data => > ", data);

            createNewTest(data)
            .then((resp) => {
                console.log("createNewTest resp => ", resp);
                console.log("status => ", resp.status);
                if(resp.status == '200' || resp.status == '201'){
                    handleClose()
                    handleFetchDetails()
                    notify(resp.message, 'success')
                }else{
                    notify(resp.message, 'error')
                }
                
                setButtonLoader(false)
            })
            .catch((err) => {
                console.log("err = ", err);
                setButtonLoader(false)
            })
        }
    };



    return (
        <div className='container'>
        <Modal
            open={true}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <div  onClick={handleClose} className='close-icon' style={{position: 'absolute', right: 30, }} >
                    <img src={icons.cancel_circle} style={{height: 24, width: 24}} />
                </div>

                <Typography className='heading' id="modal-modal-title" variant="h6" component="h2">
                    <div className='heading-text' >Create a New Test</div>
                </Typography>

                <div className='row-style' >
                    <div className='margin-right' >
                        <div className="input_label ml-10">Test Name</div>
                        <TextInput 
                            style={{ width: "100%" }} 
                            label="Test Name" 
                            value={form.testName}
                            error={errors.testName}
                            onChange={event => onChange({name: 'testName', value: event.target.value})}
                        />
                    </div>
                    <div className='margin-left' >
                        <div className='label-style' >Test Type</div>
                        <DropDown 
                            style={{width: 250}}
                            label="Test Type"
                            error={errors.testType}
                            handleChange={(val) => handleChange(val)}
                            data={[{value: 'SAT', title: 'SAT'}, {value: 'SAT', title: 'SAT'}]}
                        />
                    </div>
                </div>

                <div className='row-style ml-10' >
                    <div className='margin-right' >
                        <div className='label-style mb-20' >Due Date</div>
                        <DatePicker
                            label="Due Date"
                            value={dueDate}
                            handleChange={(newValue) => {
                                setDueDate(newValue);
                            }}
                        />
                    </div>
                </div>

                <div className='row-style ml-10 ai-c d-f' >
                    <div className='row-style fd-c d-f mt-10' >
                        <div className='input_label bold' style={{width: 130}} >Upload the Test</div>
                        <div className='error_style mt-10' >{errors.docs} </div>
                    </div>
                    <div className='row-style ml-10' >

                        <FileUploadButton 
                            buttonLoader={buttonLoader}
                            icon={icons.upload} 
                            title="Upload PDF"
                            fileName={fileName}
                            onChooseFile={(event) => chooseFile(event.target.files[0])}
                        />

                        <FileUploadButton 
                            buttonLoader={buttonLoader}
                            icon={icons.upload} 
                            title="Upload CSV"
                            onChooseFile={(event) => chooseFile(event.target.files[0])}
                        />
                    </div>
                </div>

                <div  className='center-container' style={{marginTop: 20}} >
                    <div className='margin-right' >
                        <SecondaryButton 
                            title="Cancel"
                            onClick={handleClose}
                        />
                    </div>
                    <div className='margin-left' >
                        <Button
                            primary 
                            title="Create"
                            onClick={handleCreate}
                        />
                    </div>
                </div>
            </Box>
        </Modal>

        <ToastContainer
                hideProgressBar={true}
                newestOnTop={false}
                closeButton={false}
            />
        </div>
    );
}

