import * as React from 'react';
import Box from '@mui/material/Box';

import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import Button from '../../../button/Button';

import  './createSessionModal.scss'
import SecondaryButton from '../../../button/SecondaryButton';
import TextInput from '../../../input/TextInput';
import DropDown from '../../../dropdown/DropDown';
import CheckBox from '../../../checkbox/CheckBox';
import DateTimePicker from '../../../dateTimePicker/DateTimePicker';


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    height: 600,
    bgcolor: 'background.paper',
    border: '0.5px solid #000',
    boxShadow: 10,
    p: 4,
};


const topicCovered = [
    {
        id: 1,
        subject: 'Math',
    },
    {
        id: 1,
        subject: 'English',
    },
    {
        id: 1,
        subject: 'Reading',
    },
    {
        id: 1,
        subject: 'Science',
    },
]

const studentMood = [
    {
        id: 1,
        subject: 'Engaging',
    },
    {
        id: 1,
        subject: 'Chill',
    },
    {
        id: 1,
        subject: 'Inspiring',
    },
    {
        id: 1,
        subject: 'Quiet',
    },
    {
        id: 1,
        subject: 'Frustrated',
    },
    {
        id: 1,
        subject: 'Confused',
    },
]


const homeworkAssigned = [
    {
        id: 1,
        subject: 'Practise Test',
    },
    {
        id: 1,
        subject: 'Concept Review',
    },
    {
        id: 1,
        subject: 'English Section',
    },
    {
        id: 1,
        subject: 'Math Section',
    },
    {
        id: 1,
        subject: 'Science Section',
    },
    {
        id: 1,
        subject: 'Reading Section',
    },
    {
        id: 1,
        subject: 'All Sections',
    },
]

const sessionProductive = [
    {
        id: 1,
        title: 'Yes'
    },
    {
        id: 1,
        title: 'No'
    },
    {
        id: 1,
        title: 'Not Sure'
    },
]

const weekDays = [
    {
        id: 1,
        day: 'monday',
        value: 'M',
    },
    {
        id: 2,
        day: 'tuesday',
        value: 'T',
    },
    {
        id: 3,
        day: 'wednesday',
        value: 'W',
    },
    {
        id: 4,
        day: 'thursday',
        value: 'T',
    },
    {
        id: 5,
        day: 'friday',
        value: 'F',
    },
    {
        id: 6,
        day: 'saturaday',
        value: 'S',
    },
    {
        id: 7,
        day: 'sunday',
        value: 'S',
    },
]
export default function CreateSessionModal({handleClose, handleOpen}) {

    const [value, setValue] = React.useState(new Date());
    
      const handleChange = (newValue) => {
        setValue(newValue);
      };

    return (
        <div className='create_session_modal'>
        <Modal
            open={true}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <div  onClick={handleClose} className='close-icon' style={{position: 'absolute', right: 30, }} >
                    {/* <img src={icons.brave} style={{height: 24, width: 24}} /> */}
                </div>

                <Typography className='heading' id="modal-modal-title" variant="h6" component="h2">
                    <div className='heading-text' >Create a New Session</div>
                </Typography>

                <div className='create_session_content' >
                    <div className='fd-r jc-sb d-f full-width' >
                        <div>
                            <div className='input_label bold ml-10' >Student Name</div>
                            <TextInput style={{width: 300}} label="Student Name" />
                        </div>
                        <div>
                            <div className='input_label bold ml-10' >Tutor Name</div>
                            <TextInput style={{width: 300}} label="Tutor Name" />
                        </div>
                    </div>

                    <div className='d-f fd-r ai-c jc-sb  ml-10 mr-10 mt-20' >
                        <DateTimePicker 
                            label="Start Date"
                            dateValue="2017-05-24T10:30"
                        />
                        <DateTimePicker 
                            label="End Date"
                            dateValue="2017-05-24T10:30"
                        />
                    </div>

                    <div style={{marginLeft: 10}} >
                        <CheckBox label="Recurring" value="Recurring" />
                    </div>

                    <div className='fd-r jc-sb d-f full-width mt-10' >
                        <div>
                            <div className='input_label bold ml-10' >Repeat every week on</div>
                            <div style={{marginLeft: 10, marginTop: 10, flexDirection: 'row', flexWrap: 'wrap', display: 'flex', }} >
                                {weekDays.map((item) => (
                                    <div className='week_Days' style={{backgroundColor: '#F4F5F7' }} >
                                        <div>{item.value}</div>
                                    </div>
                                ))}
                            </div>
                        </div>

                        <div style={{marginTop: 20}} >
                            <DropDown
                                style={{width: 200}}
                                label="Time Zone"
                                defaultValue={'IST'}
                                data={[{value: 'IST', title: 'IST'}, {value: 'GMT', title: 'GMT'}]}
                            />
                        </div>
                    </div>

                    <div className='fd-r jc-sb d-f full-width ' >
                        <div>
                            <div className='input_label bold ml-10' >Session Link</div>
                            <TextInput style={{width: 420}} label="Session Link" />
                        </div>
                        <div style={{ }} >
                            <div className='input_label bold ml-10' >Session Status</div>
                            <DropDown
                                style={{width: 180, marginTop: 10}}
                                label="Session Status"
                                data={[{value: 'Completed', title: 'Completed'}, {value: 'InCompleted', title: 'InCompleted'}, {value: 'InProgress', title: 'InProgress'}]}
                            />
                        </div>
                    </div>

                    <div style={{marginTop: 0, marginLeft: 10}} >
                        <div className='input_label bold' >Service</div>
                        <DropDown
                            style={{width: 400, marginTop: 10}}
                            label="Service"
                            data={[{value: 'Completed', title: 'Completed'}, {value: 'InCompleted', title: 'InCompleted'}, {value: 'InProgress', title: 'InProgress'}]}
                        />
                    </div>

                    <div className='input_label bold mb-10 mt-10 ml-10' >Topics Covered</div>
                    <div style={{marginLeft: 10, flexDirection: 'row', flexWrap: 'wrap', display: 'flex', }} >
                        {topicCovered.map((item) => (
                            <div style={{width: 150}} >
                                <CheckBox label={item.subject} value={item.subject} />
                            </div>
                        ))}
                    </div>

                    <div className='input_label bold mb-10 mt-10 ml-10' >Student Mood</div>
                    <div style={{marginLeft: 10, flexDirection: 'row', flexWrap: 'wrap', display: 'flex', }} >
                        {studentMood.map((item) => (
                            <div style={{width: 150}} >
                                <CheckBox label={item.subject} value={item.subject} />
                            </div>
                        ))}
                    </div>

                    <div className='input_label bold mb-10 mt-10 ml-10' >Homework Assigned</div>
                    <div style={{marginLeft: 10, flexDirection: 'row', flexWrap: 'wrap', display: 'flex', }} >
                        {homeworkAssigned.map((item) => (
                            <div style={{}} >
                                <CheckBox label={item.subject} value={item.subject} />
                            </div>
                        ))}
                    </div>

                    <div className='input_label bold mb-10 mt-10 ml-10' >Was the session Productive?</div>
                    <div style={{marginLeft: 10, flexDirection: 'row', flexWrap: 'wrap', display: 'flex', }} >
                        {sessionProductive.map((item) => (
                            <div style={{width: 150}} >
                                <CheckBox label={item.title} value={item.title} />
                            </div>
                        ))}
                    </div>


                    <div className='fd-r jc-sb d-f full-width mt-20' >
                        <div>
                            <div className='input_label bold mt-10 ml-10' >Session Note</div>
                            <TextInput style={{width: 680, marginTop: 10}} label="Session Note" />
                        </div>
                    </div>

                </div>

                <div  className='center-container' style={{marginTop: 20}} >
                    <div className='margin-right' >
                        <SecondaryButton 
                            title="Cancel"
                            onClick={handleClose}
                        />
                    </div>
                    <div className='margin-left' >
                        <Button
                            primary 
                            title="Schedule"
                            onClick={handleClose}
                        />
                    </div>
                </div>
            </Box>
        </Modal>
        </div>
    );
}

