import * as React from 'react';
import Box from '@mui/material/Box';

import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { Colors } from '../../../util/Colors';
import Button from '../../button/Button';

import  './createSession.scss'
import Dropdown from '../../accordian/Dropdown';
import Input from '../../textfield/Input';
import SecondaryButton from '../../button/SecondaryButton';
import SecondaryIconButton from '../../button/SecondaryIconButton';

import {FiUpload}from "react-icons/fi"
import {BsPlusLg}from "react-icons/bs"

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  bgcolor: 'background.paper',
  border: '0.5px solid #000',
  boxShadow: 10,
  p: 4,
};

export default function CreateSession() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

    return (
        <div className='container'>
            <SecondaryIconButton
                onClick={handleOpen}
                title="Create"
                icon={<BsPlusLg size={15} />}
            />
        
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Typography className='heading' id="modal-modal-title" variant="h6" component="h2">
                    <div className='heading-text' >Create a New Session for Student</div>
                </Typography>

                <div className='row-style' >
                    <div className='margin-right' >
                        <Input
                        />
                    </div>
                    <div className='margin-left' >
                        <Dropdown />
                    </div>
                </div>

                <div className='row-style' >
                    <div className='margin-right' >
                        <Dropdown />
                    </div>
                    <div className='margin-left' >
                        <Dropdown />
                    </div>
                </div>
                
                <div  className='center-container' style={{marginTop: 20}} >
                    <div className='margin-right' >
                        <SecondaryButton 
                            title="Close"
                            onClick={handleClose}
                        />
                    </div>
                    <div className='margin-left' >
                        <Button
                            primary 
                            title="Submit"
                            onClick={handleClose}
                        />
                    </div>
                </div>
            </Box>
        </Modal>
        </div>
    );
}

