import React, {useState} from 'react';
import Box from '@mui/material/Box';

import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { Colors } from '../../../util/Colors';
import Button from '../../button/Button';

import  './assignNewTest.scss'
import SecondaryButton from '../../button/SecondaryButton';
import SecondaryIconButton from '../../button/SecondaryIconButton';

import {FiUpload}from "react-icons/fi"
import { icons } from '../../../util/Icons';
import DropDown from '../../dropdown/DropDown';
import TextInput from '../../input/TextInput';


import "react-toastify/dist/ReactToastify.css";
import { ToastContainer, toast } from "react-toastify";
import DatePicker from '../../dateTimePicker/DatePicker';
import IconButton from '../../button/iconButton/IconButton';
import FileUploadButton from '../../button/fileUploadButton/FileUploadButton';
import { assignTest, createNewTest } from '../../../api/ApiManager';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 650,
  bgcolor: 'background.paper',
  border: '0.5px solid #000',
  boxShadow: 10,
  p: 4,
};

export default function AssignNewTest({handleClose, handleOpen}) {


    const [value, setValue] = React.useState('');
    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});
    const [buttonLoader, setButtonLoader] = useState(false);
    
    const [fileName, setFileName] = useState('');
    const [fileDetails, setFileDetails] = useState('');
    const [dueDate, setDueDate] = useState(new Date());

    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const notify = (text, type) => {
        if(type == 'error'){
            toast.error(text, {
                position: toast.POSITION.TOP_RIGHT,
                className: "toast",
            });
        }else if(type == 'success'){
            toast.success(text, {
                position: toast.POSITION.TOP_RIGHT,
                className: "toast",
            });
        }     
    };


    const handleCreate = (e) => {
        e.preventDefault();
        //setButtonLoader(true)
        console.log("-----SUBMIT----");

        console.log("form => >>  ", form);

        if (!form.testName) {
            setButtonLoader(false)
            console.log("Please enter a valid test name");

            setErrors((prev) => {
                return { ...prev, testName: "Please enter a valid test name" };
            });
        }

        if (!form.studentName) {
            setButtonLoader(false)
            console.log("Please enter a valid student name");

            setErrors((prev) => {
                return { ...prev, studentName: "Please enter a valid student name" };
            });
        }

        if (!form.timelLimit) {
            setButtonLoader(false)
            console.log("Please upload a valid time limit");

            setErrors((prev) => {
                return { ...prev, timelLimit: "Please upload a valid time limit" };
            });
        }

        if (!dueDate) {
            setButtonLoader(false)
            console.log("Please enter a valid dueDate");

            setErrors((prev) => {
                return { ...prev, email: "Please enter a valid due date" };
            });
        }

        if (form.testName && form.studentName && form.timelLimit && dueDate) {
            console.log("-------- USER DETAILS --------");

            var data = {
                name: "SAT0",
                durationType: "Regular",
                testId: "62f0d30fa9d6fdff259b8d84",
                students: ["62ed4335d5f1921866a74fbe"],
                dueDate: "2021-05-27T05:55:23.408+0000"
            }
            console.log("data => > ", data);

            assignTest(data)
            .then((resp) => {
                console.log("assignTest resp => ", resp);
                console.log("status => ", resp.status);
                if(resp.status == '200' || resp.status == '201'){
                    handleClose()
                    notify(resp.message, 'success')
                }else{
                    notify(resp.message, 'error')
                }
                
                setButtonLoader(false)
            })
            .catch((err) => {
                console.log("err = ", err);
                setButtonLoader(false)
            })
        }
    };



    return (
        <div className='container'>
        <Modal
            open={true}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <div  onClick={handleClose} className='close-icon' style={{position: 'absolute', right: 30, }} >
                    <img src={icons.cancel_circle} style={{height: 24, width: 24}} />
                </div>

                <Typography className='heading' id="modal-modal-title" variant="h6" component="h2">
                    <div className='heading-text' >Assign New Test</div>
                </Typography>

                <div className='row-style' >
                    <div className='margin-right' >
                        <div className="input_label ml-10">Student Name</div>
                        <TextInput 
                            style={{ width: "100%" }} 
                            label="Student Name" 
                            value={form.studentName}
                            error={errors.studentName}
                            onChange={event => onChange({name: 'studentName', value: event.target.value})}
                        />
                    </div>
                    <div className='margin-left' >
                        <div className='label-style' >Time Limit</div>
                        <DropDown 
                            style={{width: 250}}
                            label="Time Limit"
                            error={errors.timelLimit}
                            handleChange={event => onChange({name: 'timelLimit', value: event.target.value})}
                            data={[{value: 'SAT', title: 'SAT'}, {value: 'SAT', title: 'SAT'}]}
                        />
                    </div>
                </div>

                <div className='row-style ml-10' >
                    <div className='margin-right' >
                        <div className='label-style mb-20' >Due Date</div>
                        <DatePicker
                            label="Due Date"
                            value={dueDate}
                            handleChange={(newValue) => {
                                setDueDate(newValue);
                            }}
                        />
                    </div>
                    <div className='margin-left' >
                        <div className='label-style mb-20' >Test Name</div>
                        <DropDown 
                            style={{width: 250}}
                            label="Test Name"
                            error={errors.testName}
                            handleChange={event => onChange({name: 'testName', value: event.target.value})}
                            data={[{value: 'SAT', title: 'SAT'}, {value: 'SAT', title: 'SAT'}]}
                        />
                    </div>
                </div>

                <div  className='center-container' style={{marginTop: 20}} >
                    <div className='margin-right' >
                        <SecondaryButton 
                            title="Cancel"
                            onClick={handleClose}
                        />
                    </div>
                    <div className='margin-left' >
                        <Button
                            primary 
                            title="Create"
                            onClick={handleCreate}
                        />
                    </div>
                </div>
            </Box>
        </Modal>

        <ToastContainer
                hideProgressBar={true}
                newestOnTop={false}
                closeButton={false}
            />
        </div>
    );
}



// import * as React from 'react';
// import Box from '@mui/material/Box';

// import Typography from '@mui/material/Typography';
// import Modal from '@mui/material/Modal';
// import { Colors } from '../../../util/Colors';
// import Button from '../../button/Button';

// import  './assignNewTest.scss'
// import Dropdown from '../../accordian/Dropdown';
// import Input from '../../textfield/Input';
// import SecondaryButton from '../../button/SecondaryButton';
// import SecondaryIconButton from '../../button/SecondaryIconButton';

// import {FiUpload}from "react-icons/fi"
// import {BsPlusLg}from "react-icons/bs"

// const style = {
//   position: 'absolute',
//   top: '50%',
//   left: '50%',
//   transform: 'translate(-50%, -50%)',
//   width: 600,
//   bgcolor: 'background.paper',
//   border: '0.5px solid #000',
//   boxShadow: 10,
//   p: 4,
// };

// export default function AssignNewTest() {
//   const [open, setOpen] = React.useState(false);
//   const handleOpen = () => setOpen(true);
//   const handleClose = () => setOpen(false);

//     return (
//         <div className='container'>
//             <SecondaryIconButton
//                 onClick={handleOpen}
//                 title="Assign a new Test"
//                 icon={<BsPlusLg size={15} />}
//             />
        
//         <Modal
//             open={open}
//             onClose={handleClose}
//             aria-labelledby="modal-modal-title"
//             aria-describedby="modal-modal-description"
//         >
//             <Box sx={style}>
//                 <Typography className='heading' id="modal-modal-title" variant="h6" component="h2">
//                     <div className='heading-text' >Create a New Test</div>
//                 </Typography>

//                 <div className='row-style' >
//                     <div className='margin-right' >
//                         <Input
//                         />
//                     </div>
//                     <div className='margin-left' >
//                         <Dropdown />
//                     </div>
//                 </div>

//                 <div className='row-style' >
//                     <div className='margin-right' >
//                         <Dropdown />
//                     </div>
//                     <div className='margin-left' >
//                         <Dropdown />
//                     </div>
//                 </div>
                
//                 <div  className='center-container' style={{marginTop: 20}} >
//                     <div className='margin-right' >
//                         <SecondaryButton 
//                             title="Close"
//                             onClick={handleClose}
//                         />
//                     </div>
//                     <div className='margin-left' >
//                         <Button
//                             primary 
//                             title="Submit"
//                             onClick={handleClose}
//                         />
//                     </div>
//                 </div>
//             </Box>
//         </Modal>
//         </div>
//     );
// }

