import * as React from 'react';
import Box from '@mui/material/Box';

import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { Colors } from '../../../util/Colors';

import Button from '../../button/Button';

import  './attendaceDetails.scss'
import Dropdown from '../../accordian/Dropdown';
import Input from '../../textfield/Input';
import SecondaryButton from '../../button/SecondaryButton';
import SecondaryIconButton from '../../button/SecondaryIconButton';

import {FiUpload}from "react-icons/fi"
import { icons } from '../../../util/Icons';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  bgcolor: 'background.paper',
  border: '0.5px solid #000',
  boxShadow: 10,
  p: 4,
};

export default function AttendaceDetails({handleClose, handleOpen, handleSubmit, arrayList, listLength}) {

    return (
        <div className='container'>
        <Modal
            open={true}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <div  onClick={handleClose} className='close-icon' style={{position: 'absolute', right: 30, }} >
                    <img src={icons.cancel_circle} style={{height: 24, width: 24}} />
                </div>

                <Typography className='heading' id="modal-modal-title" variant="h6" component="h2">
                    <div className='heading-text' >Attendance Details</div>
                </Typography>

                <div style={{margin: 20, fontSize: 24, alignItems: 'center',justifyContent: 'center', display: 'flex'}} >
                    Are you sure, you want to submit the details? 
                </div>

                <div  style={{alignItems: 'center',justifyContent: 'center', display: 'flex', flexDirection: 'column'}} >
                    Total Student : {listLength}
                </div>

                <div  style={{alignItems: 'center',justifyContent: 'center', display: 'flex', flexDirection: 'column'}} >
                    Present Student : {arrayList?.length}
                </div>

                <div  style={{alignItems: 'center',justifyContent: 'center', display: 'flex', flexDirection: 'column'}} >
                    Absent Student : {listLength - arrayList?.length}
                </div>

                <div style={{marginTop: 20, alignItems: 'center',justifyContent: 'center', display: 'flex', flexDirection: 'column'}} >
                    {arrayList && arrayList.map((item) => (
                       <div style={{margin: 4, fontSize: 24, }} >{item.name}</div>
                    ))}
                </div>


                {/* <div className='row-style' >
                    <div className='margin-right' >
                        <Input
                        />
                    </div>
                    <div className='margin-left' >
                        <Dropdown />
                    </div>
                </div>

                <div className='row-style' >
                    <div className='margin-right' >
                    <Dropdown />
                    </div>
                </div> */}

                <div  className='center-container' style={{marginTop: 20}} >
                    <div className='margin-right' >
                        <SecondaryButton 
                            title="Cancel"
                            onClick={handleClose}
                        />
                    </div>
                    <div className='margin-left' >
                        <Button
                            primary 
                            title="Submit"
                            onClick={handleSubmit}
                        />
                    </div>
                </div>
            </Box>
        </Modal>
        </div>
    );
}

