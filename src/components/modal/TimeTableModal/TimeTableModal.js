import * as React from 'react';
import Box from '@mui/material/Box';

import Modal from '@mui/material/Modal';


import Button from '../../button/Button';

import  './timeTableModal.scss'

import SecondaryButton from '../../button/SecondaryButton';

import { icons } from '../../../util/Icons';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  bgcolor: 'background.paper',
  border: '0.5px solid #000',
  boxShadow: 10,
  p: 4,
};

const classes = [
    {
        id: 1,
        day: 'Monday',
        subjects: [
            {
                id: 1,
                name: 'History/Civics', 
                duration: '9:00 - 10-00 AM',
            },
            {
                id: 1,
                name: 'Hindi', 
                duration: '10:00 - 11-00 AM',
            },
            {
                id: 1,
                name: 'Geography', 
                duration: '11:00 - 12-00 PM',
            },
            {
                id: 1,
                name: 'Geography', 
                duration: '12:00 - 1-00 PM',
            },
        ]
    },
    {
        id: 1,
        day: 'Tuesday',
        subjects: [
            {
                id: 1,
                name: 'Mathematics', 
                duration: '9:00 - 10-00 AM',
            },
            {
                id: 1,
                name: 'Hindi', 
                duration: '10:00 - 11-00 AM',
            },
            {
                id: 1,
                name: 'Science', 
                duration: '11:00 - 12-00 PM',
            },
            {
                id: 1,
                name: 'Geography', 
                duration: '12:00 - 1-00 PM',
            },
        ]
    },
    {
        id: 1,
        day: 'Wednesday',
        subjects: [
            {
                id: 1,
                name: 'Mathematics', 
                duration: '9:00 - 10-00 AM',
            },
            {
                id: 1,
                name: 'Drawing', 
                duration: '10:00 - 11-00 AM',
            },
            {
                id: 1,
                name: 'English', 
                duration: '11:00 - 12-00 PM',
            },
            {
                id: 1,
                name: 'Art & Craft', 
                duration: '12:00 - 1-00 PM',
            },
        ]
    },
    {
        id: 1,
        day: 'Thursday',
        subjects: [
            {
                id: 1,
                name: 'Hindi', 
                duration: '9:00 - 10-00 AM',
            },
            {
                id: 1,
                name: 'Hindi', 
                duration: '10:00 - 11-00 AM',
            },
            {
                id: 1,
                name: 'Games', 
                duration: '11:00 - 12-00 PM',
            },
            {
                id: 1,
                name: 'Geography', 
                duration: '12:00 - 1-00 PM',
            },
        ]
    },
    {
        id: 1,
        day: 'Friday',
        subjects: [
            {
                id: 1,
                name: 'English', 
                duration: '9:00 - 10-00 AM',
            },
            {
                id: 1,
                name: 'History/Civics', 
                duration: '10:00 - 11-00 AM',
            },
            {
                id: 1,
                name: 'Mathematics', 
                duration: '11:00 - 12-00 PM',
            },
            {
                id: 1,
                name: 'EVS', 
                duration: '12:00 - 1-00 PM',
            },
        ]
    },
]

export default function TimeTableModal({handleClose, handleOpen, handleSubmit, }) {

    const d = new Date();
    let todaysDay = d.getDay();

    return (
        <div className='container'>
        <Modal
            open={true}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <div  onClick={handleClose} className='close-icon' style={{position: 'absolute', right: 30, }} >
                    <img src={icons.cancel_circle} style={{height: 24, width: 24}} />
                </div>

                <div style={{ alignItems: 'center', justifyContent: 'center', display: 'flex', flexDirection: 'column'}} >   
                    <div className='todays_title' >Time Table</div>
                    <div style={{flexDirection: 'row', display: 'flex', marginBottom: 20}} >
                        <div style={{width: 200, }} > </div>
                        <div style={{width: 120, fontWeight: 'bold' }} >9: 00 AM</div>
                        <div style={{width: 120, fontWeight: 'bold' }} >10: 00 AM</div>
                        <div style={{width: 120, fontWeight: 'bold' }} >11: 00 AM</div>
                        <div style={{width: 120, fontWeight: 'bold' }} >12: 00 PM</div>
                    </div>
                    {classes.map((item, index) => (
                        <div>
                                <div style={{flexDirection: 'row', display: 'flex', marginBottom: 20}} >
                                    <div style={{width: 160, fontSize: 24, alignItems: 'center', justifyContent: 'center', display: 'flex'}} >{item.day}</div>
                                    {item.subjects.map((subject) => (
                                        <div className='subject_name' >{subject.name}</div>
                                    ))}
                                </div>
                        </div>
                    ))
                    }
                </div>


                {/* <div  className='center-container' style={{marginTop: 20}} >
                    <div className='margin-right' >
                        <SecondaryButton 
                            title="Cancel"
                            onClick={handleClose}
                        />
                    </div>
                    <div className='margin-left' >
                        <Button
                            primary 
                            title="Submit"
                            onClick={handleSubmit}
                        />
                    </div>
                </div> */}
            </Box>
        </Modal>
        </div>
    );
}

