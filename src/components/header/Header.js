import React from 'react'
import IconButton from '../button/iconButton/IconButton';
import './header.scss'


const Header = (props) => {

    const { title, buttonIcon, buttonTitle, onClick, buttonColor, containerStyle} = props;
    return (    
        <div className="header-container"  >
           <h1>{title}</h1>
           {buttonIcon ? 
           <IconButton onClick={() => {onClick()}} title={buttonTitle} icon={buttonIcon} buttonColor={buttonColor} />
         : null}
        </div>
    )
}

export default Header