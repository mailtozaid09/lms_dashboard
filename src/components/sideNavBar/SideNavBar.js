import React, { useState, useEffect } from 'react';

import { Link, NavLink, useNavigate } from 'react-router-dom';
import './sideNavBar.scss'

import {FaUserAlt,}from "react-icons/fa"
import {FiUpload}from "react-icons/fi"
import { icons } from '../../util/Icons';
import { useSelector } from 'react-redux';


const SideNavBar = () => {
    const navigate = useNavigate();

    const userRole = useSelector(state=> state.userRole)
   

    
    const[isOpen ,setIsOpen] = useState(true);
    
    const toggle = () => setIsOpen (!isOpen);

    const [active, setActive] = useState('Dashboard')


    useEffect(() => {
        console.log("useEfcect ==");
        
        console.log("userRole => ==> ", userRole);

        userRole == 'STUDENT' ? navigate('/studentDashboard') : 
        userRole == 'PARENT' ? navigate('/parentDashboard') : 
        userRole == 'TUTOR' ? navigate('/tutorDashboard') : 
        navigate('/dashboard')

    }, [])
    
    const adminItem = [
        {
            path:"/dashboard",
            name:"Dashboard",
            icon: icons.online_classA,
            iconIA: icons.online_class,
        },
        // {
        //     path:"/assignedTest",
        //     name:"Assigned Tests",
        //     icon: icons.test_detailsA,
        //     iconIA: icons.test_details,
        // },
        {
            path:"/allTest",
            name:"Test Details",
            icon: icons.all_testA,
            iconIA: icons.all_test,
        },
        {
            path:"/settings",
            name:"Settings",
            icon: icons.settingsA,
            iconIA: icons.settings,
        },
        {
            path:"/users",
            name:"All Users",
            icon: icons.profileA,
            iconIA: icons.profile,
        },
        {
            path:"/attendance",
            name:"Attendance",
            icon: icons.attendanceA,
            iconIA: icons.attendance,
        },
    ]

    const studentItem = [
        {
            path:"/studentDashboard",
            name:"Dashboard",
            icon: icons.online_classA,
            iconIA: icons.online_class,
        },
        {
            path:"/studentProfile",
            name:"Profile",
            icon: icons.userA,
            iconIA: icons.user,
        },
        // {
        //     path:"/assignedTest",
        //     name:"Assigned Tests",
        //     icon: icons.test_detailsA,
        //     iconIA: icons.test_details,
        // },
        {
            path:"/allTest",
            name:"Test Details",
            icon: icons.all_testA,
            iconIA: icons.all_test,
        },
        {
            path:"/conceptAnalytics",
            name:"Concept",
            icon: icons.conceptA,
            iconIA: icons.concept,
        },
        {
            path:"/settings",
            name:"Settings",
            icon: icons.settingsA,
            iconIA: icons.settings,
        },
        {
            path:"/users",
            name:"All Users",
            icon: icons.profileA,
            iconIA: icons.profile,
        },
    ]


    const parentItem = [
        {
            path:"/parentDashboard",
            name:"Dashboard",
            icon: icons.online_classA,
            iconIA: icons.online_class,
        },
        {
            path:"/parentProfile",
            name:"Profile",
            icon: icons.userA,
            iconIA: icons.user,
        },
        // {
        //     path:"/assignedTest",
        //     name:"Assigned Tests",
        //     icon: icons.test_detailsA,
        //     iconIA: icons.test_details,
        // },
        {
            path:"/allTest",
            name:"Test Details",
            icon: icons.all_testA,
            iconIA: icons.all_test,
        },
        {
            path:"/conceptAnalytics",
            name:"Concept",
            icon: icons.conceptA,
            iconIA: icons.concept,
        },
        {
            path:"/settings",
            name:"Settings",
            icon: icons.settingsA,
            iconIA: icons.settings,
        },
        {
            path:"/users",
            name:"All Users",
            icon: icons.profileA,
            iconIA: icons.profile,
        },
    ]


    const tutorItem = [
        {
            path:"/tutorDashboard",
            name:"Dashboard",
            icon: icons.online_classA,
            iconIA: icons.online_class,
        },
        {
            path:"/tutorProfile",
            name:"Profile",
            icon: icons.userA,
            iconIA: icons.user,
        },
        // {
        //     path:"/assignedTest",
        //     name:"Assigned Tests",
        //     icon: icons.test_detailsA,
        //     iconIA: icons.test_details,
        // },
        {
            path:"/allTest",
            name:"Test Details",
            icon: icons.all_testA,
            iconIA: icons.all_test,
        },
        {
            path:"/conceptAnalytics",
            name:"Concept",
            icon: icons.conceptA,
            iconIA: icons.concept,
        },
        {
            path:"/settings",
            name:"Settings",
            icon: icons.settingsA,
            iconIA: icons.settings,
        },
        {
            path:"/users",
            name:"All Users",
            icon: icons.profileA,
            iconIA: icons.profile,
        },
    ]


    const renderAdminItem = () => {
        return(
            <div>
                <nav className='main_navigation'>
                <div className="navigation" style={{width: isOpen ? 80 : 200}} > 
                    <ul>
                            {adminItem.map((item, index)=>(
                                <li onClick={() => setActive(item.name)} className={`list  ${item.name == active && "active"}`} >
                                    <b></b>
                                    <b></b>
                                    <Link to={item.path}>
                                        <span className='icon' >
                                            {/* <FiUpload size={20} /> */}
                                            {item.name == active 
                                                ?
                                                <img src={item.icon} style={{height: 28, width: 28}} />
                                                :
                                                <img src={item.iconIA} style={{height: 28, width: 28}} />
                                                }
                                        </span>
                                        
                                        {!isOpen ?  <span className='title' >{item.name}</span> : null}
                                    
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    
                    <div onClick={() => {navigate('/signin')}} className="logout_button" style={{position: 'absolute', bottom: 20, width: isOpen ? 70 : 160, color: 'white', height: 20, display: 'flex',alignItems: 'center', justifyContent: 'space-around',}} >
                            <span className='icon' style={{display: 'flex', alignItems: 'center', }} >
                                <img src={require('../../assets/icons/logout.png')} style={{height: 28, width: 28}} />
                                {!isOpen ?  <span className='title' style={{marginLeft: "30px"}} >Logout</span> : null}
                            </span>
                            
                    </div>
                </div>
                
                
            </nav>
            </div>
        )
    }

    const renderStudentItem = () => {
        return(
            <div>
                <nav className='main_navigation'>
                <div className="navigation" style={{width: isOpen ? 80 : 200}} > 
                    <ul>
                            {studentItem.map((item, index)=>(
                                <li onClick={() => setActive(item.name)} className={`list  ${item.name == active && "active"}`} >
                                    <b></b>
                                    <b></b>
                                    <Link to={item.path}>
                                        <span className='icon' >
                                            {/* <FiUpload size={20} /> */}
                                            {item.name == active 
                                                ?
                                                <img src={item.icon} style={{height: 28, width: 28}} />
                                                :
                                                <img src={item.iconIA} style={{height: 28, width: 28}} />
                                                }
                                        </span>
                                        
                                        {!isOpen ?  <span className='title' >{item.name}</span> : null}
                                    
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    
                    <div onClick={() => {navigate('/signin')}} className="logout_button" style={{position: 'absolute', bottom: 20, width: isOpen ? 70 : 160, color: 'white', height: 20, display: 'flex',alignItems: 'center', justifyContent: 'space-around',}} >
                            <span className='icon' style={{display: 'flex', alignItems: 'center', }} >
                                <img src={require('../../assets/icons/logout.png')} style={{height: 28, width: 28}} />
                                {!isOpen ?  <span className='title' style={{marginLeft: "30px"}} >Logout</span> : null}
                            </span>
                            
                    </div>
                </div>
                
                
            </nav>
            </div>
        )
    }


    const renderParentItem = () => {
        return(
            <div>
                <nav className='main_navigation'>
                <div className="navigation" style={{width: isOpen ? 80 : 200}} > 
                    <ul>
                            {parentItem.map((item, index)=>(
                                <li onClick={() => setActive(item.name)} className={`list  ${item.name == active && "active"}`} >
                                    <b></b>
                                    <b></b>
                                    <Link to={item.path}>
                                        <span className='icon' >
                                            {/* <FiUpload size={20} /> */}
                                            {item.name == active 
                                                ?
                                                <img src={item.icon} style={{height: 28, width: 28}} />
                                                :
                                                <img src={item.iconIA} style={{height: 28, width: 28}} />
                                                }
                                        </span>
                                        
                                        {!isOpen ?  <span className='title' >{item.name}</span> : null}
                                    
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    
                    <div onClick={() => {navigate('/signin')}} className="logout_button" style={{position: 'absolute', bottom: 20, width: isOpen ? 70 : 160, color: 'white', height: 20, display: 'flex',alignItems: 'center', justifyContent: 'space-around',}} >
                            <span className='icon' style={{display: 'flex', alignItems: 'center', }} >
                                <img src={require('../../assets/icons/logout.png')} style={{height: 28, width: 28}} />
                                {!isOpen ?  <span className='title' style={{marginLeft: "30px"}} >Logout</span> : null}
                            </span>
                            
                    </div>
                </div>
                
                
            </nav>
            </div>
        )
    }

    const renderTutorItem = () => {
        return(
            <div>
                <nav className='main_navigation'>
                <div className="navigation" style={{width: isOpen ? 80 : 200}} > 
                    <ul>
                            {tutorItem.map((item, index)=>(
                                <li onClick={() => setActive(item.name)} className={`list  ${item.name == active && "active"}`} >
                                    <b></b>
                                    <b></b>
                                    <Link to={item.path}>
                                        <span className='icon' >
                                            {/* <FiUpload size={20} /> */}
                                            {item.name == active 
                                                ?
                                                <img src={item.icon} style={{height: 28, width: 28}} />
                                                :
                                                <img src={item.iconIA} style={{height: 28, width: 28}} />
                                                }
                                        </span>
                                        
                                        {!isOpen ?  <span className='title' >{item.name}</span> : null}
                                    
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    
                    <div onClick={() => {navigate('/signin')}} className="logout_button" style={{position: 'absolute', bottom: 20, width: isOpen ? 70 : 160, color: 'white', height: 20, display: 'flex',alignItems: 'center', justifyContent: 'space-around',}} >
                            <span className='icon' style={{display: 'flex', alignItems: 'center', }} >
                                <img src={require('../../assets/icons/logout.png')} style={{height: 28, width: 28}} />
                                {!isOpen ?  <span className='title' style={{marginLeft: "30px"}} >Logout</span> : null}
                            </span>
                            
                    </div>
                </div>
                
                
            </nav>
            </div>
        )
    }

    return (
        <div>
            {userRole == 'ADMIN'
                ?
                <div>
                    {renderAdminItem()}
                </div>
                :
                userRole == 'STUDENT'
                ?
                <div>
                    {renderStudentItem()}
                </div>
                :
                userRole == 'PARENT'
                ?
                <div>
                    {renderParentItem()}
                </div>
                :
                userRole == 'TUTOR'
                ?
                <div>
                    {renderTutorItem()}
                </div>
                :
                <div>ERROR</div>
            }

            

            <div onClick={toggle} className={`${!isOpen ? "toggle active": "toggle" }`} >
                <img src={require('../../assets/icons/dashboard.png')} style={{height: 28, width: 28}} />
            </div>
        </div>
    );
};

export default SideNavBar;