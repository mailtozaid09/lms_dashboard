import React from 'react'
import { ThreeDots } from  'react-loader-spinner'
import { Colors } from '../../util/Colors'

const ScreenLoader = () => {
    return (
        <div>
            <ThreeDots
                height = "150"
                width = "150"
                radius = "9"
                color = {Colors.PRIMARY}
                ariaLabel = 'three-dots-loading'     
                wrapperStyle
                wrapperClass
            />
        </div>
    )
}

export default ScreenLoader

