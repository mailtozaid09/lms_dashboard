import React from 'react'
import { ThreeDots } from  'react-loader-spinner'

const ButtonLoader = () => {
    return (
        <div>
            <ThreeDots
                height = "50"
                width = "50"
                radius = "9"
                color = 'white'
                ariaLabel = 'three-dots-loading'     
                wrapperStyle
                wrapperClass
            />
        </div>
    )
}

export default ButtonLoader

