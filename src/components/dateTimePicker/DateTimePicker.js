import * as React from 'react';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';

export default function DateTimePicker(props) {

    const { label, dateValue, width } = props;
 
    return (
        <Stack component="form" noValidate spacing={3}>
            <TextField
                id="datetime-local"
                label={label}
                type="datetime-local"
                defaultValue={dateValue}
                sx={{ width: width ? width : 250 }}
                InputLabelProps={{
                shrink: true,
                }}
            />
        </Stack>
    );
}
