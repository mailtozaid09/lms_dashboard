import * as React from 'react';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';

import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { MobileDatePicker } from '@mui/x-date-pickers/MobileDatePicker';

import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';


export default function DatePicker(props) {

    const { label, dateValue, value, handleChange, width } = props;

 
    return (
        <LocalizationProvider dateAdapter={AdapterDateFns}>
      
        <Stack component="form" noValidate  sx={{ width: width ? width : 250 }} >
            <DesktopDatePicker
                label={label}
                inputFormat="dd/MM/yyyy"
                value={value}
                onChange={(date) => handleChange(date)}
                className="date_picker"
               
                renderInput={(params) => <TextField {...params} />}
            />
                {/* <MobileDatePicker
                label="Date mobile"
                inputFormat="MM/dd/yyyy"
                // value={value}
                // onChange={handleChange}
                renderInput={(params) => <TextField {...params} />}
            /> */}

        </Stack>
        </LocalizationProvider>
    );
}
