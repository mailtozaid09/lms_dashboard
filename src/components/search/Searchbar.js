import React from 'react'
import IconButton from '@mui/material/IconButton';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import SearchIcon from '@mui/icons-material/Search';

const Searchbar = (props) => {

    const {label, value, onChangeText, onClick} = props;
    
    return (
        <div>
            <FormControl sx={{ m: 1, width: '25ch' }} variant="outlined">
            <InputLabel >{label}</InputLabel>
            <OutlinedInput
                type={'text'}
                value={value}
                onChange={onChangeText}
                endAdornment={
                <InputAdornment position="end">
                    <IconButton
                    onClick={onClick}
                    edge="end"
                    >
                    <SearchIcon /> 
                    </IconButton>
                </InputAdornment>
                }
                label={label}
            />
            </FormControl>
        </div>
    )
}

export default Searchbar