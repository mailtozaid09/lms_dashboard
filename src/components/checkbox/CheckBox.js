import React from 'react'
import IconButton from '@mui/material/IconButton';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';
import SearchIcon from '@mui/icons-material/Search';
import { Checkbox, FormControlLabel, FormLabel } from '@mui/material';

const CheckBox = (props) => {

    const {label, value, onChangeText, onClick, addToList } = props;
    
    return (
        <div onClick={() => addToList()} >
            <FormControl component="fieldset">
                <FormControlLabel
                    value={value}
                    control={<Checkbox />}
                    label={label}
                    labelPlacement="end"
                />
            </FormControl>
        </div>
    )
}

export default CheckBox