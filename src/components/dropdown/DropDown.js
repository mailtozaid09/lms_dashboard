import React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';

const DropDown = (props) => {

    const {label, data, handleChange, value, defaultValue, style, error} = props;
    
    // const [value, setValue] = React.useState(defaultValue ? defaultValue : '');

    // const handleChange = (event) => {
    //     setValue(event.target.value);
    // };

    
    return (
        <div>
            <FormControl style={style ? style : {width: 250}} >
                <InputLabel id="demo-simple-select-label">{label}</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={value ? value : defaultValue}
                    label={label}
                    //onChange={handleChange}
                    onChange={(val) => handleChange(val)}
                >
                    {data.map((item) => (
                         <MenuItem value={item.value}>{item.title}</MenuItem>
                    ))}
                </Select>
                {error ? <div className='error_style mt-10' >{error}</div> : null}
            </FormControl>
        </div>
    )
}

export default DropDown