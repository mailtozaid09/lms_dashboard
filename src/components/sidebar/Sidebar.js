import React, { useState } from 'react';

import { NavLink } from 'react-router-dom';
import './sidebar.scss'

import {FaUserAlt,}from "react-icons/fa"

const Sidebar = ({children}) => {
    const[isOpen ,setIsOpen] = useState(false);
    const toggle = () => setIsOpen (!isOpen);

    const [active, setActive] = useState('Dashboard');
    const menuItem=[
        {
            path:"/",
            name:"Dashboard",
            icon: require('../../assets/dashboard.png'),
            iconIA: require('../../assets/dashboardIA.png'),
        },
        {
            path:"/sessions",
            name:"Sessions",
            icon: require('../../assets/sessions.png'),
            iconIA: require('../../assets/sessionsIA.png'),
        },
        {
            path:"/allTest",
            name:"All Test",
            icon: require('../../assets/test.png'),
            iconIA: require('../../assets/testIA.png'),
        },
        {
            path:"/about",
            name:"Percent",
            icon: require('../../assets/percent.png'),
            iconIA: require('../../assets/percentIA.png'),
        },
        {
            path:"/users",
            name:"Users",
            icon: require('../../assets/users.png'),
            iconIA: require('../../assets/usersIA.png'),
        },
        {
            path:"/settings",
            name:"Settings",
            icon: require('../../assets/percent.png'),
            iconIA: require('../../assets/percentIA.png'),
        },
        {
            path:"/testDetails",
            name:"Test Details",
            icon: require('../../assets/users.png'),
            iconIA: require('../../assets/usersIA.png'),
        },
    ]
    return (
        <div className="container">
           <div className="sidebar">
               {/* <div className="top_section">
                   <h1 style={{display: isOpen ? "block" : "none"}} className="logo">Logo</h1>
                   <div style={{marginLeft: isOpen ? "50px" : "0px"}} className="bars">
                       <FaBars onClick={toggle}/>
                   </div>
               </div> */}
               {
                   menuItem.map((item, index)=>(
                       <NavLink to={item.path} onClick={() => setActive(item.name)} key={index} className="link" activeclassName="active">
                           <div className="icon">
                                {item.name == active 
                                ?
                                <img src={item.icon} style={{height: 28, width: 28}} />
                                :
                                <img src={item.iconIA} style={{height: 28, width: 28}} />
                                }
                              
                            </div>
                           <div style={{display: "block", color: item.name == active ? 'black' : 'gray'}} className="link_text">{item.name}</div>
                       </NavLink>
                    // <div>sads</div>
                   ))
               }
           </div>
           <main>{children}</main>
        </div>
    );
};

export default Sidebar;