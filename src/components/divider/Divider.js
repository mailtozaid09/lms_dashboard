import React from 'react'
import './divider.scss'


const Divider = (props) => {

    const { style } = props;
    return (    
        <div className="divider-container">
            <div className="divider">
            </div>
        </div>
    )
}

export default Divider