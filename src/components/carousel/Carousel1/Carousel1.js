import React from 'react'
import './carousel.scss'
import { icons } from '../../../util/Icons';

import Slider from 'infinite-react-carousel';
import IconButton from '../../button/iconButton/IconButton';
import { Colors } from '../../../util/Colors';


const data = [
    {
        id: 1,
        image: icons.expert,
        title: 'This fall get help from our Admission Experts.'
    },
    {
        id: 2,
        image: icons.expert,
        title: 'This fall get help from our Admission Experts.'
    },
]
const Carousel1 = (props) => {

    const { style } = props;
    return (    
        <div>
            <Slider 
                dots
                autoplay={false}
                arrows={false}    
                dotsClass="carousel-dots"
                className="slider-container"
            >
                 {data.map((item) => (
                    <div className='main-carousel'>
                        <div className='carousel-container fd-r d-f jc-sb' >
                            <div className='carousel-text' >
                                <div>{item.title}</div>

                                <div>
                                    <IconButton
                                        title="Know More"
                                        icon={icons.right_arrow_purple} 
                                        //onClick={handleEditOpen}
                                        iconStyle={{height: 12, width:12}}
                                        textStyle={{color: Colors.PRIMARY, fontWeight: 'bold'}}
                                        buttonStyle={{position: 'absolute', left: 45, bottom: 40, backgroundColor: Colors.WHITE, marginRight: 5}}
                                    />
                                </div>
                            </div>
                            <div>
                                <img src={item.image} className='carousel-image' />
                            </div>
                        </div>
                    </div>
                ))}
            </Slider>
        </div>
    )
}

export default Carousel1