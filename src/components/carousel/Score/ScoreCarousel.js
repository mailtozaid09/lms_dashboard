import React from 'react'
import './scoreCarousel.scss'
import { icons } from '../../../util/Icons';

import Slider from 'infinite-react-carousel';
import IconButton from '../../button/iconButton/IconButton';
import { Colors } from '../../../util/Colors';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

const data = [
    {
        id: 1,
        image: icons.expert,
        title: 'Offical SAT Scores'
    },
    {
        id: 2,
        image: icons.expert,
        title: 'Offical SAT Scores'
    },
]

const percentage = 66;


const ScoreCarousel = (props) => {

    const { style } = props;
    return (    
        <div className='score_carousel' >
            <Slider 
                dots
                autoplay={false}
                arrows={false}  
                dotsClass="carousel-dots"
                className="slider-container"
               
            >
                 {data.map((item) => (
                    <div className='main-carousel'>
                        <div className='carousel-container fd-c ' >
                            <div className='title_name' >{item.title}</div>
                            <div className='fd-r jc-sb d-f full-width mt-20' >
                                <div className='fd-c d-f ai-c' >
                                    <div style={{ width: 60, height: 60 }}>
                                        <CircularProgressbar 
                                            value={66} 
                                            text="500"
                                            styles={buildStyles({
                                                pathColor: '#7152EB',
                                                textColor: '#4715D7',
                                                textSize: '20px'
                                              })}
                                               
                                        />
                                    </div>
                                    <div className='sub_title_name' >Cumilative Score</div>
                                </div>
                                <div className='fd-r d-f ' >
                                    <div className='score_container1' >
                                        <div className='score_heading'>200</div>
                                        <div className='score_sub_heading'>Verbal Score</div>
                                    </div>
                                    <div className='score_container2' >
                                        <div className='score_heading'>300</div>
                                        <div className='score_sub_heading'>Maths Score</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </Slider>
        </div>
    )
}

export default ScoreCarousel