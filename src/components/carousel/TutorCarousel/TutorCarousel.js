import React from 'react'
import './tutorCarousel.scss'
import { icons } from '../../../util/Icons';

import Slider from 'infinite-react-carousel';
import IconButton from '../../button/iconButton/IconButton';
import { Colors } from '../../../util/Colors';


const data = [
    {
        id: 1,
        image: icons.expertProfile,
        tutorName: 'Shivam Srivastava',
        tutorDesc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    },
    {
        id: 2,
        image: icons.expertProfile,
        tutorName: 'Shivam Srivastava',
        tutorDesc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    },
]
const TutorCarousel = () => {

    return (   
        <div className='tutor_container'>
            <Slider 
                dots
                autoplay={false}
                arrows={false}    
                dotsClass="carousel-dots"
                className="tutor-slider-container"
            >
                 {data.map((item) => (
                    <div className='tutor-main-carousel'>
                        <div className='tutor-carousel-container ' >
                            <div style={{fontSize: 20, color: 'rgba(1, 1, 1, 0.6)', fontWeight: 'bold'}} >Your Tutor</div>
                                <div style={{margin: 10}} >
                                    <img src={item.image} className='tutor-carousel-image' />
                                </div>
                                <div style={{fontSize: 16, color: '#000000', fontWeight: 'bold'}} >{item.tutorName}</div>
                                <div style={{fontSize: 12, color: '#000000', textAlign: 'center', marginTop: 10}} >{item.tutorDesc}</div>
                        </div>
                    </div>
                ))}
            </Slider>
        </div> 
     
    )
}

export default TutorCarousel