import React from 'react'
import './button.scss'

const SecondaryIconButton = (props) => {

    const { title, onClick, textStyle, containerStyle, container, icon} = props;
    return (    
        <>
         <div className={container ? container :  "button-container" } onClick={onClick} >
            <div onClick={containerStyle ? onClick : null} className="secondary-icon" style={containerStyle}>
                <div style={textStyle} className="secondary-icon-text">{title}</div>
                {icon}
            </div>
        </div>
       </>
    )
}

export default SecondaryIconButton