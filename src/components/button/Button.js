import React from 'react'
import './button.scss'


const Button = (props) => {

    const { title, onClick, textStyle, containerStyle, primary} = props;
    return (    
        <>
        {!primary
        ?
        <div className="button-container" onClick={onClick} >
            <div onClick={containerStyle ? onClick : null} className="button" style={containerStyle}>
                <div style={textStyle} className="button-text">{title}</div>
            </div>
        </div>
        :
        <div className="button-container"  onClick={onClick} >
            <div onClick={containerStyle ? onClick : null} className="primary" style={containerStyle}>
                <div style={textStyle} className="primary-text">{title}</div>
            </div>
        </div>
        }
       
       </>
    )
}

export default Button