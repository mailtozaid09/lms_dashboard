import React from 'react'
import { Colors } from '../../../util/Colors';
import { icons } from '../../../util/Icons';
import './backButton.scss'

const BackButton = (props) => {

    const { title, onClick, textStyle, buttonColor, container, icon, buttonStyle} = props;
    return (    
        <>
         <div className={container ? container :  "icon-button-container" } onClick={onClick} >
            <div className="icon-button" style={buttonStyle ? buttonStyle : {}} >
                <img src={icon ? icon : icons.left_arrow} style={{height: 24, width: 24}} />
                <div style={textStyle} className="back-button-text">{title}</div>
            </div>
        </div>
       </>
    )
}

export default BackButton