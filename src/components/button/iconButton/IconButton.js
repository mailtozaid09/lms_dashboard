import React from 'react'
import { Colors } from '../../../util/Colors';
import ButtonLoader from '../../loader/ButtonLoader';
import './iconButton.scss'

const IconButton = (props) => {

    const { title, onClick, textStyle, buttonColor, container, icon, buttonStyle, iconStyle, buttonLoader} = props;
    return (    
        <>
         <div className={container ? container :  "icon-button-container" } onClick={!buttonLoader ? onClick : null} >
            <div className="icon-button" style={buttonStyle ? buttonStyle : {backgroundColor: buttonColor ? buttonColor : Colors.PRIMARY}} >
                
                {buttonLoader
                ?
                    <div>
                        <ButtonLoader />
                    </div>
                :
                <>  
                    <div style={textStyle} className="icon-button-text">{title}</div>
                    {icon ? <img src={icon} style={iconStyle ? iconStyle : {height: 24, width: 24}} /> : null}
                </>
                }
               
            </div>
        </div>
       </>
    )
}

export default IconButton