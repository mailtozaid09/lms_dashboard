import React from 'react'
import { Colors } from '../../../util/Colors';
import ButtonLoader from '../../loader/ButtonLoader';
import './fileUploadButton.scss'

const FileUploadButton = (props) => {

    const { title, onChooseFile, fileName, onClick, textStyle, buttonColor, container, icon, buttonStyle, iconStyle, buttonLoader} = props;
    return (    
        <>
         <div> 
            
            <label className="input__file" htmlFor="fileInput">
                {buttonLoader
                    ?
                        <div>
                            <ButtonLoader />
                        </div>
                    :
                    <>  
                     <span>{title} </span>
                        {icon ? <img src={icon} style={iconStyle ? iconStyle : {height: 24, width: 24, marginLeft: 10}} /> : null} 
                        <input  
                            style={{display: "none"}} 
                            name="fileInput" 
                            id="fileInput" 
                            type="file" 
                            onChange={onChooseFile} 
                        /> 
                       
                    </>
                }
                   
               
            </label>
            <div>{fileName ? fileName : null}</div>
        </div> 
       </>
    )
}

export default FileUploadButton