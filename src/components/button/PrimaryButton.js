import React from 'react'
import './button.scss'


const PrimaryButton = (props) => {

    const { title, onClick, textStyle, containerStyle} = props;
    return (    
        <div className="button-container"  onClick={onClick} >
            <div onClick={containerStyle ? onClick : null} className="primary" style={containerStyle}>
                <div style={textStyle} className="primary-text">{title}</div>
            </div>
        </div>
    )
}

export default PrimaryButton