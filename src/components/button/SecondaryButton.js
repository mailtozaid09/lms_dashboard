import React from 'react'
import './button.scss'


const SecondaryButton = (props) => {

    const { title, onClick, textStyle, containerStyle, primary} = props;
    return (    
        <>
         <div className="button-container"  onClick={onClick} >
            <div onClick={containerStyle ? onClick : null} className="secondary" style={containerStyle}>
                <div style={textStyle} className="secondary-text">{title}</div>
            </div>
        </div>
       </>
    )
}

export default SecondaryButton