import React from 'react';
import BackButton from '../../../../components/button/BackButton/BackButton';
import IconButton from '../../../../components/button/iconButton/IconButton';
import { Colors } from '../../../../util/Colors';
import { icons } from '../../../../util/Icons';
import './editProfile.scss'

import { useNavigate } from 'react-router-dom';


const profileDetails = {
    id: 1,
    timeZone: 'IST (GMT+5:30)',
    birthYear: '1984',
    suubscriptionType: '3 Months Trial',
    industry: 'Medical',
    address: '1315 N State St, Ukiah, California,Postal Code- 95482 U',
    email: 'ranasapna78@gmail.com',
    whatsapp: '+91 012-3456-789',
}


const Profile = () => {

    const navigate = useNavigate();


    return (
        <div className='edit_profile_container' >
            <div className='profile_header' >
                <div className='profile_img_container' >
                    <div style={{position: 'relative'}} >
                        <img src={icons.expertProfile} className="user_img" />
                        <div className='camera_container' >
                            <img src={icons.camera} style={{height: 20, width: 20}} />
                        </div>
                    </div>

                    <div className='fd-r ai-c d-f' >
                        <div className='user_name' >Phill Brown</div>
                        <img className='edit_icon' src={icons.edit_purple} />
                    </div>
                   
                    <div className='fd-r ai-c d-f' >
                        <div className='user_description' >Phill Brown</div>
                        <img className='edit_icon' src={icons.edit_purple} />
                    </div>
                    
                </div>

                <div className='edit_container'>
                    <BackButton
                        title="Back"
                        onClick={() => {navigate(-1)}} 
                        icon={icons.left_arrow_grey} 
                        textStyle={{fontSize: 18, marginLeft: 10, color: '#FAFDFF', fontWeight: 'bold'}}
                        buttonStyle={{backgroundColor: Colors.MUSTURD,}}
                    />
                </div>
            </div>

            <div className='profile_details_container' >
                <div className='profile_details1 pos-rel ' >
                    <div className='associated_img_container  ' >
                        <div className='associated_user' >Associated Student</div>
                        <img src={icons.expertProfile} className="associated_user_img" />
                        <div className='associated_user_name' >Joseph Brown</div>
                        <div className='view_profile' >
                            <div style={{fontSize: 13}} >View Profile</div>
                            <img src={icons.right_arrow} style={{height: 8, width: 8, marginLeft: 8}} />
                        </div>
                    </div>
                    <div className='edit_icon_container' >
                        <img className='edit_icon' src={icons.edit_purple} />
                    </div>
                </div>
                <div className='profile_details2' >
                    <div  className='profile_details_row1'>
                        <div className='details_card1 ai-c d-f'>
                            <div className='jc-sb ai-c d-f full-width' >
                                <div className=' d-f fd-c pos-rel ' >
                                    <div className='details_heading'>Time Zone</div>
                                    <div className='details_sub_heading'>{profileDetails.timeZone}</div>
                                    <div className='edit_icon_container' style={{top: -12, right: -30}} >
                                        <img className='edit_icon' src={icons.edit_purple} />
                                    </div>
                                </div>
                                <div className=' d-f fd-c details_width' >
                                    <div className='details_heading' >Subscription Type</div>
                                    <div className='details_sub_heading'>{profileDetails.suubscriptionType}</div>
                                </div>
                            </div>
                        </div>
                        <div className='details_card2 ai-c d-f'>
                            <div className='fd-c ai-c d-f full-width pos-rel'  >
                                <div className=' d-f fd-c ' >
                                    <div className='details_heading'>Contact</div>
                                   
                                </div>
                                <div className='jc-sb d-f fd-r full-width' >
                                    <div className='ai-c d-f fd-c' >
                                        <img src={icons.email} style={{height: 22, width: 22}} />
                                        <div className='details_sub_heading'>{profileDetails.email}</div>
                                    </div>
                                   
                                    <div className='ai-c d-f fd-c' >
                                        <img src={icons.whatsapp} style={{height: 22, width: 22}} />
                                        <div className='details_sub_heading'>{profileDetails.whatsapp}</div>
                                    </div>
                                </div>

                                <div className='edit_icon_container' style={{top: -12, right: -10}} >
                                    <img className='edit_icon' src={icons.edit_purple} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div  className='profile_details_row2'>
                        <div className='details_card1 ai-c d-f'>
                            <div className='jc-sb ai-c d-f full-width' >
                                <div className=' d-f fd-c pos-rel ' >
                                    <div className='details_heading'>Birth Year</div>
                                    <div className='details_sub_heading'>{profileDetails.birthYear}</div>
                                    <div className='edit_icon_container' style={{top: -12, right: -68}} >
                                        <img className='edit_icon' src={icons.edit_purple} />
                                    </div>
                                </div>
                                <div className=' d-f fd-c details_width pos-rel' >
                                    <div className='details_heading' >Industry</div>
                                    <div className='details_sub_heading'>{profileDetails.industry}</div>

                                    <div className='edit_icon_container' style={{top: -12, right: -10}} >
                                        <img className='edit_icon' src={icons.edit_purple} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='details_card2 ai-c d-f'>
                            <div className='fd-c ai-c d-f full-width pos-rel' >
                                <div  className=' d-f fd-c' >
                                    <div className='details_heading'>Residential Address</div>
                                </div>
                                <div className='jc-sb d-f fd-r full-width' >
                                    <div className='details_sub_heading'>{profileDetails.address}</div>
                                </div>
                                <div className='edit_icon_container' style={{top: -22, right: -10}} >
                                    <img className='edit_icon' src={icons.edit_purple} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Profile;