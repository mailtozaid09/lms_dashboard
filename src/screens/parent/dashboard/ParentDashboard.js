import React from 'react';
import { icons } from '../../../util/Icons';
import './parentDash.scss'

import ProgressBar from "@ramonak/react-progress-bar";
import IconButton from '../../../components/button/iconButton/IconButton'
import { Colors } from '../../../util/Colors';

import ConceptAnalytics from '../../../components/graphs/ConceptAnalytics'
import Carousel1 from '../../../components/carousel/Carousel1/Carousel1';
import TutorCarousel from '../../../components/carousel/TutorCarousel/TutorCarousel';

const profileDetails = [
    {
        id: 1,
        label: 'Unpaid Invoice(s)',
        data: [
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'pay',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'pay',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'pay',
            },
        ]
    },
    {
        id: 1,
        label: 'Invoice History',
        data: [
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'download',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'download',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'download',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'download',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'download',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'download',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'download',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'download',
            },
        ]
    },
    {
        id: 1,
        label: 'Assigned Tests',
        data: [
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'Started',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'Started',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'Started',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'Started',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'Started',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'Started',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'Started',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'Started',
            },
        ]
    },
    {
        id: 1,
        label: 'Completed Tests',
        data: [
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: '0/1250',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: '1202/1250',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'download',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'download',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'download',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'download',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'download',
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'download',
            },
        ]
    },
]
const ParentDashboard = () => {
    return (
        <div>
            <h1>Parent Dashboard</h1>

            <div className='parent-dash-container' >
                <div className='parent-dash-container1' >

                    <div className='ai-c d-f full-width' >
                       <Carousel1 />
                       <TutorCarousel />
                    </div>

                    <ConceptAnalytics graphWidth='100%' />
                </div>



                <div className='parent-dash-container2' >
                    <div className='your-profile-container' >
                        <h1>Complete your Profile</h1>
                        <img src={icons.right_arrow} style={{height: 24, width: 24}} />
                    </div>

                    <div className='jc-sb ai-c d-f full-width' >
                        <div>Profile Status</div>
                        <div>65%</div>
                    </div>

                    <div className='full-width ' style={{marginTop: 10}}>
                        <ProgressBar 
                            completed={65} 
                            customLabel=" "  
                            bgColor='#62DD43'
                            height="12px"
                        />
                    </div>


                    <div className='parent-profile-deatils full-width'  >
                        {profileDetails && profileDetails.map((item) => (
                            <div>
                                <div className='label' >{item.label}</div>
                                <div className='container full-width' style={{height: item.data && item.data.length === 1 ? 80 : 120}} >
                                {item.data && item.data.map((el) => (
                                    <div className='jc-sb ai-c d-f full-width' style={{marginBottom: item.data && item.data.length === 1 ? 0 : 12}} >
                                        <div className='text-container' >
                                            <div className='' >
                                                {el.name}
                                            </div>
                                            <div className='' >
                                                {el.dueDate}
                                            </div>
                                        </div>
                                        <div className='' >
                                            <IconButton 
                                                title={el.button}
                                                buttonStyle={{backgroundColor: Colors.MUSTURD, width: 120}}
                                            />
                                        </div>
                                    </div>
                                ))}
                                </div>
                            </div>
                        ))}

                    </div>
                </div>
            </div>

        </div>
    );
};

export default ParentDashboard;