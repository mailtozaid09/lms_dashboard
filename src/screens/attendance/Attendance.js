import React, {useState, useEffect, } from 'react';
import IconButton from '../../components/button/iconButton/IconButton';
import CheckBox from '../../components/checkbox/CheckBox';
import DropDown from '../../components/dropdown/DropDown';
import AttendaceDetails from '../../components/modal/AttendanceModal/AttendanceDetails';
import NoticeBoardModal from '../../components/modal/NoticeBoradModal/NoticeBoradModal';
import TimeTableModal from '../../components/modal/TimeTableModal/TimeTableModal';
import TodaysClassesModal from '../../components/modal/TodaysClassesModal/TodaysClassesModal';
import { Colors } from '../../util/Colors';
import { icons } from '../../util/Icons';

import './attendance.scss'


const options = [
    {
        id: 1,
        title: "Today's Classes",
        color: '#009EFF',
    },
    {
        id: 1,
        title: "Time Table",
        color: '#F5A962',
    },
    // {
    //     id: 1,
    //     title: "Take Attendance",
    //     color: '#231955',
    // },
    {
        id: 1,
        title: "Notice Board",
        color: '#3C8DAD',
    },
]

const Attendance = () => {




    const [value, setValue] = React.useState('');
    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});

    const [arrayList, setArrayList] = useState([]);

    const [confirmation, setConfirmation] = useState(false);
    
    const [todaysClassesModal, setTodaysClassesModal] = useState(false);
    const [noticeBoardModal, setNoticeBoardModal] = useState(false);
    const [timeTableModal, setTimeTableModal] = useState(false);

    const [listLength, setListLength] = useState(0);
    

    const handleChange = (event) => {
        console.log("event.target.value => " , event.target.value);
        setErrors({})
        setValue(event.target.value);
        setForm({ ...form, ['testType']: event.target.value });

        if(event.target.value == 'Class10'){
            setListLength(5)
        }else if(event.target.value == 'Class11'){
            setListLength(10)
        }else if(event.target.value == 'Class12'){
            setListLength(5)
        }
        
        
        
    };

    const handleAttendanceOpen = () => {
        console.log("open modal");
        setConfirmation(true);
    };
    
    const handleAttendanceClose = () => {
        console.log("close modal");
        setConfirmation(false);
    }

    const handleAttendanceSubmit = () => {
        console.log("close modal");
        setConfirmation(false);
        setValue('')
    }



    const handleTodaysClassesOpen = () => {
        console.log("open modal");
        setTodaysClassesModal(true);
    };
    
    const handleTodaysClassesClose = () => {
        console.log("close modal");
        setTodaysClassesModal(false);
    }



    const handleTimeTableOpen = () => {
        console.log("open modal");
        setTimeTableModal(true);
    };
    
    const handleTimeTableClose = () => {
        console.log("close modal");
        setTimeTableModal(false);
    }

    

    const handleNoticeBoardOpen = () => {
        console.log("open modal");
        setNoticeBoardModal(true);
    };
    
    const handleNoticeBoardClose = () => {
        console.log("close modal");
        setNoticeBoardModal(false);
    }



    const class10List = [
        {
            id: 1,
            name: 'Student 1',
        },
        {
            id: 2,
            name: 'Student 2',
        },
        {
            id: 3,
            name: 'Student 3',
        },
        {
            id: 4,
            name: 'Student 4',
        },
        {
            id: 5,
            name: 'Student 5',
        },
    ]

    const class11List = [
        {
            id: 1,
            name: 'Student 1',
        },
        {
            id: 2,
            name: 'Student 2',
        },
        {
            id: 3,
            name: 'Student 3',
        },
        {
            id: 4,
            name: 'Student 4',
        },
        {
            id: 5,
            name: 'Student 5',
        },
        {
            id: 6,
            name: 'Student 6',
        },
        {
            id: 7,
            name: 'Student 7',
        },
        {
            id: 8,
            name: 'Student 8',
        },
        {
            id: 9,
            name: 'Student 9',
        },
        {
            id: 10,
            name: 'Student 10',
        },
    ]

    const class12List = [
        {
            id: 1,
            name: 'Student 1',
        },
        {
            id: 2,
            name: 'Student 2',
        },
        {
            id: 3,
            name: 'Student 3',
        },
        {
            id: 4,
            name: 'Student 4',
        },
        {
            id: 5,
            name: 'Student 5',
        },
    ]

    const addtoList = (array, name) => {
        var newArr = []
        newArr.push(name)

        //setArrayList(...arrayList, {"name": name})

        setArrayList(prevArray => [...prevArray, {'name': name}])
        //setArrayList(prevState => ({ ...prevState, {'name': name}}));

        console.log("name - ", name);
        console.log("arrayList - ", arrayList);
    }


    const optionsCLick = (title) => {
     
        if(title == "Today's Classes"){
            handleTodaysClassesOpen()
        }else if(title == "Time Table"){
            handleTimeTableOpen()
        }else if(title == "Notice Board"){
            handleNoticeBoardOpen()
        }else if(title == "Take Attendance"){

        }
    }

    return (
        <div>
            <h1>  Services</h1>

            <div className='attendance_container' style={{margin: 20}} >

                    <div>
                        {options.map((item) => (
                            <div className='options_container' onClick={() => optionsCLick(item.title)} >
                                <div className='options_title' >{item.title}</div>
                            </div>
                        ))}
                    </div>

                        {/* <div className='label-style' >Choose Section</div>
                        
                        <div style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%', alignItems: 'center', display: 'flex'}} >
                            <DropDown 
                                style={{width: 250}}
                                label="Section"
                                error={errors.testType}
                                handleChange={(val) => handleChange(val)}
                                data={[{value: 'Class10', title: 'Class 10'}, {value: 'Class11', title: 'Class 11'}, {value: 'Class12', title: 'Class 12'}]}
                            />
                            <div>
                                <IconButton
                                    title="Submit Details"
                                    icon={icons.attendance} 
                                    onClick={handleAttendanceOpen}
                                    buttonStyle={{backgroundColor: Colors.PRIMARY, marginRight: 5}}
                                />   
                            </div>
                        </div>
                        

                
                        
                        <div style={{marginTop: 20}} >
                            {value ?  
                                <div>
                                    {value == 'Class10'
                                    ?
                                        <div>
                                            {class10List.map((item) => (
                                                <div style={{flexDirection: 'row', alignItems: 'center', display: 'flex'}} >
                                                    <div style={{width: 200, marginTop: 10, fontSize: 24}} >{item.name}</div>
                                                    <div style={{width: 200, marginTop: 10, marginLeft: 20}} >
                                                        <CheckBox addToList={() => addtoList(class10List, item.name)} />
                                                    </div>
                                                </div>
                                            ))}
                                        </div>
                                    :
                                    value == 'Class11'
                                    ?
                                        <div>
                                            {class11List.map((item) => (
                                                <div style={{flexDirection: 'row', alignItems: 'center', display: 'flex'}} >
                                                    <div style={{width: 200, marginTop: 10, fontSize: 24}} >{item.name}</div>
                                                    <div style={{width: 200, marginTop: 10, marginLeft: 20}} >
                                                        <CheckBox addToList={() => addtoList(class11List, item.name)} />
                                                    </div>
                                                </div>
                                            ))}
                                        </div>
                                    :
                                    value == 'Class12'
                                    ?
                                        <div>
                                            {class12List.map((item) => (
                                                <div style={{flexDirection: 'row', alignItems: 'center', display: 'flex'}} >
                                                    <div style={{width: 200, marginTop: 10, fontSize: 24}} >{item.name}</div>
                                                    <div style={{width: 200, marginTop: 10, marginLeft: 20}} >
                                                        <CheckBox addToList={() => addtoList(class12List, item.name)} />
                                                    </div>
                                                </div>
                                            ))}
                                        </div>
                                    :
                                    null
                                    }
                                </div>
                            :
                            null
                            }
                        </div>

                        */}
                        {confirmation && <AttendaceDetails listLength={listLength} arrayList={arrayList} handleOpen={() => handleAttendanceOpen()} handleClose={() => handleAttendanceClose()} handleSubmit={() => handleAttendanceSubmit()}  />}

                        {todaysClassesModal && <TodaysClassesModal handleOpen={() => handleTodaysClassesOpen()} handleClose={() => handleTodaysClassesClose()}  />}

                        {timeTableModal && <TimeTableModal handleOpen={() => handleTimeTableOpen()} handleClose={() => handleTimeTableClose()}  />}

                        {noticeBoardModal && <NoticeBoardModal handleOpen={() => handleNoticeBoardOpen()} handleClose={() => handleNoticeBoardClose()}  />}
                    </div>
        </div>
    );
};



export default Attendance;