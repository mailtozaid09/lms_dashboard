import React, { useState, useEffect } from "react";

import "../../App.css";

import { BrowserRouter, Routes, Route, Link, Navigate } from "react-router-dom";

import SignIn from "../../screens/login/signin/SignIn";
import SignUp from "../../screens/login/signup/SignUp";

import Navbar from "../../components/navbar/Navbar";
import Sidebar from "../../components/sidebar/Sidebar";

import Dashboard from "../../screens/dashboard/dashboard/Dashboard";
import About from "../../screens/dashboard/About";

import Sessions from "../../screens/dashboard/sessions/Sessions";
import Users from "../../screens/dashboard/users/Users";
import Invoice from "../../screens/dashboard/invoice/Invoice";
import Settings from "../../screens/dashboard/settings/Settings";
import Concept from "../../screens/dashboard/concept/Concept";
import SideNavBar from "../../components/sideNavBar/SideNavBar";


import AllTest from "../../screens/dashboard/testDetails/AllTest/AllTest";
import AssignedTest from "../../screens/dashboard/testDetails/assignedTest/AssignedTest";
import AssignedTestPage from "../../screens/dashboard/testDetails/assignedTestPage/AssignedTestPage";
import TestDetails from "../../screens/dashboard/testDetails/testDetails/TestDetails";

import StartTest from "../../screens/dashboard/testDetails/startTest/StartTest";

// Parent
import ParentDashboard from "../../screens/parent/dashboard/ParentDashboard";
import Profile from "../../screens/parent/profile/Profile";
import EditProfile from "../../screens/parent/profile/edit/EditProfile";

//Student
import StudentProfile from "../../screens/student/profile/StudentProfile";
import EditStudentProfile from "../../screens/student/profile/edit/EditStudentProfile";

//Tutor
import TutorProfile from "../../screens/tutor/profile/TutorProfile";
import EditTutorProfile from "../../screens/tutor/profile/edit/EditTutorProfile";
import HomeScreen from "../HomeScreen";
import { useSelector } from "react-redux";
import StudentDashboard from "../student/dashboard/StudentDashboard";
import TutorDashboard from "../tutor/dashboard/TutorDashboard";
import Attendance from "../attendance/Attendance";
import ViewTestDetails from "../dashboard/testDetails/ViewTestDetails/ViewTestDetails";

const Navigator = () => {
  const [isLogin, setIsLogin] = useState(true);


  useEffect(() => {
    // storing input name
    // localStorage.setItem("name", "Zaid Ahmed");
      // const showNavBar = localStorage.getItem("showNavBar");
      // console.log("showNavBar => ", showNavBar);

      // if(userDetails.role[0] == 'ADMIN'){
      //     console.log("SET As ADMIN");
      // }else if(userDetails.role[0] == 'STUDENT'){
      //     console.log("SET As STUDENT");
      // }else if(userDetails.role[0] == 'PARENT'){
      //     console.log("SET As PARENT");
      // }else if(userDetails.role[0] == 'TUTOR'){
      //     console.log("SET As TUTOR");
      // }

  }, []);

  return (
    <BrowserRouter>
        <div
            style={{
            display: "flex",
            columnGap: "10px",
            backgroundColor: "#F3F5F7",
            }}
        >
            <SideNavBar /> 
            

            <main style={{ backgroundColor: "#F3F5F7" }}>

            <Routes>
                <Route path="*" element={<Navigate to="/signin" />} />
                <Route exact path='/signin' element={<SignIn/>} />

                {/* <Route path="/signin" element={<SignIn />} /> */}

                {/* <Route path="*" element={<Navigate to="/signup" />}  /> */}
                <Route path="/signup" element={<SignUp />} />

                {/* <Route path="/" element={<Dashboard />} /> */}
                <Route path="/dashboard" element={<Dashboard />} />

                <Route path="/home" element={<HomeScreen />} />

                <Route path="/users" element={<Users />} />

                <Route path="/allTest" element={<AllTest />} />
                <Route path="/settings" element={<Settings />} />
                <Route path="/invoice" element={<Invoice />} />

                <Route path="/conceptAnalytics" element={<Concept />} />

                <Route path="/assignedTest" element={<AssignedTest />} />
                <Route path="/assignedTestPage" element={<AssignedTestPage />} />

                <Route path="/testDetails" element={<TestDetails />} />
                <Route path="/startTest" element={<StartTest />} />

                <Route path="/viewTestDetails" element={<ViewTestDetails />} />
                

                
                <Route path="/about" element={<About />} />
                <Route path="/sessions" element={<Sessions />} />

                {/* PARENT DASHBOARD */}

                <Route path="/parentDashboard" element={<ParentDashboard />} />
                <Route path="/parentProfile" element={<Profile />} />
                <Route path="/editProfile" element={<EditProfile />} />

                {/* STUDENT DASHBOARD */}
                <Route path="/studentDashboard" element={<StudentDashboard />} />
                <Route path="/studentProfile" element={<StudentProfile />} />
                <Route path="/editStudentProfile" element={<EditStudentProfile />} />

                {/* TUTOR DASHBOARD */}

                <Route path="/tutorDashboard" element={<TutorDashboard />} />
                <Route path="/tutorProfile" element={<TutorProfile />} />
                <Route path="/editTutorProfile" element={<EditTutorProfile />} />

                {/* ATTENDANCE */}

                <Route path="/attendance" element={<Attendance />} />
            </Routes>
            </main>
        </div>
    </BrowserRouter>
  );
};

export default Navigator;
