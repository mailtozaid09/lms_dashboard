import React, { useState } from "react";
import "./signin.scss";

import { useNavigate } from "react-router-dom";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import TextInput from "../../../components/input/TextInput";
import IconButton from "../../../components/button/iconButton/IconButton";
import { Colors } from "../../../util/Colors";
import { icons } from "../../../util/Icons";
import { userLogin } from "../../../api/ApiManager";
import FileUploadButton from "../../../components/button/fileUploadButton/FileUploadButton";
import { useDispatch, useSelector } from "react-redux";
import { setIsLoading, setUserRole } from "../../../redux/actions";


const SignIn = () => {

    const users = useSelector(state=> state.users)
    const dispatch = useDispatch()
    const navigate = useNavigate();

    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});
    const [buttonLoader, setButtonLoader] = useState(false);

    const [showPassword, setShowPassword] = useState(false);
    

    const [userName, setuserName] = useState('dsadsa');
    const [userPass, setuserPass] = useState('dsdswws');


    const onChange = ({ name, value }) => {
        console.log("users==> ", users);
        setForm({ ...form, [name]: value });
        setErrors({})
    };

  // const validateEmail = (email) =>  {
  //     var validRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  //     if (email.match(validRegex)) {
  //         return true;
  //     } else {
  //         setErrors((prev) => {
  //             return {...prev, email: 'Please enter a valid email'}
  //         })
  //         return false;
  //     }
  // }


//   const [values, setValues] = React.useState<State>({
//     amount: '',
//     password: '',
//     weight: '',
//     weightRange: '',
//     showPassword: false,
//   });

//   const handleChange =  (prop: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
//       setValues({ ...values, [prop]: event.target.value });
//     };

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };


    const notify = (text, type) => {
        if(type == 'error'){
            toast.error(text, {
                position: toast.POSITION.TOP_RIGHT,
                className: "toast",
            });
        }else if(type == 'success'){
            toast.success(text, {
                position: toast.POSITION.TOP_RIGHT,
                className: "toast",
            });
        }     
    };

    const loginButton = (e) => {
        e.preventDefault();
      
        setButtonLoader(true)
        console.log("-----SUBMIT----");

        var isEmailValid = false;
        var isNameValid = false;

        if (!form.password) {
            setButtonLoader(false)
            console.log("Please enter a valid password");

            setErrors((prev) => {
                return { ...prev, password: "Please enter a valid password" };
            });
        }

        if (!form.userName) {
            setButtonLoader(false)
            console.log("Please enter a valid email");

            setErrors((prev) => {
                return { ...prev, userName: "Please enter a valid email" };
            });
            } else {
            // isEmailValid = validateEmail(form.email)
        }

        if (form.password && form.userName) {
            navigate("/dashboard");
            console.log("-------- USER DETAILS --------");
            console.log(form);
            console.log(form.userName);
            console.log(form.password);

            userLogin(form)
            .then((resp) => {
                console.log("userLogin resp => ", resp);
                console.log("status => ", resp.status);
                if(resp.status == '200'){
                    dispatch(setIsLoading(true));
                    var userDetails = resp.data
                    var token = resp.data.token

                    console.log("userDetails role => ", userDetails.role[0]);
                    console.log("token => ", token);

                    dispatch(setUserRole(userDetails.role[0]));

                    localStorage.setItem('auth_token', JSON.stringify(token));
                    localStorage.setItem('user_details', JSON.stringify(userDetails));
                    
                    navigate("/dashboard");
                    
                    notify(resp.message, 'success')


                    setTimeout(() => {
                        dispatch(setIsLoading(false));
                    }, 1000);

                }else{
                    notify(resp.message, 'error')
                }
                
                setButtonLoader(false)
            })
            .catch((err) => {
                console.log("err = ", err);
                setButtonLoader(false)
            })
        }
    };


    return (
        <div className="signin-page">
            <div className="logo-container"
            style={{ 
                alignItems: 'center',
                justifyContent: 'center',
                display: 'flex',
            }}>
                <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}} >
                    <img src={icons.school_logo} style={{height: 200, width: 200, borderWidth: 1, borderRadius: 10, marginBottom: 50}} />
                    <h1>Jai Tulsi Vidhya Vihar School</h1>
                </div>
            </div>
            <div className="content-container">
                <div>
                <div className="heading">Log In</div>


                    <div className="form_container">
                        <div className="ml-10 bold mb-20">Login with email address</div>

                        <div className="fd-c d-f full-width mt-20">
                        <div>
                            <div className="input_label ml-10">Email ID</div>
                            <TextInput 
                                style={{ width: "100%" }} 
                                label="Email ID" 
                                value={form.userName}
                                error={errors.userName}
                                onChange={event => onChange({name: 'userName', value: event.target.value})}
                            />
                        </div>
                        <div>
                            <div className="input_label ml-10">Password</div>
                            <TextInput 
                                style={{ width: "100%" }} 
                                label="Password" 
                                value={form.password}
                                error={errors.password}
                                password={true}
                                showPassword={showPassword}
                                handleClickShowPassword={() => handleClickShowPassword()}
                                onChange={event => onChange({name: 'password', value: event.target.value})}
                            />
                        </div>
                        </div>

                        <div>
                        <IconButton
                            title="Login"
                            buttonLoader={buttonLoader}
                            onClick={(e) => {
                                loginButton(e);
                                //signIn(e)
                            }}
                            textStyle={{ color: Colors.WHITE, fontWeight: "bold" }}
                            buttonStyle={{
                            width: "100%",
                            marginTop: 30,
                            marginLeft: 10,
                            height: 50,
                            padding: 10,
                            backgroundColor: Colors.PRIMARY,
                            }}
                        />
                        </div>
                    </div>
                </div>

                <div className="signin_or_signup_container">
                <div
                    onClick={() => {
                    navigate("/signup");
                    }}
                    className="signin_or_signup"
                >
                    Don't have an account yet?{" "}
                    <div className="signin_or_signup_text">Sign Up</div>{" "}
                </div>
                </div>
            </div>

            <ToastContainer
                hideProgressBar={true}
                newestOnTop={false}
                closeButton={false}
            />
        </div>
    );
};

export default SignIn;
