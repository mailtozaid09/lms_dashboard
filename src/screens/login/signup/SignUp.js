import React,{useState} from 'react'
import './signup.scss'
import 'react-toastify/dist/ReactToastify.css';
import ProgressStepBar from './progress/ProgressStepBar'
import { useNavigate } from 'react-router-dom';


const SignUp = () => {

    const navigate = useNavigate();

    const step2Validator = () => {
    return true
    }
    
    const step3Validator = () => {
        return true
    }
    
    const onFormSubmit = () => {
    // handle the submit logic here
    // This function will be executed at the last step
    // when the submit button (next button in the previous steps) is pressed
    }

    return (    
        <div className='signup-page' >
            <div className='logo-container' >
                <div  ></div>
            </div>
            <div className='content-container' >
                <div>
                    <div className='heading mb-20'>Sign Up</div>
                    <ProgressStepBar />
                </div>

                <div className='signin_or_signup_container' >
                    <div onClick={() => {navigate('/signin')}} className='signin_or_signup' >Already have an account? <div className='signin_or_signup_text' >Sign In</div> </div>
                </div>
            </div>

        </div>
    )
}

export default SignUp