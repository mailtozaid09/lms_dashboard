import React,{useState} from 'react'
import { useNavigate } from 'react-router-dom';
import { userSignup } from '../../../../api/ApiManager';
import IconButton from '../../../../components/button/iconButton/IconButton';
import CheckBox from '../../../../components/checkbox/CheckBox';
import DropDown from '../../../../components/dropdown/DropDown';
import TextInput from '../../../../components/input/TextInput';
import { Colors } from '../../../../util/Colors';
import { icons } from '../../../../util/Icons';
import './progress.scss'

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const service = [
    {
        id: 1,
        label: 'SAT / ACT Prep',
        value: 'SAT / ACT Prep',
    },
    {
        id: 1,
        label: 'Subject Tutoring',
        value: 'Subject Tutoring',
    },
    {
        id: 1,
        label: 'AP Tutoring',
        value: 'AP Tutoring',
    },
    {
        id: 1,
        label: '2 + 2 Program',
        value: '2 + 2 Program',
    },
]


const selectAny = [
    {
        id: 1,
        label: 'I am only looking to improve my scores, nothing else.',
        value: 'I am only looking to improve my scores, nothing else.',
    },
    {
        id: 1,
        label: 'I am concerned about my motivation to learn.',
        value: 'I am concerned about my motivation to learn.',
    },
    {
        id: 1,
        label: 'I have accommodations.',
        value: 'I have accommodations.',
    },
    {
        id: 1,
        label: 'I want a mentor that can guide me through the learning process.',
        value: 'I want a mentor that can guide me through the learning process.',
    },
]


const hearAboutUs = [
    {
        id: 1,
        label: 'Referred by a friend or family',
        value: 'Referred by a friend or family',
    },
    {
        id: 1,
        label: 'Online Search',
        value: 'Online Search',
    },
    {
        id: 1,
        label: 'Social Media',
        value: 'Social Media',
    },
    {
        id: 1,
        label: 'College Counselor',
        value: 'College Counselor',
    },
    {
        id: 1,
        label: 'Others',
        value: 'Others',
    },
]

const StepComponent1 = ({selectedImg, onChangeImg}) => {
    return (
        <div className='progress_container fd-c' >
            <div className='signupas' >Sign up as {selectedImg} </div>
            <div className='d-f ai-c mt-20' >
                <div onClick={() => onChangeImg('Student')} >
                    <img src={selectedImg == 'Student' ? icons.studentActive : icons.studentInactive} style={{height: 180, width: 150, marginRight: 10, borderRadius: 10}} />
                </div>
                <div onClick={() => onChangeImg('Parent')} >
                    <img src={selectedImg == 'Parent' ?  icons.parentActive : icons.parentInactive} style={{height: 180, width: 150, marginLeft: 10, borderRadius: 10}} />
                </div>
            </div>
        </div>
    )
}


const StepComponent2 = ({form, onChange, errors}) => {

    return (
        <div className='progress_container fd-c' style={{width: 500}} >
            <div className='fd-r jc-sb d-f full-width' >
                <div>
                    <div className='input_label ml-10' >First Name</div>
                    <TextInput
                        style={{width: '100%'}} 
                        label="First Name" 
                        value={form?.firstName}
                        error={errors?.firstName}
                        onChange={event => onChange({name: 'firstName', value: event.target.value})}
                    />
                </div>
                <div>
                    <div className='input_label ml-10' >Last Name</div>
                    <TextInput
                        style={{width: '100%'}} 
                        label="Last Name" 
                        value={form?.lastName}
                        error={errors?.lastName}
                        onChange={event => onChange({name: 'lastName', value: event.target.value})}
                    />
                </div>
            </div>

            <div className='input_label ml-10' >Email Address</div>
            <TextInput
                style={{width: '100%'}} 
                label="Email Address" 
                value={form?.email}
                error={errors?.email}
                onChange={event => onChange({name: 'email', value: event.target.value})}
            />

            <div className='input_label mt-10 ml-10' >Phone Number</div>
            <TextInput
                style={{width: '100%'}} 
                label="Phone Number" 
                value={form?.mobile}
                error={errors?.mobile}
                onChange={event => onChange({name: 'mobile', value: event.target.value})}
            />

            <div className='input_label ml-10' >Create Password</div>
            <TextInput
                style={{width: '100%'}} 
                label="Create Password" 
                value={form?.password}
                error={errors?.password}
                onChange={event => onChange({name: 'password', value: event.target.value})}
            />

            <div className='input_label ml-10' >Your Grade</div>
            <DropDown
                style={{width: 200, marginLeft: 10, marginTop: 10}}
                label="Your Grade"
                data={[{value: 'Class 10', title: 'Class 10'}, {value: 'Class 11', title: 'Class 11'}, {value: 'Class 12', title: 'Class 12'}]}
            />
            
        </div>
    )
}


const StepComponent3 = ({}) => {
    return (
        <div className='progress_container fd-c' >
            <div className='input_label ml-10' >Phone Number</div>
            <TextInput label="Phone Number" />

            <div className='input_label mb-10 mt-10' >What service are you seeking?</div>

            <div style={{ flexDirection: 'row', flexWrap: 'wrap', display: 'flex', }} >
                {service.map((item) => (
                    <div style={{width: 300}} >
                        <CheckBox label={item.label} value={item.value} />
                    </div>
                ))}
            </div>
            
        </div>
    )
}


const StepComponent4 = ({}) => {
    return (
        <div className='progress_container fd-c' >
            <div className='input_label ml-10' >Do you have any PSAT / P-ACT scores to share? How are your grades in school?</div>
            <TextInput label="Enter Value" />

            <div className='input_label mb-10 mt-10 ml-10' >What service are you seeking?</div>
            <div style={{marginLeft: 10, flexDirection: 'row', flexWrap: 'wrap', display: 'flex', }} >
                {service.map((item) => (
                    <div style={{width: 300}} >
                        <CheckBox label={item.label} value={item.value} />
                    </div>
                ))}
            </div>

            
            <div className='input_label mb-10 mt-10 ml-10' >Select if any of these apply to you</div>
            <div style={{ marginLeft: 10,}} >
                {selectAny.map((item) => (
                    <div style={{}} >
                        <CheckBox label={item.label} value={item.value} />
                    </div>
                ))}
            </div>
            
        </div>
    )
}


const StepComponent5 = ({}) => {
    return (
        <div className='progress_container fd-c' >
           

            <div className='input_label mb-10 mt-10 ml-10' >How did you hear about us?</div>

            <div style={{marginLeft: 10, flexDirection: 'row', flexWrap: 'wrap', display: 'flex', }} >
                {hearAboutUs.map((item) => (
                    <div style={{width: 300}} >
                        <CheckBox label={item.label} value={item.value} />
                    </div>
                ))}
            </div>

            <div className='input_label ml-10' >Please enter the subscription code required to access Seven Square Learning and starting prep.</div>
            <TextInput label="Enter Subscription Code" />

            <div style={{marginLeft: 10, width: 300}} >
                <CheckBox label={"I don't have one."} value={"I don't have one."} />
            </div>
            
        </div>
    )
}


const ProgressStepBar = () => {

    const navigate = useNavigate();


    const [currectActive, setCurrectActive] = useState(1);

    const [selectedImg, setSelectedImg] = useState('');
    
    const [stepForm2, setStepForm2] = useState({});
    const [stepForm3, setStepForm3] = useState({});
    const [stepForm4, setStepForm4] = useState({});
    const [stepForm5, setStepForm5] = useState({});
    
    const [errors, setErrors] = useState({});

    const [buttonLoader, setButtonLoader] = useState(false);


    const [circle, setCircle] = useState([
        {id: 1, circle: 'active', bar: 'inactive'},
        {id: 2, circle: 'active', bar: 'inactive'},
        {id: 3, circle: 'active', bar: 'inactive'},
        {id: 4, circle: 'active', bar: 'inactive'},
        {id: 5, circle: 'active', bar: 'inactive'}
    ])


    const notify = (text, type) => {
        if(type == 'error'){
            toast.error(text, {
                position: toast.POSITION.TOP_RIGHT,
                className: "toast",
            });
        }else if(type == 'success'){
            toast.success(text, {
                position: toast.POSITION.TOP_RIGHT,
                className: "toast",
            });
        }     
    };


    const onChangeDetailsStep2 = ({ name, value }) => {
        setStepForm2({ ...stepForm2, [name]: value });
        setErrors({})

    };

    const onChangeStep = (id) => {
        console.log("id => ", id);

        if(id >0 && id<circle.length+1){
            setCurrectActive(id)
        }
    }


    const onSubmitDetails = () => {
        console.log("onSubmitDetails => ", stepForm2);

        setButtonLoader(true)
        onChangeStep(currectActive+1);

        var data = {
            name: stepForm2.firstName + " " + stepForm2.lastName,
            email: stepForm2.email,
            mobile: stepForm2.mobile,
            password: stepForm2.password,
            role:[selectedImg.toUpperCase()]
        }

        console.log("data => " , data);
 
        userSignup(data)
        .then((resp) => {
            console.log("userSignup resp => ", resp);
            console.log("status => ", resp.status);
            if(resp.status == '200' || resp.status == '201'){
                navigate("/dashboard");
                notify(resp.message, 'success')
            }else{
                notify(resp.message, 'error')
            }
            
            setButtonLoader(false)
        })
        .catch((err) => {
            console.log("err = ", err);
            setButtonLoader(false)
        })

    }

    return (    
        <div>
           <div className='progress_container'>
                {circle.map((item, index) => (
                    <div className='container'>
                        <div className={`circle_container ${item.id <= currectActive ? 'completed' : null} `}>
                            <div>{item.id}</div>
                        </div>
                        {item.id < circle.length ? <div className={`circle_bar_container active `} /> : null}
                    </div>
                ))}
            </div>

            <div>
                {currectActive == 1
                ?
                <StepComponent1 
                    selectedImg={selectedImg}
                    onChangeImg={(val) => setSelectedImg(val)}
                />
                :
                currectActive == 2
                ?
                <StepComponent2
                    form={stepForm2}
                    onChange={(name, value) => onChangeDetailsStep2(name, value)}
                />
                :
                currectActive == 3
                ?
                <StepComponent3 
                    // selectedImg={selectedImg}
                    // onChangeImg={(val) => setSelectedImg(val)}
                />
                :
                currectActive == 4
                ?
                <StepComponent4
                    // selectedImg={selectedImg}
                    // onChangeImg={(val) => setSelectedImg(val)}
                />
                :
                currectActive == 5
                ?
                <StepComponent5
                    // selectedImg={selectedImg}
                    // onChangeImg={(val) => setSelectedImg(val)}
                />
                :
                null
                }
            </div>

            <div className='fd-r d-f full-width mt-20' >
                    <IconButton
                        title="Back"
                        onClick={() => onChangeStep(currectActive-1)} 
                        iconStyle={{height: 12, width:12}}
                        textStyle={{color: Colors.WHITE, fontWeight: 'bold'}}
                        buttonStyle={{width: 130, marginRight: 10,  height: 50, padding: 10, backgroundColor: Colors.PRIMARY, marginRight: 5}}
                    />

                    {currectActive == circle.length
                    ?
                        <IconButton
                            title="Submit"
                            onClick={() => {
                                onSubmitDetails()
                            }}
                            buttonLoader={buttonLoader}
                            iconStyle={{height: 12, width:12}}
                            textStyle={{color: Colors.WHITE, fontWeight: 'bold'}}
                            buttonStyle={{width: 130, marginLeft: 10,  height: 50, padding: 10, backgroundColor: Colors.PRIMARY, marginRight: 5}}
                        />
                    :
                    <IconButton
                        title="Next"
                        onClick={() => onChangeStep(currectActive+1)}
                        iconStyle={{height: 12, width:12}}
                        textStyle={{color: Colors.WHITE, fontWeight: 'bold'}}
                        buttonStyle={{width: 130, marginLeft: 10,  height: 50, padding: 10, backgroundColor: Colors.PRIMARY, marginRight: 5}}
                    />
                    }
                    
            </div>

            <ToastContainer
                hideProgressBar={true}
                newestOnTop={false}
                closeButton={false}
            />
        </div>
    )
}

export default ProgressStepBar
