import React from 'react';
import IconButton from '../../../components/button/iconButton/IconButton';
import { Colors } from '../../../util/Colors';
import { icons } from '../../../util/Icons';
import './studentProfile.scss'

import { useNavigate } from 'react-router-dom';
import { Score } from '@mui/icons-material';
import ScoreCarousel from '../../../components/carousel/Score/ScoreCarousel';

const profileDetails = {
    id: 1,
    timeZone: 'IST (GMT+5:30)',
    birthYear: '1984',
    suubscriptionType: '3 Months Trial',
    industry: 'Medical',
    address: '1315 N State St, Ukiah, California,Postal Code- 95482 U',
    email: 'ranasapna78@gmail.com',
    whatsapp: '+91 012-3456-789',
}

const valuesDetails = [
    {
        id: 1,
        label: 'Values',
        data: [
            {
                id: 1,
                interestName: 'Honest',
                bgColor: '#A5A3F6',
                icon: icons.honest,
            },
            {
                id: 1,
                interestName: 'Confident',
                bgColor: '#85C396',
                icon: icons.confident,
            },
            {
                id: 1,
                interestName: 'Brave',
                bgColor: '#FFA7C1',
                icon: icons.brave,
            },
            {
                id: 1,
                interestName: 'Honest',
                bgColor: '#A5A3F6',
                icon: icons.honest,
            },
            {
                id: 1,
                interestName: 'Confident',
                bgColor: '#85C396',
                icon: icons.confident,
            },
            {
                id: 1,
                interestName: 'Brave',
                bgColor: '#FFA7C1',
                icon: icons.brave,
            },
        ]
    }
];

const interestDetails = [
    {
        id: 1,
        label: 'Values',
        data: [
            {
                id: 1,
                interestName: 'Video Game',
                bgColor: '#F6D0A3',
                icon: icons.game,
            },
            {
                id: 1,
                interestName: 'Cooking',
                bgColor: '#F1EAAC',
                icon: icons.cooking,
            },
            {
                id: 1,
                interestName: 'Yoga',
                bgColor: '#AADFEB',
                icon: icons.yoga,
            },
            {
                id: 1,
                interestName: 'Video Game',
                bgColor: '#F6D0A3',
                icon: icons.game,
            },
            {
                id: 1,
                interestName: 'Cooking',
                bgColor: '#F1EAAC',
                icon: icons.cooking,
            },
            {
                id: 1,
                interestName: 'Yoga',
                bgColor: '#AADFEB',
                icon: icons.yoga,
            },
        ]
    }
];


const Profile = () => {

    const navigate = useNavigate();

    return (
        <div className='student_profile_container' >
            <div className='profile_header' >
                <div className='profile_img_container' >
                    <div>
                        <img src={icons.expertProfile} className="user_img" />
                    </div>
                    <div className='user_name' >Student Phill Brown</div>
                    <div className='user_description' >Phill Brown</div>
                </div>

                <div className='edit_container'>
                    <IconButton
                        title="Back"
                        icon={icons.left_arrow} 
                        onClick={() => {navigate(-1)}} 
                        //onClick={() => {navigate('/editStudentProfile')}} 
                        iconStyle={{height: 20, width:20, marginBottom: 2}}
                        textStyle={{fontSize: 18, marginRight: 20, color: '#636363', fontWeight: 'bold'}}
                        buttonStyle={{padding: 10, paddingLeft: 20, paddingRight: 20, backgroundColor: Colors.WHITE, marginRight: 5}}
                    />
                </div>
            </div>

            <div className='student_details_container' >
                <div>
                    <div className='details_cardC1 fd-c d-f '>
                        <div className=' d-f fd-c' >
                            <div className='details_heading '>Birth Year</div>
                            <div className='details_sub_heading'>{profileDetails.timeZone}</div>
                        </div>

                        <div className=' d-f fd-c' >
                            <div className='details_heading mt-20'>Subjects</div>
                            <div className='details_sub_heading'>{profileDetails.timeZone}</div>
                        </div>
                    </div>
                </div>
                <div>
                    <div className='student_associated_details' >
                        <div className='associated_img_container' >
                            <div className='associated_user' >Associated Parent</div>
                            <img src={icons.expertProfile} className="associated_user_img" />
                            <div className='associated_user_name' >Phill Brown</div>
                            <div className='view_profile' >
                                <div style={{fontSize: 13}} >View Profile</div>
                                <img src={icons.right_arrow} style={{height: 8, width: 8, marginLeft: 8}} />
                            </div>
                        </div>
                    </div>
                </div>
                <div className='full-width'>
                    <div className='details_card1 ai-c d-f'>
                        <div className='fd-c ai-c d-f full-width' >
                            <div className=' d-f fd-c' >
                                <div className='details_heading'>Contact</div>
                            </div>
                            <div className='jc-sb d-f fd-r full-width' >
                                <div className='ai-c d-f fd-c' >
                                    <img src={icons.email} style={{height: 22, width: 22}} />
                                    <div className='details_sub_heading'>{profileDetails.email}</div>
                                </div>
                                
                                <div className='ai-c d-f fd-c' >
                                    <img src={icons.whatsapp} style={{height: 22, width: 22}} />
                                    <div className='details_sub_heading'>{profileDetails.whatsapp}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='details_card1 ai-c d-f mt-20'>
                        <div className='jc-sb ai-c d-f full-width' >
                            <div className=' d-f fd-c' >
                                <div className='details_heading'>PSAT / P-ACT Scores</div>
                                <div className='details_sub_heading'>{profileDetails.timeZone}</div>
                            </div>
                            <div className=' d-f fd-c details_width' >
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div className='details_cardC1 fd-c d-f ml-20'>
                        <div className=' d-f fd-c' >
                            <div className='details_heading '>Time Zone</div>
                            <div className='details_sub_heading'>{profileDetails.timeZone}</div>
                        </div>

                        <div className=' d-f fd-c' >
                            <div className='details_heading mt-20'>Subsciption</div>
                            <div className='details_sub_heading'>{profileDetails.suubscriptionType}</div>
                        </div>

                        <div className=' d-f fd-c' >
                            <div className='details_heading mt-20'>Accomodations</div>
                            <div className='details_sub_heading'>N/A</div>
                        </div>
                    </div>
                </div>
            </div>
           
            <div className='student_details_container1' >
                <div>
                    <div className='details_card_values fd-c d-f '>
                        <div className=' d-f fd-c ai-c' >
                            <div className='details_heading '>Values</div>
                        </div>

                        <div className='values_container full-width mt-20'  >
                            {valuesDetails && valuesDetails.map((item) => (
                                <div className='' >
                                    {item.data.map((el) => (
                                        <div className='ai-c jc-c d-f fd-c' >
                                            <div style={{height: 70, width: 70, borderRadius: 35, marginTop: 10, backgroundColor: el.bgColor }} className='ai-c jc-c d-f'>
                                                <img src={el.icon} style={{height: 30, width: 30}} />
                                            </div>
                                            <div className='mt-10' >{el.interestName}</div>
                                        </div>
                                    ))}
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
                <div className='full-width'>
                    <div className="mb-20">
                        <ScoreCarousel />
                    </div>
                    <div>
                        <ScoreCarousel />
                    </div>
                </div>
                <div>
                    <div className='details_card_values ai-c fd-c d-f mr-20'>
                        <div className=' d-f fd-c ai-c' >
                            <div className='details_heading '>Interest</div>
                        </div>

                        <div className='values_container full-width mt-20'  >
                            {interestDetails && interestDetails.map((item) => (
                                <div className='' >
                                    {item.data.map((el) => (
                                        <div className='ai-c jc-c d-f fd-c' >
                                            <div style={{height: 70, width: 70, borderRadius: 35, marginTop: 10, backgroundColor: el.bgColor }} className='ai-c jc-c d-f'>
                                                <img src={el.icon} style={{height: 30, width: 30}} />
                                            </div>
                                            <div className='mt-10' >{el.interestName}</div>
                                        </div>
                                    ))}
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Profile;