import React from 'react';
import { icons } from '../../../util/Icons';
import './tutorDash.scss'

import ProgressBar from "@ramonak/react-progress-bar";
import IconButton from '../../../components/button/iconButton/IconButton'
import { Colors } from '../../../util/Colors';

import ConceptAnalytics from '../../../components/graphs/ConceptAnalytics'
import Carousel1 from '../../../components/carousel/Carousel1/Carousel1';
import TutorCarousel from '../../../components/carousel/TutorCarousel/TutorCarousel';

const profileDetails = [
    {
        id: 1,
        label: 'Latest Tests Assigned',
        data: [
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
            {
                id: 1,
                name: 'SAT B2',
                dueDate: 'June 20, 2022',
                button: 'view',
                image: icons.expertProfile,
            },
        ]
    },
]

const todaysSchedule = [
    {
        id: 1,
        label: 'Today’s Schedule',
        data: [
            {
                id: 1,
                name: 'Test Prep',
                date: 'June 20, 2022',
                time: '16:15 - 17:00 IST',
                withStudent: 'Kate Johnson',
            },
            {
                id: 1,
                name: 'Test Prep',
                date: 'June 20, 2022',
                time: '16:15 - 17:00 IST',
                withStudent: 'Kate Johnson',
            },
            {
                id: 1,
                name: 'Test Prep',
                date: 'June 20, 2022',
                time: '16:15 - 17:00 IST',
                withStudent: 'Kate Johnson',
            },
            {
                id: 1,
                name: 'Test Prep',
                date: 'June 20, 2022',
                time: '16:15 - 17:00 IST',
                withStudent: 'Kate Johnson',
            },
            {
                id: 1,
                name: 'Test Prep',
                date: 'June 20, 2022',
                time: '16:15 - 17:00 IST',
                withStudent: 'Kate Johnson',
            },
            {
                id: 1,
                name: 'Test Prep',
                date: 'June 20, 2022',
                time: '16:15 - 17:00 IST',
                withStudent: 'Kate Johnson',
            },

        ]
    },
]


const latestStudent = [
    {
        id: 1,
        name: 'Sam',
        dueDate: 'June 20, 2022',
        button: 'view',
        image: icons.expertProfile,
    },
    {
        id: 1,
        name: 'Lily',
        dueDate: 'June 20, 2022',
        button: 'view',
        image: icons.expertProfile,
    },
    {
        id: 1,
        name: 'Sam',
        dueDate: 'June 20, 2022',
        button: 'view',
        image: icons.expertProfile,
    },
    {
        id: 1,
        name: 'Lily',
        dueDate: 'June 20, 2022',
        button: 'view',
        image: icons.expertProfile,
    },
    {
        id: 1,
        name: 'Sam',
        dueDate: 'June 20, 2022',
        button: 'view',
        image: icons.expertProfile,
    },
    {
        id: 1,
        name: 'Lily',
        dueDate: 'June 20, 2022',
        button: 'view',
        image: icons.expertProfile,
    },
    {
        id: 1,
        name: 'Sam',
        dueDate: 'June 20, 2022',
        button: 'view',
        image: icons.expertProfile,
    },
    {
        id: 1,
        name: 'Lily',
        dueDate: 'June 20, 2022',
        button: 'view',
        image: icons.expertProfile,
    },
    {
        id: 1,
        name: 'Sam',
        dueDate: 'June 20, 2022',
        button: 'view',
        image: icons.expertProfile,
    },
    {
        id: 1,
        name: 'Lily',
        dueDate: 'June 20, 2022',
        button: 'view',
        image: icons.expertProfile,
    },
]

const TutorDashboard = () => {
    return (
        <div>
            <h1>Tutor Dashboard</h1>

            <div className='tutor-dash-container' >
                <div className='tutor-dash-container1' >

                    <div className='ai-c d-f full-width' >
                      
                        <div className='tutor-profile-deatils full-width'  >
                            <div className='label' >Latest Students</div>
                            <div className='latest_container full-width' style={{ width: 800}} >
                                <div className='fd-r d-f full-width' style={{}} >
                                {latestStudent && latestStudent.map((item) => (
                                    <div className='fd-r  d-f ' >
                                        <div className='fd-c jc-c ai-c d-f mr-20' >
                                            <img src={item.image} className="latest_user_img" />
                                            <div className='' >
                                                {item.name}
                                            </div>
                                        </div>
                                    </div>
                                ))}

                                </div>
                            </div>
                           
                        </div>
                    </div>

                                    
                    <div className='jc-se ai-c d-f full-width mt-20'> 
                        <div className='small_card1' >
                            <div className='fd-c d-f ai-c jc-c' style={{backgroundColor: '#FFFFFF20', padding: 12, borderRadius: 10}} >
                                <div className='' style={{fontSize: 34, color: 'white'}} >14</div>
                                <div className='' style={{fontSize: 20, color: 'white'}}  >Hours</div>
                            </div>
                            <div className='fd-c d-f jc-c ml-20' >
                                <div className='' style={{fontSize: 28, color: 'white'}}  >Total Hours</div>
                                <div className='' style={{fontSize: 20, color: 'white'}}  >this month</div>
                            </div>
                        </div>
                        <div className='small_card2' >
                            <div className='fd-c d-f ai-c jc-c' style={{backgroundColor: '#FFFFFF20', padding: 12, borderRadius: 10}} >
                                <div className='' style={{fontSize: 34, color: 'white'}} >2800</div>
                                <div className='' style={{fontSize: 20, color: 'white'}}  >USD</div>
                            </div>
                            <div className='fd-c d-f jc-c ml-20' >
                                <div className='' style={{fontSize: 28, color: 'white',}}  >Estimated Payment</div>
                                <div className='' style={{fontSize: 20, color: 'white'}}  >this month</div>
                            </div>
                        </div>
                    </div>

                    <div className='tutor-profile-deatils full-width'  >
                        {todaysSchedule && todaysSchedule.map((item) => (
                            <div>
                                <div className='label' >{item.label}</div>
                                <div className='container full-width' style={{height: 320}} >
                                {item.data && item.data.map((el) => (
                                    <div className='jc-sb ai-c d-f full-width' style={{backgroundColor: '#e5ebf7', borderRadius: 10, marginBottom: 10, padding: 10, paddingLeft: 20, paddingRight: 20 }} >
                                        <div className='fd-r ai-c d-f ' >
                                            <div className='text-container mr-20 ' >
                                                <div className='bold' style={{color: 'rgba(57, 53, 243, 0.9)', marginBottom: 10, }} >
                                                    {el.name}
                                                </div>
                                                <div className='' >
                                                    with {el.withStudent}
                                                </div>
                                            </div>
                                            <div className='text-container ml-20' >
                                                <div className='' >
                                                    {el.date}
                                                </div>
                                                <div className='' >
                                                    {el.time}
                                                </div>
                                            </div>
                                        </div>
                                        <div className='' >
                                            <IconButton 
                                                title="Edit Session"
                                                textStyle={{color: '#000000',}}
                                                buttonStyle={{backgroundColor: '#DFDFDF',  width: 140, marginBottom: 2}}
                                            />
                                            <IconButton 
                                                title="Meeting Link"
                                                buttonStyle={{backgroundColor: Colors.MUSTURD, width: 140, marginTop: 2}}
                                            />
                                        </div>
                                    </div>
                                ))}
                                </div>
                            </div>
                        ))}

                    </div>
                </div>



                <div className='tutor-dash-container2' >
                    <div className='your-profile-container' >
                        <h1>Complete your Profile</h1>
                        <img src={icons.right_arrow} style={{height: 24, width: 24}} />
                    </div>

                    <div className='jc-sb ai-c d-f full-width' >
                        <div>Profile Status</div>
                        <div>65%</div>
                    </div>

                    <div className='full-width ' style={{marginTop: 10}}>
                        <ProgressBar 
                            completed={65} 
                            customLabel=" "  
                            bgColor='#62DD43'
                            height="12px"
                        />
                    </div>


                    <div className='tutor-profile-deatils full-width'  >
                        {profileDetails && profileDetails.map((item) => (
                            <div>
                                <div className='label' >{item.label}</div>
                                <div className='container full-width' style={{height: 580}} >
                                {item.data && item.data.map((el) => (
                                    <div className='jc-sb ai-c d-f full-width' style={{marginBottom: item.data && item.data.length === 1 ? 0 : 12}} >
                                        <div className='fd-r ai-c d-f ' >
                                            <div className=' mr-20' >
                                                <img src={el.image} className="user_img" />
                                            </div>
                                            <div className='text-container' >
                                                <div className='' >
                                                    {el.name}
                                                </div>
                                                <div className='' >
                                                    {el.dueDate}
                                                </div>
                                            </div>
                                        </div>
                                        <div className='' >
                                            <IconButton 
                                                title="VIEW"
                                                buttonStyle={{backgroundColor: Colors.MUSTURD, width: 120}}
                                            />
                                        </div>
                                    </div>
                                ))}
                                </div>
                            </div>
                        ))}

                    </div>
                </div>
            </div>

        </div>
    );
};

export default TutorDashboard;