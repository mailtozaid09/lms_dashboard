import React from 'react';
import IconButton from '../../../../components/button/iconButton/IconButton';
import { Colors } from '../../../../util/Colors';
import { icons } from '../../../../util/Icons';
import './editTutorProfile.scss'

import { useNavigate } from 'react-router-dom';
import { Score } from '@mui/icons-material';
import ScoreCarousel from '../../../../components/carousel/Score/ScoreCarousel';
import BackButton from '../../../../components/button/BackButton/BackButton';

const profileDetails = {
    id: 1,
    timeZone: 'IST (GMT+5:30)',
    birthYear: '1984',
    suubscriptionType: '3 Months Trial',
    industry: 'Medical',
    address: '1315 N State St, Ukiah, California,Postal Code- 95482 U',
    email: 'ranasapna78@gmail.com',
    whatsapp: '+91 012-3456-789',
    linkedin: 'linkedin.com/in/sha-shanks/',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor rhoncus dolor purus non enim praesent elementum facilisis leo, vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in ornare quam viverra orci sagittis eu volutpat odio facilisis mauris sit amet massa vitae tortor condimentum lacinia quis vel eros donec ac odio tempor orci dapibus ultrices in iaculis nunc sed augue lacus, viverra vitae congue eu, consequat ac felis donec et odio pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus faucibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor rhoncus dolor purus non enim praesent elementum facilisis leo, vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in ornare quam viverra',
}

const valuesDetails = [
    {
        id: 1,
        label: 'Values',
        data: [
            {
                id: 1,
                interestName: 'Honest',
                bgColor: '#A5A3F6',
                icon: icons.honest,
            },
            {
                id: 1,
                interestName: 'Confident',
                bgColor: '#85C396',
                icon: icons.confident,
            },
            {
                id: 1,
                interestName: 'Brave',
                bgColor: '#FFA7C1',
                icon: icons.brave,
            },
            {
                id: 1,
                interestName: 'Honest',
                bgColor: '#A5A3F6',
                icon: icons.honest,
            },
            {
                id: 1,
                interestName: 'Confident',
                bgColor: '#85C396',
                icon: icons.confident,
            },
            {
                id: 1,
                interestName: 'Brave',
                bgColor: '#FFA7C1',
                icon: icons.brave,
            },
        ]
    }
];

const interestDetails = [
    {
        id: 1,
        label: 'Values',
        data: [
            {
                id: 1,
                interestName: 'Video Game',
                bgColor: '#F6D0A3',
                icon: icons.game,
            },
            {
                id: 1,
                interestName: 'Cooking',
                bgColor: '#F1EAAC',
                icon: icons.cooking,
            },
            {
                id: 1,
                interestName: 'Yoga',
                bgColor: '#AADFEB',
                icon: icons.yoga,
            },
            {
                id: 1,
                interestName: 'Video Game',
                bgColor: '#F6D0A3',
                icon: icons.game,
            },
            {
                id: 1,
                interestName: 'Cooking',
                bgColor: '#F1EAAC',
                icon: icons.cooking,
            },
            {
                id: 1,
                interestName: 'Yoga',
                bgColor: '#AADFEB',
                icon: icons.yoga,
            },
        ]
    }
];


const EditTutorProfile = () => {

    const navigate = useNavigate();

    return (
        <div className='edit_tutor_profile_container' >
            <div className='profile_header' >
                

                <div className='profile_img_container' >
                    <img src={icons.tutor_profile} style={{height: 400, width: '100%', objectFit:'cover', borderRadius: 20 }} />
                    <div className='ai-c jc-c d-f fd-c' style={{position: 'absolute', bottom: 50, zIndex: 1}} >
                        <div className='user_name' >Tutor Kalpana srivastava   <img className='edit_icon' src={icons.edit_purple} style={{position: 'absolute', top: -5, marginLeft: 20}} /> </div>
                        
                        <div className='user_description' >Lorem ipsum dolor sit amet, consectetur adipiscing elit.  <img className='edit_icon' src={icons.edit_purple} style={{position: 'absolute', top: 35, marginLeft: 20}} /> </div> 
                    </div>
                </div>

                <div className='image_blur' >
                    
                </div>

                <div className='edit_container mt-20 ml-20'>
                    <BackButton
                        title="Back"
                        onClick={() => {navigate(-1)}} 
                        icon={icons.left_arrow_grey} 
                        textStyle={{fontSize: 18, marginLeft: 10, color: '#FAFDFF', fontWeight: 'bold'}}
                        buttonStyle={{backgroundColor: Colors.MUSTURD,}}
                    />
                </div>
            </div>
           
            <div className='student_details_container1' style={{marginTop: 80}} >
                <div>
                    <div className='education_details_card ai-c fd-c d-f pos-rel'>
                        <div className=' d-f fd-c ai-c' >
                            <div className='details_heading '>Education</div>
                            <div className='details_sub_heading'>Lorem ipsum dolor sit amet</div>
                        </div>
                        <div className='edit_icon_container' style={{top: 0, right: 0,}} >
                            <img className='edit_icon' src={icons.edit_purple} />
                        </div>
                    </div>
                    <div className='details_card_values fd-c d-f ai-c pos-rel'>
                        <div className=' d-f fd-c ai-c' >
                            <div className='details_heading'>Service Specializations</div>
                        </div>

                        <div className='values_container full-width mt-20'  >
                            {valuesDetails && valuesDetails.map((item) => (
                                <div className='' >
                                    {item.data.map((el) => (
                                        <div className='ai-c jc-c d-f fd-c' >
                                            <div style={{height: 70, width: 70, borderRadius: 35, marginTop: 10, backgroundColor: el.bgColor }} className='ai-c jc-c d-f'>
                                                <img src={el.icon} style={{height: 30, width: 30}} />
                                            </div>
                                            <div className='mt-10' >{el.interestName}</div>
                                        </div>
                                    ))}
                                </div>
                            ))}
                        </div>

                        <div className='edit_icon_container' style={{top: 0, right: 0,}} >
                            <img className='edit_icon' src={icons.edit_purple} />
                        </div>
                    </div> 
                    <div className='education_details_card1 ai-c fd-c d-f mt-20 '>
                        <div className=' d-f fd-c ai-c' >
                            <div className='details_heading '>Income</div>
                            <div className='details_sub_heading'>SAT, ACT</div>
                        </div>

                        <div className=' d-f fd-c ai-c mt-10' >
                            <div className='details_heading '>Rate</div>
                            <div className='details_sub_heading'>$ 40 per hour</div>
                        </div>
                    </div>
                </div>
                <div className='full-width' style={{marginRight: 40}}>
                    <div>
                        <div className='details_card_description ai-c d-f mr-20'>
                            <div className='fd-c ai-c d-f full-width pos-rel' >
                                <div  className='associated_user_img d-f fd-c mb-20' style={{position: 'absolute', top: -85, borderColor: '#F6A429', borderWidth: 4, borderRadius: 50, }} >
                                    <img src={icons.expertProfile}  />
                                    <div className='camera_container' style={{bottom: -10, right: -10,}} >
                                        <img src={icons.camera} style={{height: 20, width: 20}} />
                                    </div>
                                </div>
                                <div className='jc-sb d-f fd-r full-width mt-20' >
                                    <div className='details_description'>{profileDetails.description}</div>
                                </div>
                                <div className='edit_icon_container' style={{top: -15, right: -15,}} >
                                    <img className='edit_icon' src={icons.edit_purple} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="mb-20 mt-20 pos-rel">
                        <div className='details_card2 ai-c d-f '>
                            <div className='fd-c ai-c d-f full-width ' >
                                <div className=' d-f fd-c' >
                                    <div className='details_heading'>Contact</div>
                                </div>
                                <div className='jc-sb d-f fd-r full-width' >
                                    <div className='ai-c d-f fd-c' >
                                        <img src={icons.email} style={{height: 22, width: 22}} />
                                        <div className='details_sub_heading'>{profileDetails.email}</div>
                                    </div>
                                   
                                    <div className='ai-c d-f fd-c' >
                                        <img src={icons.whatsapp} style={{height: 22, width: 22}} />
                                        <div className='details_sub_heading'>{profileDetails.whatsapp}</div>
                                    </div>

                                    <div className='ai-c d-f fd-c' >
                                        <img src={icons.linkedin} style={{height: 22, width: 22}} />
                                        <div className='details_sub_heading'>{profileDetails.linkedin}</div>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div className='edit_icon_container' style={{top: 0, right: -10,}} >
                                <img className='edit_icon' src={icons.edit_purple} />
                            </div>
                    </div>
                    <div>
                        <div className='details_card2 ai-c d-f mr-20 pos-rel' style={{height: 160}} >
                            <div className='fd-c ai-c d-f full-width' >
                                <div  className=' d-f fd-c' >
                                    <div className='details_heading'>Address</div>
                                </div>
                                <div className='jc-sb d-f fd-r full-width' >
                                    <div className='details_sub_heading'>{profileDetails.address}</div>
                                </div>
                              
                            </div>

                            <div className='edit_icon_container' style={{top: 0, right: 0,}} >
                                <img className='edit_icon' src={icons.edit_purple} />
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div className='education_details_card ai-c fd-c d-f pos-rel'>
                        <div className=' d-f fd-c ai-c' >
                            <div className='details_heading '>Experience</div>
                            <div className='details_sub_heading'>Lorem ipsum dolor sit amet</div>
                        </div>

                        <div className='edit_icon_container' style={{top: 0, right: 0,}} >
                            <img className='edit_icon' src={icons.edit_purple} />
                        </div>
                    </div>
                    <div className='details_card_values ai-c fd-c d-f mr-20 pos-rel'>
                        <div className=' d-f fd-c ai-c' >
                            <div className='details_heading '>Interest</div>
                        </div>

                        <div className='values_container full-width mt-20'  >
                            {interestDetails && interestDetails.map((item) => (
                                <div className='' >
                                    {item.data.map((el) => (
                                        <div className='ai-c jc-c d-f fd-c' >
                                            <div style={{height: 70, width: 70, borderRadius: 35, marginTop: 10, backgroundColor: el.bgColor }} className='ai-c jc-c d-f'>
                                                <img src={el.icon} style={{height: 30, width: 30}} />
                                            </div>
                                            <div className='mt-10' >{el.interestName}</div>
                                        </div>
                                    ))}
                                </div>
                            ))}
                        </div>

                        <div className='edit_icon_container' style={{top: 0, right: 0,}} >
                            <img className='edit_icon' src={icons.edit_purple} />
                        </div>
                    </div>
                    <div className='education_details_card1 ai-c fd-c d-f mt-20 '>
                        <div className=' d-f fd-c ai-c' >
                            <div className='details_heading '>Payment Info</div>
                            <div className='details_sub_heading'>Bank Name</div>
                            <div className='details_sub_heading'>Acc No.</div>
                            <div className='details_sub_heading'>IFCS Code</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default EditTutorProfile;