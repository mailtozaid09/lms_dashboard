export const events = [
    {
      'title': 'Science Presentaion',
      'start': new Date(2022, 10, 6, 0, 0, 0),
      'end': new Date(2022, 10, 8, 0, 0, 0)
    },
  
    {
      'title': 'Maths Test',
      'start': new Date(2022, 10, 6, 0, 0, 0),
      'end': new Date(2022, 10, 7, 0, 0, 0)
    },
    {
      'title': 'Parents Meeting',
      'start':new Date(2022, 10, 14, 0, 0, 0),
      'end':new Date(2022, 10, 16, 0, 0, 0),
      desc: 'Big conference for important people'
    },
    {
      'title': 'Service Learing Exam',
      'start': new Date(2022, 10, 14, 0, 0, 0),
      'end': new Date(2022, 10, 14, 0, 0, 0),
      desc: 'Pre-meeting meeting, to prepare for the meeting'
    },
  ]
  