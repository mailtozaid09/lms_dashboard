import React, {useEffect, useState} from 'react';
import './dashboard.scss';
import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment'
import 'react-big-calendar/lib/css/react-big-calendar.css';
import {events} from './events';
import DatePicker from 'sassy-datepicker';
import Searchbar from '../../../components/search/Searchbar';
import DropDown from '../../../components/dropdown/DropDown';
import { icons } from '../../../util/Icons';
import { Colors } from '../../../util/Colors';
import IconButton from '../../../components/button/iconButton/IconButton';
import CreateSessionModal from '../../../components/modal/calender/create/CreateSessionModal';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setIsLoading } from '../../../redux/actions';



const localizer = momentLocalizer(moment)

const Dashboard = () => {

    const [createModal, setCreateModal] = React.useState(false);

    const navigate = useNavigate();
    const dispatch = useDispatch()

    const [userDetails, setUserDetails] = useState();

    useEffect(() => {

        console.log("DASHBOARD==> ");
        var details = localStorage.getItem('user_details');
        var user_details = JSON.parse(details)
        console.log("user_details= > > ", user_details);
        setUserDetails(user_details)

        

        setTimeout(() => {
            //dispatch(setIsLoading(false));
        }, 2000);
    }, [])
    

    const handleOpenCreateModal = () => {
        console.log("open modal");
        setCreateModal(true);
    };
    
    const handleCloseCreateModal = () => {
        console.log("close modal");
        setCreateModal(false);
    }

    const onChange = (date) => {
        console.log(date.toString());
    };

    const handleSelected = (event) => {
         console.info('[handleSelected - event]', event);
    };


  return (
        <div className='dashboard_container d-f fd-r jc-sb full-width' style={{marginTop: 80,}} >
            <div>
                <DatePicker onChange={onChange} />

                <div className='mt-20'>
                    <Searchbar label="Type Name" />
                </div>
            </div>
            
            <div className='big_calender' >
                <div className='d-f fd-r jc-sb full-width mb-20'>
                    <IconButton
                        title="Add Event"
                        icon={icons.plus_circle} 
                        onClick={() => {handleOpenCreateModal()}} 
                        iconStyle={{height: 20, width:20, marginBottom: 2}}
                        textStyle={{fontSize: 18, marginRight: 20, color: 'white', fontWeight: 'bold'}}
                        buttonStyle={{padding: 10, paddingLeft: 20, paddingRight: 20, backgroundColor: Colors.PRIMARY, marginRight: 5}}
                    />
                    <DropDown
                        style={{width: 200}}
                        label="Time Zone"
                        defaultValue={'IST'}
                        data={[{value: 'IST', title: 'IST'}, {value: 'GMT', title: 'GMT'}]}
                    />
                </div>

                <Calendar
                    localizer={localizer}
                    events={events}
                    startAccessor="start"
                    endAccessor="end"
                    onSelectEvent={handleSelected}
                    style={{ height: 500, width: 1000}}
                />
            </div>

            {createModal && <CreateSessionModal handleOpen={() => handleOpenCreateModal()} handleClose={() => handleCloseCreateModal()} />}
        </div>
    )
}

export default Dashboard
