import React from 'react';
import './concept.scss';

import Input from '../../../components/input/Input'
import IconButton from '../../../components/button/iconButton/IconButton';
import { Colors } from '../../../util/Colors';
import ConceptAnalytics from '../../../components/graphs/ConceptAnalytics';


const invoiceData = [
    {
        id: 1,
        tutorName: 'Joseph Brown',
        date: '27/06/22',
        hourlyRate: '$ 40',
        duration: '45 mins',
        totalAmt: '$ 30',
    },
    {
        id: 2,
        tutorName: 'Joseph Brown',
        date: '27/06/22',
        hourlyRate: '$ 40',
        duration: '45 mins',
        totalAmt: '$ 30',
    },
    {
        id: 3,
        tutorName: 'Joseph Brown',
        date: '27/06/22',
        hourlyRate: '$ 40',
        duration: '45 mins',
        totalAmt: '$ 30',
    },
    {
        id: 4,
        tutorName: 'Joseph Brown',
        date: '27/06/22',
        hourlyRate: '$ 40',
        duration: '45 mins',
        totalAmt: '$ 30',
    },
]

const Concept = () => {
    return (
        <div>
            <h1>Concept Analytics</h1>
            
            <div style={{ justifyContent: 'center', display: 'flex',  width: '100%'}} >
                <ConceptAnalytics />
            </div>
        </div>
    );
};

export default Concept;