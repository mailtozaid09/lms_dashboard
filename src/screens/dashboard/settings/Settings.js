import React,{useState} from 'react';
import './settings.scss'

import SecondaryIconButton from '../../../components/button/SecondaryIconButton';

import {BsPlusLg}from "react-icons/bs"
import {AiFillCloseSquare}from "react-icons/ai"
import { icons } from '../../../util/Icons';
import { Colors } from '../../../util/Colors';
import IconButton from '../../../components/button/iconButton/IconButton';
import EditDetails from '../../../components/modal/edit/EditDetails/EditDetails';


const settingsData = [
    {
        leadStatusData: {
            id: 1,
            title: 'Lead Status Items',
            items: [
                {
                    id: 1,
                    itemName: 'Open / Raw',
                },
                {
                    id: 2,
                    itemName: 'Contacted Once - Answered',
                },
                {
                    id: 3,
                    itemName: 'Contacted Once - Unanswered',
                },
                {
                    id: 4,
                    itemName: 'Contacted Twice - Unanswered',
                },
                {
                    id: 5,
                    itemName: 'Not Interested',
                },
                {
                    id: 6,
                    itemName: 'Finished',
                },
                {
                    id: 7,
                    itemName: 'Scheduled',
                },
                {
                    id: 8,
                    itemName: 'Interested - Prospective',
                },
                {
                    id: 9,
                    itemName: 'Inactive / On a break',
                },
                {
                    id: 10,
                    itemName: 'Scheduled',
                },
                {
                    id:11,
                    itemName: 'Interested - Prospective',
                },
                {
                    id: 12,
                    itemName: 'Inactive / On a break',
                },
            ]
        },
        tutorStatusData: {
            id: 1,
            title: 'Tutor Status Items',
            items: [
                {
                    id: 13,
                    itemName: 'Interviewed',
                },
                {
                    id: 14,
                    itemName: 'Training',
                },
                {
                    id: 15,
                    itemName: 'Active',
                },
                {
                    id: 16,
                    itemName: 'Inactive',
                },
                {
                    id: 17,
                    itemName: 'Vacation',
                },
                {
                    id: 18,
                    itemName: 'Released',
                },
                {
                    id: 19,
                    itemName: 'Not Interested',
                },
            ]
        },
        sessionsTagData: {
            id: 1,
            title: 'Session Tags',
            topicsCovered: {
                id: 1,
                title: 'Topics Covered',
                items: [
                    {
                        id: 20,
                        itemName: 'Math',
                    },
                    {
                        id: 21,
                        itemName: 'English',
                    },
                    {
                        id: 22,
                        itemName: 'Reading',
                    },
                    {
                        id: 23,
                        itemName: 'Science',
                    },
                ],
            },
            studentMood: {
                id: 1,
                title: 'Student Mood',
                items: [
                    {
                        id: 24,
                        itemName: 'Engaging',
                    },
                    {
                        id: 25,
                        itemName: 'Chill',
                    },
                    {
                        id: 26,
                        itemName: 'Inspiring',
                    },
                    {
                        id: 27,
                        itemName: 'Quite',
                    },
                    {
                        id: 28,
                        itemName: 'Frustrated',
                    },
                    {
                        id: 29,
                        itemName: 'Confused',
                    },
                ],
            },
            homeworkAssigned: {
                id: 1,
                title: 'Homework Assigned',
                items: [
                    {
                        id: 30,
                        itemName: 'Practice Test',
                    },
                    {
                        id: 31,
                        itemName: 'Concept Review',
                    },
                    {
                        id: 32,
                        itemName: 'English Section',
                    },
                    {
                        id: 33,
                        itemName: 'Math Section',
                    },
                    {
                        id: 34,
                        itemName: 'Science Section',
                    },
                    {
                        id: 35,
                        itemName: 'Reading Section',
                    },
                    {
                        id: 36,
                        itemName: 'All Section',
                    },
                ],
            },
            sessionProductive: {
                id: 1,
                title: 'Was the session Productive?',
                items: [
                    {
                        id: 37,
                        itemName: 'Yes',
                    },
                    {
                        id: 38,
                        itemName: 'No',
                    },
                    {
                        id: 39,
                        itemName: 'English Section',
                    },
                    {
                        id: 40,
                        itemName: 'Not sure',
                    },
                ],
            },
        }
    }
]



const Settings = () => {

    const [hover, setHover] = useState(false);
    const [itemId, setItemId] = useState(0);

    const [editOpen, setEditOpen] = React.useState(false);
    const handleEditOpen = () => {
        console.log("open modal");
        setEditOpen(true);
    };
    
    const handleEditClose = () => {
        console.log("close modal");
        setEditOpen(false);
    }

    return (
        <div >
            <h1>Settings</h1>

            <div className='header-container' >
                <div>
                    <div>
                        <div className='invoice-details-row'>
                            <div className='invoice-details-left'>Full Name</div>
                            <div className='invoice-details-right'>: Jai Tulsi Vidhya Vihar</div>
                        </div>
                        <div className='invoice-details-row'>
                            <div className='invoice-details-left'>Email</div>
                            <div className='invoice-details-right'>: jaitulsividhyavihar@gmail.com</div>
                        </div>
                        <div className='invoice-details-row'>
                            <div className='invoice-details-left'>Number</div>
                            <div className='invoice-details-right'>: +91 9123456780</div>
                        </div>
                    </div>
                </div>
                <div>
                    <IconButton
                        title="Edit Details"
                        icon={icons.edit} 
                        onClick={handleEditOpen}
                        buttonStyle={{backgroundColor: Colors.PRIMARY, marginRight: 5}}
                    />   
                </div>
            </div>


            <div className='card' style={{marginTop: 50}} >
                {Object.values(settingsData[0]).map((el) => (
                    <div className='card-container'>
                        <div style={{fontSize: 20, color: '#25335A', fontWeight: 'bold'}} className='card-heading fontFamily'>
                            {el.title}
                        </div> 
                        <div className='card-content'>
                            <IconButton
                                title="Add Tags"
                                icon={icons.plus_purple} 
                                //onClick={handleOpen}
                                textStyle={{color: Colors.PRIMARY}}
                                buttonStyle={{backgroundColor: '#E9E3FF', marginRight: 5}}
                            />   
                        </div>

                       
                        <div className='flex-row' >
                            {el.items  && el.items.map((item) => (
                                <div 
                                className='flex-row'
                                onMouseEnter={e => {
                                    console.log("dsdsa");
                                    setItemId(item.id)
                                    setHover(true)
                                }}
                                onMouseLeave={e => {
                                    console.log("oooo");
                                    setItemId(item.id)
                                    setHover(false)
                                }}
                                
                                >
                                    <div className='item-container' style={{backgroundColor: '#E9E3FF', padding: 6}}>
                                        <div>{item.itemName}</div>
                                    </div>
                                    
                                    <div className='cross-button'>
                                        {hover && item.id == itemId  ? <AiFillCloseSquare size={20}  /> :  null  }
                                    </div>
                            
                                </div>
                            ))}
                        </div>


                        <div>
                            <div style={{color: '#25335A', marginTop: 10, fontWeight: '600'}} className='card-heading margin-top fontFamily'>
                                {el.topicsCovered?.title}
                            </div> 
                            <div className='flex-row' >
                            {el.topicsCovered  && Object.values(el.topicsCovered.items).map((mmm) => (
                                    <div className='item-container' style={{backgroundColor: '#E9E3FF', padding: 6}} >{mmm.itemName}</div>
                            ))}
                            </div>
                        </div>


                        <div>
                            <div style={{color: '#25335A', marginTop: 10, fontWeight: '600'}} className='card-heading margin-top fontFamily'>
                                {el.studentMood?.title}
                            </div> 
                            <div className='flex-row' >
                            {el.studentMood  && Object.values(el.studentMood.items).map((mmm) => (
                                    <div className='item-container' style={{backgroundColor: '#E9E3FF', padding: 6}} >{mmm.itemName}</div>
                            ))}
                            </div>
                        </div>


                        <div>
                            <div style={{color: '#25335A', marginTop: 10, fontWeight: '600'}} className='card-heading margin-top fontFamily'>
                                {el.homeworkAssigned?.title}
                            </div> 
                            <div className='flex-row' >
                            {el.homeworkAssigned  && Object.values(el.homeworkAssigned.items).map((mmm) => (
                                    <div className='item-container' style={{backgroundColor: '#E9E3FF', padding: 6}} >{mmm.itemName}</div>
                            ))}
                            </div>
                        </div>

                        <div>
                            <div style={{color: '#25335A', marginTop: 10, fontWeight: '600'}} className='card-heading margin-top fontFamily'>
                                {el.sessionProductive?.title}
                            </div> 
                            <div className='flex-row' >
                            {el.sessionProductive  && Object.values(el.sessionProductive.items).map((mmm) => (
                                    <div className='item-container' style={{backgroundColor: '#E9E3FF', padding: 6}} >{mmm.itemName}</div>
                            ))}
                            </div>
                        </div>
                      
                    </div>
                ))}
            </div> 

            {editOpen && <EditDetails handleOpen={() => handleEditOpen()} handleClose={() => handleEditClose()} />}
        </div>
    );
};

export default Settings;