import React from 'react';

import Dropdown from '../../../components/accordian/Dropdown';

import Button from '../../../components/button/Button';
import PrimaryButton from '../../../components/button/PrimaryButton';
import SecondaryButton from '../../../components/button/SecondaryButton';
import SecondaryIconButton from '../../../components/button/SecondaryIconButton';
import AddNewTest from '../../../components/modal/AddNewTest/AddNewTest';
import { Colors } from '../../../util/Colors';
import './sessions.scss'
import AssignNewTest from '../../../components/modal/assignNewTest/AssignNewTest';

const testData = [
    {
        id: 1,
        testName: 'Test Name',
        studentName: 'Student Name',
        createdOn: 'MM-DD-YY',
        duration: 'Regular 1.5x Unlimited',
        score: 'Based on test Format',
        status: 'Completed',
        dateAssigned: 'Date Assigned: MM-DD-YY',
    },
    {
        id: 2,
        testName: 'Test Name',
        studentName: 'Student Name',
        createdOn: 'MM-DD-YY',
        duration: 'Regular 1.5x Unlimited',
        score: 'Based on test Format',
        status: 'inCompleted',
        dateAssigned: 'Date Assigned: MM-DD-YY',
    },
    {
        id: 3,
        testName: 'Test Name',
        studentName: 'Student Name',
        createdOn: 'MM-DD-YY',
        duration: 'Regular 1.5x Unlimited',
        score: 'Based on test Format',
        status: 'inProgress',
        dateAssigned: 'Date Assigned: MM-DD-YY',
    },
    {
        id: 4,
        testName: 'Test Name',
        studentName: 'Student Name',
        createdOn: 'MM-DD-YY',
        duration: 'Regular 1.5x Unlimited',
        score: 'Based on test Format',
        status: 'Completed',
        dateAssigned: 'Date Assigned: MM-DD-YY',
    },
]
const Sessions = () => {
    return (
        <div>
            

            <div className='row-space' >
                <h1>All Sessions</h1>
                <AssignNewTest />
            </div>

             
            <div style={{width: 200}} >
            {/* <Dropdown/> */}
            </div>
          
            <div className='table-header' >
                <div className='heading' >Student Name</div>
                <div className='heading' >Test Name</div>
                <div className='heading'  style={{width: 150}} >Duration</div>
                <div className='heading' >Completion Status</div>
                <div className='heading'  style={{width: 150}}  >Score</div>
                <div className='heading' ></div>
                <div className='heading' > </div>
            </div>

            <div className='table-content-container' >
                {testData.map((item) => (
                    <div className='table-content' >
                        <div className='rowItem' >
                            <div className='sub-heading-blue' >{item.studentName}</div>
                            <div className='sub-heading margin-top' >{item.dateAssigned}</div>
                        </div>
                        <div className='sub-heading' >{item.testName}</div>
                        <div  style={{width: 150}} >
                            <div className='sub-heading'  style={{width: 70}} >{item.duration}</div>
                        </div>

                        
                        
                        <div className='sub-heading' >
                        {item.status == 'Completed'
                        ?
                        <div style={{height: 20, width: 20, backgroundColor: Colors.GREEN, borderRadius: 10}} ></div>
                        :
                        item.status == 'inCompleted'
                        ?
                        <div style={{height: 20, width: 20, backgroundColor: Colors.RED, borderRadius: 10}} ></div>
                        :
                        item.status == 'inProgress'
                        ?
                        <div style={{height: 20, width: 20, backgroundColor: Colors.YELLOW, borderRadius: 10}} ></div>
                        :
                        null
                        }
                        </div>

                        <div  style={{width: 150}} >
                            <div className='sub-heading'  style={{width: 70}} >{item.score}</div>
                        </div>

                        <div className='columnItem' >
                            
                            <PrimaryButton title="Test Details" containerStyle={{marginRight: 5}} />
                            <SecondaryButton title="Resend Test" containerStyle={{marginLeft: 5}}  />
                        </div>
                    </div>
                ))}
            </div>

           

            
        </div>
    );
};

export default Sessions;