import React, {useState} from 'react'
import { useLocation } from 'react-router-dom'
import { Colors } from '../../../../util/Colors'
import { icons } from '../../../../util/Icons'

import './viewTestDetails.scss'

const ViewTestDetails = (props) => {

    const testDetails = [
        {
            id: 1,
            testName: 'Chapter 8',
            testTitle: 'Your Teacher posted a new assignment: Google Form for subject Chapter 8',
            type: 'GF',
            testType: 'Assignment',
            createdOn: '22/06/2022',
            duration: 'Regular'
        },
        {
            id: 2,
            testName: 'Chapter 7',
            testTitle: 'Your Teacher posted a new material: PPT material for subject Chapter 7',
            type: 'MATERIAL',
            testType: 'Assignment',
            createdOn: '26/06/2022',
            duration: 'Regular'
        },
        {
            id: 3,
            testName: 'Chapter 6',
            testTitle: 'Your Teacher posted a new assignment: Google Form for subject Chapter 6',
            type: 'GF',
            testType: 'Assignment',
            createdOn: '30/06/2022',
            duration: 'Regular'
        },
        {
            id: 4,
            testName: 'Chapter 5',
            testTitle: 'Your Teacher posted a new material: PPT material for subject Chapter 5',
            type: 'MATERIAL',
            testType: 'Assignment',
            createdOn: '04/07/2022',
            duration: 'Regular'
        },
        {
            id: 5,
            testName: 'Chapter 4 & 3',
            testTitle: 'Your Teacher posted a new material: PPT material for subject Chapter 4 & 3',
            type: 'MATERIAL',
            testType: 'Assignment',
            createdOn: '22/06/2022',
            duration: 'Regular'
        },
        {
            id: 6,
            testName: 'Chapter 2',
            testTitle: 'Your Teacher posted a new material: PPT material for subject Chapter 2',
            type: 'MATERIAL',
            testType: 'Assignment',
            createdOn: '26/06/2022',
            duration: 'Regular'
        },
        {
            id: 7,
            testName: 'Chapter 1',
            testTitle: 'Your Teacher posted a new assignment: Google Form for subject Chapter 1',
            type: 'GF',
            testType: 'Assignment',
            createdOn: '30/06/2022',
            duration: 'Regular'
        },
    ]

    const location = useLocation();

    const [testName, setTestName] = useState(location.state.testName);


    const [currentId, setCurrentId] = useState(null);


    const buttonClickFunc = (type) => {

        if(type == 'GF'){
            window.open('https://docs.google.com/forms/d/e/1FAIpQLSea4RSJQTcGEOqsv7PU_LFZYI_hyz-iqHHv5cUIO7uwDpEQhQ/viewform?usp=sf_link', "_blank")
        }else{
            window.open('https://drive.google.com/file/u/1/d/1Uw8XlrJEf72mx4E3Xlm_VpKtq7L4e-lf/view?usp=drive_web', "_blank")
        }
    
    }

    return (
        <div style={{alignItems: 'center', flexDirection: 'column', justifyContent: 'center', display: 'flex'}} >
             
            <div style={{ width: 900,}} >
                <div style={{background: Colors.PRIMARY, width: 900, height: 200,  borderRadius: 20, marginTop: 20, marginBottom: 20 }} >
                    <div style={{width: 300, height: 100, position: 'absolute', top: 120, left: 400}} >
                        <div style={{fontSize: 24, color: 'white'}} >{testName}</div>
                    </div>
                    <img src={icons.class_back} style={{ height: 200, }} />
                </div>

                <div>
                    {testDetails.map((item) => (
                        <div onClick={() => {setCurrentId(item.id)}} className='class_container' style={{ height: currentId == item.id ? 170 : 70, background: 'white', borderWidth: 1, borderColor: 'gray', marginTop: 10, marginBottom: 10, borderRadius: 8, width: '100%', paddingLeft: 20, flexDirection: 'column', justifyContent: 'flex-start', display: 'flex'}} >
                             
                            <div style={{flexDirection: 'row', display: 'flex'}} >
                                <div style={{height: 40, width: 40, borderRadius: 20, background: item.type == 'GF' ? Colors.PRIMARY : '#80d3fb', alignItems: 'center', justifyContent: 'center', display: 'flex', marginTop: 15, marginRight: 20}} >
                                    <img src={item.type == 'GF' ? icons.gform : icons.all_test} style={{width: 20, height: 20, }} />
                                </div>
                                <div style={{marginTop: 25}} >{item.testTitle}</div>
                            </div>

                            <div>
                            {currentId == item.id 
                            ?
                            <div style={{marginTop: 20, flexDirection: 'row', display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '100%',}} >
                                <div style={{marginTop: 10, marginBottom: 10}} >Due date - 20 November 2022</div>
                                <div onClick={() => {buttonClickFunc(item.type)}} style={{height: 60, width: 300, marginRight: 20, paddingLeft: 20, paddingRight: 20, background: item.type == 'GF' ? Colors.PRIMARY : '#80d3fb', borderRadius: 8, flexDirection: 'row', display: 'flex'}} >
                                    <div style={{height: 40, width: 40, borderRadius: 20, background: item.type == 'GF' ? Colors.WHITE : null, alignItems: 'center', justifyContent: 'center', display: 'flex', marginRight: 20, marginTop: 10}} >
                                        <img src={item.type == 'GF' ? icons.gform : icons.all_test} style={{width: 20, height: 20, }} />
                                    </div>
                                    <div style={{marginTop: 20, color: 'white'}} >{item.testName} {item.type == 'GF' ? 'Test' : 'Material' }</div>
                                </div>
                               
                            </div>
                            :
                            null
                            }
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default ViewTestDetails