import React, { useState, useEffect} from 'react';

import { useNavigate } from 'react-router-dom';
import { deleteTest, getAllTests } from '../../../../api/ApiManager';
import Dropdown from '../../../../components/accordian/Dropdown';

import Button from '../../../../components/button/Button';
import IconButton from '../../../../components/button/iconButton/IconButton';
import DatePicker from '../../../../components/dateTimePicker/DatePicker';
import DropDown from '../../../../components/dropdown/DropDown';
import Header from '../../../../components/header/Header';
import ScreenLoader from '../../../../components/loader/ScreenLoader';
import AddNewTest from '../../../../components/modal/AddNewTest/AddNewTest';
import DeleteQuestion from '../../../../components/modal/delete/DeleteQuestion/DeleteQuestion';
import Searchbar from '../../../../components/search/Searchbar';
import { Colors } from '../../../../util/Colors';
import { showDate } from '../../../../util/CustomUtils';
import { icons } from '../../../../util/Icons';
import './allTest.scss'


const testDetails = [
    {
        id: 1,
        testName: 'Science',
        testType: 'Assignment',
        createdOn: '22/06/2022',
        duration: 'Regular'
    },
    {
        id: 2,
        testName: 'English',
        testType: 'Assignment',
        createdOn: '26/06/2022',
        duration: 'Regular'
    },
    {
        id: 3,
        testName: 'Mathematics',
        testType: 'Assignment',
        createdOn: '30/06/2022',
        duration: 'Regular'
    },
    {
        id: 4,
        testName: 'Hindi',
        testType: 'Assignment',
        createdOn: '04/07/2022',
        duration: 'Regular'
    },
    {
        id: 5,
        testName: 'Kannada',
        testType: 'Assignment',
        createdOn: '22/06/2022',
        duration: 'Regular'
    },
    {
        id: 6,
        testName: 'History & Civics',
        testType: 'Assignment',
        createdOn: '26/06/2022',
        duration: 'Regular'
    },
    {
        id: 7,
        testName: 'Geography',
        testType: 'Assignment',
        createdOn: '30/06/2022',
        duration: 'Regular'
    },
]

const AllTest = () => {

    const navigate = useNavigate();
    const [open, setOpen] = React.useState(false);
    const [deleteOpen, setDeleteOpen] = React.useState(false);

    const [loader, setLoader] = useState(false);
    const [testData, setTestData] = useState([]);
    const [currentTestId, setCurrentTestId] = useState('');

    useEffect(() => {
    
        window.addEventListener("focus", onFocus);

        onFocus();
        return () => {
            window.removeEventListener("focus", onFocus);
        };

    }, [])
    

    const onFocus = () => {
        //setLoader(true)
        //getAllTestsDetails()
    };


    const getAllTestsDetails = () => {
        getAllTests()
        .then((resp) => {
            if(resp.status == '200'){
                setTestData(resp.data)
                setLoader(false)
            }
        })
        .catch((err) => {
            setLoader(false)
            console.log("err ", err);
        })
    }

    const handleOpen = () => {
        console.log("open modal");
        setOpen(true);
    };
    
    const handleClose = () => {
        console.log("close modal");
        setOpen(false);
    }

    const handleDeleteOpen = (testId) => {
        console.log("open  delete modal");
        setCurrentTestId(testId)
        setDeleteOpen(true);
    };
    
    const handleDeleteClose = () => {
        console.log("close delete modal");
        setDeleteOpen(false);
    }
       
    const handleDelete = (id) => {
        console.log("handleDeletel  " , id);
       
        deleteTest(id)
        .then((resp) => {
            if(resp.status == '200'){
                handleDeleteClose()
                getAllTestsDetails()
            }
        })
        .catch((err) => {
            console.log("err ", err);
        })
    }


    
    return (
        <div style={{width: '100%', display: 'flex', flexDirection: 'column',}} >
            
            {loader
            ?
            <div  className='loader_style' >
                <ScreenLoader />
            </div>
            :
                <div>
                     <Header
                        title="All Test" 
                        buttonTitle="Add new test" 
                        buttonColor={Colors.MUSTURD}
                        buttonIcon={icons.plus_circle} 
                        onClick={handleOpen}
                    />

                    {/* <div className='mt-20 mb-20' >
                        <Searchbar 
                            label="Test Name"
                        />
                    </div> */}

                    <div className='all-test-table-header' >
                        <div className='all-test-heading' style={{width: '190%'}} >Test Name</div>
                        <div className='all-test-heading' style={{width: '190%'}} >Date Modified</div>
                        <div className='all-test-heading' style={{width: '190%'}} >Test Type</div>
                        <div className='all-test-heading' style={{width: '100%'}} ></div>
                        <div className='all-test-heading' style={{width: '100%'}} ></div>
                    </div>

                    <div className='all-test-table-content-container' >
                        {testDetails.map((item, index) => (
                            <div className='all-test-table-content' style={{marginBottom: 10, border: "0.1px solid grey", backgroundColor: index % 2 == 0 ? 'white' : '#F5F7F9'}} >
                                <div className='all-test-sub-heading' style={{width: '100%'}} >{item.testName}</div>
                                <div className='all-test-sub-heading' style={{width: '100%'}} >{item.createdOn}</div>
                                <div className='all-test-sub-heading' style={{width: '100%'}} >{item.testType}</div>
                                <div className='all-test-sub-heading' style={{width: '100%'}} >
                                    <div>
                                        <IconButton 
                                            title="View Test"
                                            onClick={() => {
                                                navigate('/viewTestDetails',{
                                                    state:  {
                                                        testName: item.testName,
                                                    }
                                                });
                                            }} 
                                            buttonStyle={{width: 120, backgroundColor: Colors.MUSTURD, marginRight: 5}}
                                        />    
                                    </div>
                                    {/* <div>
                                        <IconButton 
                                            title="Delete"
                                            onClick={() => handleDeleteOpen(item._id)}
                                            buttonStyle={{width: 120, backgroundColor: Colors.PRIMARY,  marginLeft: 5}}
                                        />    
                                    </div> */}
                                </div>
                            </div>
                        ))}
                     </div>
{/* 
                    <div className='all-test-table-content-container' >
                        {testDetails.map((item, index) => (
                            <div className='all-test-table-content' style={{marginBottom: 10, border: "0.1px solid grey", backgroundColor: index % 2 == 0 ? 'white' : '#F5F7F9'}} >
                                <div className='all-test-sub-heading' style={{width: '100%'}} >{item.name}</div>
                                <div className='all-test-sub-heading' style={{width: '100%'}} >{showDate(item.auditFields.updatedAt)}</div>
                                <div className='all-test-sub-heading' style={{width: '100%'}} >{item.type}</div>
                                <div className='all-test-sub-heading' style={{width: '100%'}} >
                                    <div>
                                        <IconButton 
                                            title="View Test"
                                            onClick={() => {
                                                navigate('/assignedTestPage',{
                                                    state:  {
                                                        testId: item._id,
                                                    }
                                                });
                                            }} 
                                            buttonStyle={{width: 120, backgroundColor: Colors.MUSTURD, marginRight: 5}}
                                        />    
                                    </div>
                                    <div>
                                        <IconButton 
                                            title="Delete"
                                            onClick={() => handleDeleteOpen(item._id)}
                                            buttonStyle={{width: 120, backgroundColor: Colors.PRIMARY,  marginLeft: 5}}
                                        />    
                                    </div>
                                </div>
                            
                            
                            </div>
                        ))}
                    </div> */}

           
                </div>
            }

           
            {open && <AddNewTest handleFetchDetails={() => getAllTestsDetails()} handleOpen={() => handleOpen()} handleClose={() => handleClose()} />}

            {deleteOpen && <DeleteQuestion handleDelete={() => handleDelete(currentTestId)} handleOpen={() => handleDeleteOpen()} handleClose={() => handleDeleteClose()} />}
            
        </div>
    );
};

export default AllTest;