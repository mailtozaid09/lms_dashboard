import React from 'react';
import './assignedTest.scss'


import Dropdown from '../../../../components/accordian/Dropdown';
import Button from '../../../../components/button/Button';

import Header from '../../../../components/header/Header';
import AddNewTest from '../../../../components/modal/AddNewTest/AddNewTest';
import IconButton from '../../../../components/button/iconButton/IconButton';

import { icons } from '../../../../util/Icons';
import { Colors } from '../../../../util/Colors';

import { useNavigate } from 'react-router-dom';
import Searchbar from '../../../../components/search/Searchbar';
import DropDown from '../../../../components/dropdown/DropDown';
import AssignNewTest from '../../../../components/modal/assignNewTest/AssignNewTest';

const assignedTest = [
    {
        id: 1,
        testName: 'Class Test',
        studentName: 'Ram Kumar',
        assignedOn: '23/05/2022',
        duration: 'Regular',
        score: '80',
        status: 'Completed',
        dateAssigned: 'Date Assigned: 23/05/2022',
        button: 'Test Details',
    },
   
    {
        id: 2,
        testName: 'Class Test',
        studentName: 'Shivam Singh',
        assignedOn: '21/05/2022',
        duration: 'Regular',
        score: 'NA',
        status: 'inCompleted',
        dateAssigned: 'Date Assigned: 21/05/2022',
        button: 'Not Started',
    },
    {
        id: 3,
        testName: 'Class Test',
        studentName: 'Amit Sinha',
        assignedOn: '27/05/2022',
        duration: 'Regular',
        score: '91',
        status: 'Completed',
        dateAssigned: 'Date Assigned: 27/05/2022',
        button: 'Test Details',
    },
    {
        id: 4,
        testName: 'Class Test',
        studentName: 'Adarsh Sharma',
        assignedOn: '30/05/2022',
        duration: 'Regular',
        score: '54',
        status: 'Completed',
        dateAssigned: 'Date Assigned: 30/05/2022',
        button: 'Test Details',
    },
    {
        id: 5,
        testName: 'Class Test',
        studentName: 'Abhishek Makhloga',
        assignedOn: '21/05/2022',
        duration: 'Regular',
        score: 'NA',
        status: 'Completed',
        dateAssigned: 'Date Assigned: 21/05/2022',
        button: 'Test Details',
    },
    {
        id: 6,
        testName: 'Class Test',
        studentName: 'Aditya Rawat',
        assignedOn: '27/05/2022',
        duration: 'Regular',
        score: '91',
        status: 'Completed',
        dateAssigned: 'Date Assigned: 27/05/2022',
        button: 'Test Details',
    },
    {
        id: 7,
        testName: 'Class Test',
        studentName: 'Mahesh Sharma',
        assignedOn: '30/05/2022',
        duration: 'Regular',
        score: '54',
        status: 'Completed',
        dateAssigned: 'Date Assigned: 30/05/2022',
        button: 'Test Details',
    },
    {
        id: 8,
        testName: 'Class Test',
        studentName: 'Yash Raj',
        assignedOn: '21/05/2022',
        duration: 'Regular',
        score: 'NA',
        status: 'inCompleted',
        dateAssigned: 'Date Assigned: 21/05/2022',
        button: 'Not Started',
    },
    {
        id: 9,
        testName: 'Class Test',
        studentName: 'Rohit Singh',
        assignedOn: '21/05/2022',
        duration: 'Regular',
        score: 'NA',
        status: 'inCompleted',
        dateAssigned: 'Date Assigned: 21/05/2022',
        button: 'Not Started',
    },
    {
        id: 10,
        testName: 'Class Test',
        studentName: 'Deepak Ahuja',
        assignedOn: '30/05/2022',
        duration: 'Regular',
        score: '54',
        status: 'Completed',
        dateAssigned: 'Date Assigned: 30/05/2022',
        button: 'Test Details',
    },
]
const AssignedTest = () => {

    const navigate = useNavigate();

    const [open, setOpen] = React.useState(false);
      const handleOpen = () => {
        console.log("open modal");
        setOpen(true);
    };
    
    const handleClose = () => {
        console.log("close modal");
        setOpen(false);
    }

    return (
        <div style={{width: '100%', display: 'flex', flexDirection: 'column',}} >

            <Header 
                title="Assigned Test" 
                buttonTitle="Add new test" 
                buttonColor={Colors.MUSTURD}
                buttonIcon={icons.plus_circle} 
                onClick={handleOpen}
            />

            <div className='mt-20 mb-20 d-f fd-r ai-c jc-sb full-width' >
                <Searchbar
                    label="Student Name"
                />

                <Searchbar 
                    label="Test Name"
                />

                <Searchbar 
                    label="Tutor Name"
                />

                <DropDown
                    label="Completion Status"
                    data={[{value: 'Completed', title: 'Completed'}, {value: 'InCompleted', title: 'InCompleted'}, {value: 'InProgress', title: 'InProgress'}]}
                />

            </div>

            
            <div className='assigned-test-table-header' >
                <div className='assigned-test-heading' style={{width: '100%'}} >Name</div>
                <div className='assigned-test-heading' style={{width: '100%'}} >Assigned on</div>
                <div className='assigned-test-heading' style={{width: '100%'}} >Test Name</div>
                <div className='assigned-test-heading' style={{width: '100%'}} >Duration</div>
                <div className='assigned-test-heading' style={{width: '100%'}} >Status</div>
                <div className='assigned-test-heading' style={{width: '100%'}} >Scores</div>
                <div className='assigned-test-heading' style={{width: '100%'}} ></div>
                <div className='assigned-test-heading' style={{width: '100%'}} ></div>
            </div>

            <div className='assigned-test-table-content-container' >
                {assignedTest.map((item, index) => (
                    <div className='assigned-test-table-content' style={{marginBottom: 10, border: "0.1px solid grey", backgroundColor: index % 2 == 0 ? 'white' : '#F5F7F9'}} >
                        <div className='assigned-test-sub-heading' style={{width: '100%'}} >{item.studentName}</div>
                        <div className='assigned-test-sub-heading' style={{width: '100%'}} >{item.assignedOn}</div>
                        <div className='assigned-test-sub-heading' style={{width: '100%'}} >{item.testName}</div>
                        <div className='assigned-test-sub-heading' style={{width: '100%'}} >{item.duration}</div>
                        <div className='assigned-test-sub-heading' style={{width: '100%'}} >
                            {item.status == 'Completed'
                            ?
                            <div style={{height: 20, width: 20, backgroundColor: Colors.GREEN, borderRadius: 10}} ></div>
                            :
                            item.status == 'inCompleted'
                            ?
                            <div style={{height: 20, width: 20, backgroundColor: Colors.RED, borderRadius: 10}} ></div>
                            :
                            item.status == 'inProgress'
                            ?
                            <div style={{height: 20, width: 20, backgroundColor: Colors.YELLOW, borderRadius: 10}} ></div>
                            :
                            null
                            }    
                        </div>
                        <div className='assigned-test-sub-heading' style={{width: '100%'}} >{item.score}</div>
                        <div className='assigned-test-sub-heading' style={{width: '100%'}} >
                            <IconButton title={item.button} 
                                buttonStyle={{width: 140, backgroundColor: Colors.PRIMARY}}
                                onClick={() => {item.button == 'Start Test' ? navigate('/startTest') : navigate('/testDetails')}} 
                            />    
                        </div>
                        <div className='assigned-test-sub-heading' style={{width: '100%'}} >
                            <img src={icons.document} style={{height: 24, width: 24}} />
                        </div>
                    </div>
                ))}
            </div>

            {open && <AssignNewTest handleOpen={() => handleOpen()} handleClose={() => handleClose()} />}
                    
            
        </div>
    );
};

export default AssignedTest;