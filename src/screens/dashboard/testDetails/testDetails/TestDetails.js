import React,{useState} from 'react';
import './testDetails.scss'


import Dropdown from '../../../../components/accordian/Dropdown';
import Button from '../../../../components/button/Button';

import Header from '../../../../components/header/Header';
import AddNewTest from '../../../../components/modal/AddNewTest/AddNewTest';
import IconButton from '../../../../components/button/iconButton/IconButton';

import { icons } from '../../../../util/Icons';
import { Colors } from '../../../../util/Colors';

import { useNavigate } from 'react-router-dom';
import BackButton from '../../../../components/button/BackButton/BackButton';


import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
  } from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { faker } from '@faker-js/faker';
import TimeTaken from '../../../../components/graphs/TimeTaken';
import ConceptualAccuracy from '../../../../components/graphs/ConceptualAccuracy';


const questionDetails = [
    {
        id: 1,
        correctAns: 'A',
        studentResp: 'B',
        accuracy: 'Incorrect',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
        time: 21,
        solution: ''
    },
    {
        id: 2,
        correctAns: 'A',
        studentResp: 'B',
        accuracy: 'Incorrect',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
        time: 21,
        solution: ''
    },
    {
        id: 3,
        correctAns: 'A',
        studentResp: 'A',
        accuracy: 'Correct',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
        time: 21,
        solution: ''
    },
    {
        id: 4,
        correctAns: 'A',
        studentResp: 'B',
        accuracy: 'Incorrect',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
        time: 21,
        solution: ''
    },
    {
        id: 2,
        correctAns: 'A',
        studentResp: 'B',
        accuracy: 'Incorrect',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
        time: 21,
        solution: ''
    },
    {
        id: 3,
        correctAns: 'A',
        studentResp: 'A',
        accuracy: 'Correct',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
        time: 21,
        solution: ''
    },
    {
        id: 4,
        correctAns: 'A',
        studentResp: 'B',
        accuracy: 'Incorrect',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
        time: 21,
        solution: ''
    },
    {
        id: 2,
        correctAns: 'A',
        studentResp: 'B',
        accuracy: 'Incorrect',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
        time: 21,
        solution: ''
    },
    {
        id: 3,
        correctAns: 'A',
        studentResp: 'A',
        accuracy: 'Correct',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
        time: 21,
        solution: ''
    },
    {
        id: 4,
        correctAns: 'A',
        studentResp: 'B',
        accuracy: 'Incorrect',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
        time: 21,
        solution: ''
    },
]

const testDetails =  {
    id: 1,
    testName: 'ACT O',
    studentName: 'Joseph Brown',
    dateAssigned: '06/24/22',
    duration: 'Regular',
    startedOn: '06/24/22',
    completedOn: '06/26/22',
};

const subjectDetails =  [
    { 
        id: 1,
        subjectName: 'English',
        obtainMarks: 55,
        totalMarks: 75,
        concepts: [
            {
                id: 1,
                conceptName: 'Punctuating Clauses',
                incorrectAns: 5,
                totalAns: 17,
            },
            {
                id: 2,
                conceptName: 'Apostrophies',
                incorrectAns: 9,
                totalAns: 17,
            },
            {
                id: 3,
                conceptName: 'Modification',
                incorrectAns: 4,
                totalAns: 7,
            },
            {
                id: 4,
                conceptName: 'Punctuating Clauses',
                incorrectAns: 5,
                totalAns: 17,
            },
            {
                id: 5,
                conceptName: 'Punctuating Clauses',
                incorrectAns: 5,
                totalAns: 17,
            },
        ]  
    },
    { 
        id: 2,
        subjectName: 'Mathematics',
        obtainMarks: 42,
        totalMarks: 80,
        concepts: [
            {
                id: 1,
                conceptName: 'Punctuating Clauses',
                incorrectAns: 5,
                totalAns: 20,
            },
            {
                id: 2,
                conceptName: 'Apostrophies',
                incorrectAns: 9,
                totalAns: 20,
            },
            {
                id: 3,
                conceptName: 'Modification',
                incorrectAns: 16,
                totalAns: 20,
            },
            {
                id: 4,
                conceptName: 'Punctuating Clauses',
                incorrectAns: 10,
                totalAns: 20,
            },
        ]  
    },
    { 
        id: 3,
        subjectName: 'Science',
        obtainMarks: 55,
        totalMarks: 75,
        concepts: [
            {
                id: 1,
                conceptName: 'Punctuating Clauses',
                incorrectAns: 5,
                totalAns: 17,
            },
            {
                id: 2,
                conceptName: 'Apostrophies',
                incorrectAns: 9,
                totalAns: 17,
            },
            {
                id: 3,
                conceptName: 'Modification',
                incorrectAns: 4,
                totalAns: 7,
            },
            {
                id: 4,
                conceptName: 'Punctuating Clauses',
                incorrectAns: 5,
                totalAns: 17,
            },
            {
                id: 5,
                conceptName: 'Punctuating Clauses',
                incorrectAns: 5,
                totalAns: 17,
            },
        ]  
    },
    { 
        id: 4,
        subjectName: 'Reading',
        obtainMarks: 42,
        totalMarks: 80,
        concepts: [
            {
                id: 1,
                conceptName: 'Punctuating Clauses',
                incorrectAns: 5,
                totalAns: 20,
            },
            {
                id: 2,
                conceptName: 'Apostrophies',
                incorrectAns: 9,
                totalAns: 20,
            },
            {
                id: 3,
                conceptName: 'Modification',
                incorrectAns: 16,
                totalAns: 20,
            },
            {
                id: 4,
                conceptName: 'Punctuating Clauses',
                incorrectAns: 10,
                totalAns: 20,
            },
        ]  
    },
];


const TestDetails = () => {

    const navigate = useNavigate();

    const [currentSub, setCurrentSub] = useState(0);

    const [open, setOpen] = React.useState(false);
      const handleOpen = () => {
        console.log("open modal");
        setOpen(true);
    };
    
    const handleClose = () => {
        console.log("close modal");
        setOpen(false);
    }

    return (
        <div style={{width: '100%', display: 'flex', flexDirection: 'column',}} >

            <div className='back-button-container' >
                <BackButton
                    title="Back" 
                    onClick={() => {navigate(-1)}} 
                />
            </div>

            <div>
                <div className='test-details-title'>{testDetails.testName}</div>

                <div className='test-details-row-container'>
                    <div className='test-details-row'>
                        <div className='test-details-heading' >Student’s Name</div>
                        <div className='test-details-colon' >:</div>
                        <div className='test-details-sub-heading'>{testDetails.studentName}</div>
                    </div>
                    <div className='test-details-row'>
                        <div className='test-details-heading'>Started on</div>
                        <div className='test-details-colon' >:</div>
                        <div className='test-details-sub-heading'>{testDetails.startedOn}</div>
                    </div>
                </div>

                <div className='test-details-row-container'>
                    <div className='test-details-row'>
                        <div className='test-details-heading' >Date Assigned</div>
                        <div className='test-details-colon' >:</div>
                        <div className='test-details-sub-heading'>{testDetails.dateAssigned}</div>
                    </div>
                    <div className='test-details-row'>
                        <div className='test-details-heading'>Completed on</div>
                        <div className='test-details-colon' >:</div>
                        <div className='test-details-sub-heading'>{testDetails.completedOn}</div>
                    </div>
                </div>

                <div className='test-details-row-container'>
                    <div className='test-details-row'>
                        <div className='test-details-heading' >Duration</div>
                        <div className='test-details-colon' >:</div>
                        <div className='test-details-sub-heading'>{testDetails.duration}</div>
                    </div>
                </div>
            </div>

            <div className='subject-main-container' >
                {subjectDetails.map((subject, subIdx) => (
                    <div onClick={() => setCurrentSub(subIdx)} className='subject-container' style={{color: currentSub == subIdx ? Colors.WHITE : Colors.DARKGREY, backgroundColor: currentSub == subIdx ? Colors.PRIMARY : Colors.SECONDARY }} >{subject.subjectName}</div>
                ))}
            </div>

            <div className='subject-score' >Score : {subjectDetails[currentSub].obtainMarks}/{subjectDetails[currentSub].totalMarks}</div>

            <div className='subject-concept-main-container' >
                {subjectDetails && subjectDetails.map((subject, subIdx) => (
                    <div>
                        {currentSub == subIdx && <div className='concept-row-container' >
                            <div className='concept-column-container'>
                                <div className='concept-container' >
                                    <div className='concept-heading-title' >Concepts</div>
                                    <div className='concept-heading-title' >Incorrect Answer</div>
                                </div>
                                {subject && subject.concepts.map((concepts) => (
                                    <div className='concept-container' >
                                        <div className='concept-heading' >{concepts.conceptName}</div>
                                        <div className='concept-heading'>{concepts.incorrectAns}/{concepts.totalAns}</div>
                                    </div>
                                ))}
                            </div>
                        </div>}
                    </div>
                ))}
            </div>
            
            <div className='assigned-test-table-header' >
                <div className='assigned-test-heading' style={{width: '100%'}} >Q No.</div>
                <div className='assigned-test-heading' style={{width: '100%'}} >Correct Answer</div>
                <div className='assigned-test-heading' style={{width: '100%'}} >Student Response</div>
                <div className='assigned-test-heading' style={{width: '100%'}} >Accuracy</div>
                <div className='assigned-test-heading' style={{width: '100%'}} >Concept</div>
                <div className='assigned-test-heading' style={{width: '100%'}} >Strategy</div>
                <div className='assigned-test-heading' style={{width: '100%'}} >Time</div>
                <div className='assigned-test-heading' style={{width: '100%'}} >Solution</div>
            </div>

            <div className='assigned-test-table-content-container' >
                {questionDetails.map((item, index) => (
                    <div className='assigned-test-table-content' style={{marginBottom: 10, border: "0.1px solid grey", backgroundColor: index % 2 == 0 ? 'white' : '#F5F7F9'}} >
                        <div className='assigned-test-sub-heading' style={{width: '100%'}} >{item.id}</div>
                        <div className='assigned-test-sub-heading' style={{width: '100%'}} >{item.correctAns}</div>
                        <div className='assigned-test-sub-heading' style={{width: '100%'}} >{item.studentResp}</div>
                        <div className='assigned-test-sub-heading' style={{width: '100%'}} >
                            {item.accuracy == 'Correct'
                            ?
                            <div style={{ color: Colors.GREEN,}} >{item.accuracy}</div>
                            :
                            item.accuracy == 'Incorrect'
                            ?
                            <div style={{ color: Colors.RED,}} >{item.accuracy}</div>
                            :
                            null
                            }    
                        </div>
                        <div className='assigned-test-sub-heading' style={{width: '100%', padding: 8}} >{item.concept}</div>
                        <div className='assigned-test-sub-heading' style={{width: '100%', padding: 8}} >{item.strategy}</div>
                        <div className='assigned-test-sub-heading' style={{width: '100%', padding: 8}} >{item.time}</div>
                        {/* <div className='assigned-test-sub-heading' style={{width: '100%'}} >
                            <IconButton title="Test Details" onClick={() => {navigate('/dashboard')}} />    
                        </div> */}
                        <div className='assigned-test-sub-heading' style={{width: '100%'}} >
                            <img src={icons.document} style={{height: 24, width: 24}} />
                        </div>
                    </div>
                ))}
            </div>

            <div style={{marginTop: 50, flexDirection: 'column', alignItems: 'center', justifyContent: 'center', display: 'flex', width: '100%'}} >
                <TimeTaken />
                <ConceptualAccuracy />
            </div>

            {open && <AddNewTest handleOpen={() => handleOpen()} handleClose={() => handleClose()} />}
                    
            
        </div>
    );
};

export default TestDetails;