import React,{useState} from 'react';
import './testDetails.scss'

//import SecondaryIconButton from '../../../components/button/SecondaryIconButton';

import {BsPlusLg}from "react-icons/bs"
import {AiFillCloseSquare}from "react-icons/ai"
import Details from './Details';
import QuestionSection from './QuestionSection';


const settingsData = [
    {
        leadStatusData: {
            id: 1,
            title: 'Lead Status Items',
            items: [
                {
                    id: 1,
                    itemName: 'Open / Raw',
                },
                {
                    id: 2,
                    itemName: 'Contacted Once - Answered',
                },
                {
                    id: 3,
                    itemName: 'Contacted Once - Unanswered',
                },
                {
                    id: 4,
                    itemName: 'Contacted Twice - Unanswered',
                },
                {
                    id: 5,
                    itemName: 'Not Interested',
                },
                {
                    id: 6,
                    itemName: 'Finished',
                },
                {
                    id: 7,
                    itemName: 'Scheduled',
                },
                {
                    id: 8,
                    itemName: 'Interested - Prospective',
                },
                {
                    id: 9,
                    itemName: 'Inactive / On a break',
                },
                {
                    id: 10,
                    itemName: 'Scheduled',
                },
                {
                    id:11,
                    itemName: 'Interested - Prospective',
                },
                {
                    id: 12,
                    itemName: 'Inactive / On a break',
                },
            ]
        },
        tutorStatusData: {
            id: 1,
            title: 'Tutor Status Items',
            items: [
                {
                    id: 13,
                    itemName: 'Interviewed',
                },
                {
                    id: 14,
                    itemName: 'Training',
                },
                {
                    id: 15,
                    itemName: 'Active',
                },
                {
                    id: 16,
                    itemName: 'Inactive',
                },
                {
                    id: 17,
                    itemName: 'Vacation',
                },
                {
                    id: 18,
                    itemName: 'Released',
                },
                {
                    id: 19,
                    itemName: 'Not Interested',
                },
            ]
        },
        sessionsTagData: {
            id: 1,
            title: 'Session Tags',
            topicsCovered: {
                id: 1,
                title: 'Topics Covered',
                items: [
                    {
                        id: 20,
                        itemName: 'Math',
                    },
                    {
                        id: 21,
                        itemName: 'English',
                    },
                    {
                        id: 22,
                        itemName: 'Reading',
                    },
                    {
                        id: 23,
                        itemName: 'Science',
                    },
                ],
            },
            studentMood: {
                id: 1,
                title: 'Student Mood',
                items: [
                    {
                        id: 24,
                        itemName: 'Engaging',
                    },
                    {
                        id: 25,
                        itemName: 'Chill',
                    },
                    {
                        id: 26,
                        itemName: 'Inspiring',
                    },
                    {
                        id: 27,
                        itemName: 'Quite',
                    },
                    {
                        id: 28,
                        itemName: 'Frustrated',
                    },
                    {
                        id: 29,
                        itemName: 'Confused',
                    },
                ],
            },
            homeworkAssigned: {
                id: 1,
                title: 'Homework Assigned',
                items: [
                    {
                        id: 30,
                        itemName: 'Practice Test',
                    },
                    {
                        id: 31,
                        itemName: 'Concept Review',
                    },
                    {
                        id: 32,
                        itemName: 'English Section',
                    },
                    {
                        id: 33,
                        itemName: 'Math Section',
                    },
                    {
                        id: 34,
                        itemName: 'Science Section',
                    },
                    {
                        id: 35,
                        itemName: 'Reading Section',
                    },
                    {
                        id: 36,
                        itemName: 'All Section',
                    },
                ],
            },
            sessionProductive: {
                id: 1,
                title: 'Was the session Productive?',
                items: [
                    {
                        id: 37,
                        itemName: 'Yes',
                    },
                    {
                        id: 38,
                        itemName: 'No',
                    },
                    {
                        id: 39,
                        itemName: 'English Section',
                    },
                    {
                        id: 40,
                        itemName: 'Not sure',
                    },
                ],
            },
        }
    }
]



const TestDetails = () => {

    const [hover, setHover] = useState(false);
    const [itemId, setItemId] = useState(0);


    return (
        <div className='container' >
            {/* <div className='container1' >
                <Details />
            </div>

            <div className='container2' >
                <QuestionSection />
            </div> */}
            

{/* 
            <div className='card' >
                {Object.values(settingsData[0]).map((el) => (
                    <div className='card-container'>
                        <div className='card-heading fontFamily'>
                            {el.title}
                        </div> 
                        <div className='card-content'>
                            <SecondaryIconButton
                                title="TestDetails"
                                icon={<BsPlusLg size={15} />}
                            />
                        </div>

                       
                        <div className='flex-row' >
                            {el.items  && el.items.map((item) => (
                                <div 
                                className='flex-row'
                                onMouseEnter={e => {
                                    console.log("dsdsa");
                                    setItemId(item.id)
                                    setHover(true)
                                }}
                                onMouseLeave={e => {
                                    console.log("oooo");
                                    setItemId(item.id)
                                    setHover(false)
                                }}
                                >
                                    <div className='item-container'>
                                        <div>{item.itemName}</div>
                                    </div>
                                    
                                    <div className='cross-button'>
                                        {hover && item.id == itemId  ? <AiFillCloseSquare size={20}  /> :  null  }
                                    </div>
                            
                                </div>
                            ))}
                        </div>


                        <div>
                            <div className='card-heading margin-top fontFamily'>
                                {el.topicsCovered?.title}
                            </div> 
                            <div className='flex-row' >
                            {el.topicsCovered  && Object.values(el.topicsCovered.items).map((mmm) => (
                                    <div className='item-container'>{mmm.itemName}</div>
                            ))}
                            </div>
                        </div>


                        <div>
                            <div className='card-heading margin-top fontFamily'>
                                {el.studentMood?.title}
                            </div> 
                            <div className='flex-row' >
                            {el.studentMood  && Object.values(el.studentMood.items).map((mmm) => (
                                    <div className='item-container'>{mmm.itemName}</div>
                            ))}
                            </div>
                        </div>


                        <div>
                            <div className='card-heading margin-top fontFamily'>
                                {el.homeworkAssigned?.title}
                            </div> 
                            <div className='flex-row' >
                            {el.homeworkAssigned  && Object.values(el.homeworkAssigned.items).map((mmm) => (
                                    <div className='item-container'>{mmm.itemName}</div>
                            ))}
                            </div>
                        </div>

                        <div>
                            <div className='card-heading margin-top fontFamily'>
                                {el.sessionProductive?.title}
                            </div> 
                            <div className='flex-row' >
                            {el.sessionProductive  && Object.values(el.sessionProductive.items).map((mmm) => (
                                    <div className='item-container'>{mmm.itemName}</div>
                            ))}
                            </div>
                        </div>
                      
                    </div>
                ))}
            </div> */}

           
        </div>
    );
};

export default TestDetails;