import React, {useState, useEffect} from 'react';
import './assignedTestPage.scss'


import { icons } from '../../../../util/Icons';
import { Colors } from '../../../../util/Colors';

import { useLocation, useNavigate } from 'react-router-dom';


import Details from './Details'
import QuestionSection from './QuestionSection'
import ScreenLoader from '../../../../components/loader/ScreenLoader';
import { getTestDetails } from '../../../../api/ApiManager';


const AssignedTest = (props) => {

    const navigate = useNavigate();
    const location = useLocation();

    const [loader, setLoader] = useState(true);
    const [testDetails, setTestDetails] = useState([]);
    const [testId, setTestId] = useState((location.state.testId).toString());

    useEffect(() => {
        window.addEventListener("focus", onFocus);

        onFocus();

        return () => {
            window.removeEventListener("focus", onFocus);
        };

    }, [])
    

    const onFocus = () => {
        setLoader(true)
        viewTestDetails()
    };


    const viewTestDetails = () => {
        getTestDetails(testId)
        .then((resp) => {
            //console.log("resp ", resp);

            if(resp.status == '200'){
                setTestDetails(resp.data)
                setLoader(false)
            }
        })
        .catch((err) => {
            console.log("err ", err);
            setLoader(false)
        })
    }

    const [open, setOpen] = React.useState(false);
      const handleOpen = () => {
        console.log("open modal");
        setOpen(true);
    };
    
    const handleClose = () => {
        console.log("close modal");
        setOpen(false);
    }

    return (
        <div style={{width: '100%', display: 'flex', flexDirection: 'column',}} >
            {loader
            ?
            <div  className='loader_style' >
                <ScreenLoader />
            </div>
            :
                // <div style={{width: '100%', display: 'flex', justifyContent: 'space-between'}} >
                //     <div style={{width: '100%'}} >
                //         <Details props={testDetails} />
                //     </div>

                //     <div style={{width: '100%'}} >
                //         <QuestionSection props={testDetails} />
                //     </div>
                // </div>
                <div>dsadsadsa</div>
            }
        </div>
    );
};

export default AssignedTest;