import React,{useEffect, useState} from 'react';
import './testDetails.scss'

//import SecondaryIconButton from '../../../../components/button/SecondaryIconButton';

import {FiEdit}from "react-icons/fi"
import {AiFillCloseSquare}from "react-icons/ai"
import AddQuestion from '../../../../components/modal/addQuestion/AddQuestion';
import IconButton from '../../../../components/button/iconButton/IconButton';
import { Colors } from '../../../../util/Colors';
import { icons } from '../../../../util/Icons';
import EditQuestion from '../../../../components/modal/edit/EditQuestion/EditQuestion';


const sections = [
    {
        id: 1,
        sectionName: 'English',
    },
    {
        id: 2,
        sectionName: 'Math',
    },
    {
        id: 3,
        sectionName: 'Reading',
    },
    {
        id: 4,
        sectionName: 'Science',
    },
]


const testData = [
    {
        id: 1,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 2,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 3,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 4,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 1,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 2,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 3,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 4,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 1,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 2,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 3,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 4,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 1,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 2,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 3,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
    {
        id: 4,
        answer: 'A',
        concept: 'Trigonometry',
        strategy: 'Backsolving',
    },
]


const QuestionSection = () => {

    const [open, setOpen] = React.useState(false);
    const handleOpen = () => {
      console.log("open modal");
      setOpen(true);
  };
  
  const handleClose = () => {
      console.log("close modal");
      setOpen(false);
  }

    const [windowSize, setWindowSize] = useState(getWindowSize());

    useEffect(() => {
      function handleWindowResize() {
        setWindowSize(getWindowSize());
      }
  
      window.addEventListener('resize', handleWindowResize);
  
      return () => {
        window.removeEventListener('resize', handleWindowResize);
      };
    }, []);

    function getWindowSize() {
        const {innerWidth, innerHeight} = window;
        return {innerWidth, innerHeight};
    }


    return (
        <div 
        style={{}}
        //style={{width: windowSize.innerWidth/2-80}} 
        >
            <h1>Questions By Section</h1>

            {/* <div>
                <h2>Width: {windowSize.innerWidth}</h2>

                <h2>Height: {windowSize.innerHeight}</h2>
            </div> */}

            <div className='section-container' >
                {sections.map((item) => (
                    <div className='section'>{item.sectionName}</div>
                ))}
            </div>

            <div className='flex-end margin-top-bottom' >
                {/* <AddQuestion /> */}
                <div>
                    <IconButton
                        title="Add new question"
                        icon={icons.plus_circle} 
                        //onClick={handleOpen}
                        buttonStyle={{backgroundColor: Colors.PRIMARY, marginRight: 5}}
                    />    
                </div>
            </div>

            <div className='all-test-table-header' >
                <div className='section-heading' style={{alignItems: 'center', justifyContent:'center', display: 'flex', width: '45%'}} >Q No.</div>
                <div className='section-heading' style={{width: '30%'}} >Answer</div>
                <div className='section-heading' style={{width: '45%', marginLeft: 20, }} >Concept</div>
                <div className='section-heading' style={{width: '45%', marginLeft: 20, }} >Strategy</div>
                <div className='section-heading' style={{width: '25%'}} ></div>
            </div>

            <div className='all-test-table-content-container' >
                {testData.map((item, index) => (
                    <div className='all-test-table-content' style={{marginBottom: 10, border: "0.1px solid grey", backgroundColor: index % 2 == 0 ? 'white' : '#F5F7F9'}} >
                        <div className='all-test-sub-heading' style={{width: '30%', padding: 8}} >{item.id}</div>
                        <div className='all-test-sub-heading' style={{width: '20%', padding: 8}} >{item.answer}</div>
                        <div className='all-test-sub-heading' style={{width: '40%', padding: 8}} >{item.concept}</div>
                        <div className='all-test-sub-heading' style={{width: '40%', padding: 8}} >{item.strategy}</div>
                        <div className='all-test-sub-heading'>
                            <div onClick={handleOpen} className='edit'>
                                <FiEdit size={20} />
                            </div>
                        </div>
                        
                    </div>
                ))}
            </div>

            {open && <EditQuestion handleOpen={() => handleOpen()} handleClose={() => handleClose()} />}
        </div>
    );
};

export default QuestionSection;