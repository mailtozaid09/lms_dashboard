import React,{useEffect, useState} from 'react';
import './testDetails.scss'

//import SecondaryIconButton from '../../../components/button/SecondaryIconButton';

import {BsPlusLg}from "react-icons/bs"
import {AiFillCloseSquare}from "react-icons/ai"
import BackButton from '../../../../components/button/BackButton/BackButton';
import { useNavigate, useLocation } from 'react-router-dom';
import { getTestDetails } from '../../../../api/ApiManager';
import { showDate } from '../../../../util/CustomUtils';


const questionDetails = [
  {
      id: 1,
      correctAns: 'A',
      studentResp: 'B',
      accuracy: 'Incorrect',
      concept: 'Trigonometry',
      strategy: 'Backsolving',
      time: 21,
      solution: ''
  },
  {
      id: 2,
      correctAns: 'A',
      studentResp: 'B',
      accuracy: 'Incorrect',
      concept: 'Trigonometry',
      strategy: 'Backsolving',
      time: 21,
      solution: ''
  },
  {
      id: 3,
      correctAns: 'A',
      studentResp: 'A',
      accuracy: 'Correct',
      concept: 'Trigonometry',
      strategy: 'Backsolving',
      time: 21,
      solution: ''
  },
  {
      id: 4,
      correctAns: 'A',
      studentResp: 'B',
      accuracy: 'Incorrect',
      concept: 'Trigonometry',
      strategy: 'Backsolving',
      time: 21,
      solution: ''
  },
]

const testDetails =  {
  id: 1,
  testName: 'ACT O',
  studentName: 'Joseph Brown',
  dateAssigned: '06/24/22',
  duration: 'Regular',
  startedOn: '06/24/22',
  completedOn: '06/26/22',
};


const scoring = [
    {
        id: 1,
        subjects: [
            {
                name: 'Reading',
                marks: 1,
            },
            {
                name: 'Grammar',
                marks: 1,
            },
            {
                name: 'Math - N/Cal',
                marks: 1,
            },
            {
                name: 'Math - W/Cal',
                marks: 1,
            },
        ]
    },
    {
        id: 2,
        subjects: [
            {
                name: 'Reading',
                marks: 2,
            },
            {
                name: 'Grammar',
                marks: 2,
            },
            {
                name: 'Math - N/Cal',
                marks: 2,
            },
            {
                name: 'Math - W/Cal',
                marks: 2,
            },
        ]
    },
    {
        id: 3,
        subjects: [
            {
                name: 'Reading',
                marks: 3,
            },
            {
                name: 'Grammar',
                marks: 3,
            },
            {
                name: 'Math - N/Cal',
                marks: 3,
            },
            {
                name: 'Math - W/Cal',
                marks: 3,
            },
        ]
    },
    {
        id: 4,
        subjects: [
            {
                name: 'Reading',
                marks: 4,
            },
            {
                name: 'Grammar',
                marks: 4,
            },
            {
                name: 'Math - N/Cal',
                marks: 4,
            },
            {
                name: 'Math - W/Cal',
                marks: 4,
            },
        ]
    },
]
  

const Details = (props) => {

    const navigate = useNavigate();
   
    const [testDetails, setTestDetails] = useState(props.props);

    const [open, setOpen] = React.useState(false);
    
    const handleOpen = () => {
        console.log("open modal");
        setOpen(true);
    };
    
    const handleClose = () => {
        console.log("close modal");
        setOpen(false);
    }


    useEffect(() => {
    
    }, []);


    return (
        <div style={{width: '100%', display: 'flex', flexDirection: 'column',}} >

            <div className='back-button-container' >
                <BackButton
                    title="Back" 
                    onClick={() => {navigate(-1)}} 
                />
            </div>

            <div>
                <div className='test-details-title'>{testDetails?.testName}</div>
                <div className='details-row-item' >
                <div className='details-row-item-title'>Created</div>
                <div className='details-row-colon' >:</div>
                <div className='details-row-item-subtitle'>{showDate(testDetails?.auditFields?.createdAt)}</div>
                </div>

                <div className='details-row-item' >
                <div className='details-row-item-title'>Updated</div>
                <div className='details-row-colon' >:</div>
                <div className='details-row-item-subtitle'>{showDate(testDetails?.auditFields?.updatedAt)}</div>
                </div>

                <div className='details-row-item' >
                <div className='details-row-item-title'>Name</div>
                <div className='details-row-colon' >:</div>
                <div className='details-row-item-subtitle'>{testDetails?.name}</div>
                </div>

                <div className='details-row-item' >
                <div className='details-row-item-title'>Type</div>
                <div className='details-row-colon' >:</div>
                <div className='details-row-item-subtitle'>{testDetails?.type}</div>
                </div>
            </div>


            <div>
                <div className='details-row-item-subheading'>Sections</div>
                <div className='details-row-item' >
                    <div className='details-row-section'>Section</div>
                    <div className='details-row-time' >Time</div>
                    <div className='details-row-ques'>Total Questions</div>
                </div>

                {testDetails?.sections?.map((item) => (
                    <div className='details-row-item' >
                        <div className='details-row-section title-color'>{item.subject}</div>
                        <div className='details-row-time title-color' >{item.duration} mins</div>
                        <div className='details-row-ques title-color'>{item.totalQue}</div>
                    </div>
                ))}
            </div>

            <div>
                <div className='details-row-item-subheading'>Scoring</div>

                <div className='scoring-container' >
                    <div className='scoring-container-heading' >
                       
                        <div style={{flexDirection: 'column', display: 'flex', alignItems: 'center' }} >
                            <div className='scoring-heading' >Q No.</div>
                            {testDetails && testDetails?.scoring && testDetails?.scoring[0]?.marks.map((item) => (
                                <div>{item.queNo}</div>
                            ))}
                        </div>
                            
                        {testDetails?.scoring?.map((item) => (
                            <div>
                                <div className='scoring-heading' >{item.subject}</div>
                                <div style={{flexDirection: 'column', display: 'flex', alignItems: 'center' }} >
                                {item?.marks?.map((marks) => (
                                    <div>{marks.mark}</div>
                                ))}
                                </div>
                            </div>
                        ))}
                    </div>
                    
                </div>
            </div>
        </div>
    );
};

export default Details;