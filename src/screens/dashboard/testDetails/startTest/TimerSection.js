import React,{useEffect, useState} from 'react';
import IconButton from '../../../../components/button/iconButton/IconButton';
import { Colors } from '../../../../util/Colors';
import './testDetails.scss'



const TimerSection = () => {

    const [timerValue, setTimerValue] = useState('2:00:23');


    return (
        <div style={{display: 'flex', padding: 50, height: '95vh', alignItems: 'center', justifyContent: 'center', }} >
            <div style={{alignItems: 'center', flexDirection: 'column', display: 'flex'}} >
                <div style={{backgroundColor: Colors.PRIMARY, borderRadius: 10, padding: 15, width: 300, height: 180, flexDirection: 'column', alignItems: 'center', justifyContent: 'center', display: 'flex'}} >
                    <div style={{fontSize: 30, color: 'white'}} >Timer</div>
                    <div style={{fontSize: 50, color: 'white', fontWeight: 'bold'}}>{timerValue}</div>
                </div>
                <div style={{marginTop: 50, marginBottom: 120}} >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sagittis pulvinar quam pulvinar facilisis morbi vulputate suspendisse purus. Massa enim egestas donec integer amet. Purus tincidunt convallis integer ante felis placerat massa. Tincidunt purus proin porta aliquam tellus massa duis leo varius.
                    Mollis nullam sed maecenas aliquam dui consectetur. Orci, sit est proin dignissim duis vitae vitae interdum. Mauris donec non ultricies arcu. Viverra in maecenas ac quam sit condimentum. </div>
                <div>
                    <IconButton
                        title='Submit Section'
                        buttonStyle={{backgroundColor: Colors.PRIMARY, width: 300, height: 60}}
                    />
                </div>
                
            </div>

            {/* {open && <EditQuestion handleOpen={() => handleOpen()} handleClose={() => handleClose()} />} */}
        </div>
    );
};

export default TimerSection;