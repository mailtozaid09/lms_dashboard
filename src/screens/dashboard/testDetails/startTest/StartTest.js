import React from 'react';
import './startTest.scss'

import { icons } from '../../../../util/Icons';
import { Colors } from '../../../../util/Colors';

import { useNavigate } from 'react-router-dom';
import TimerSection from './TimerSection'

import Details from './Details'

const assignedTest = [
    {
        id: 1,
        testName: 'Test Name',
        studentName: 'Student Name',
        assignedOn: 'MM-DD-YY',
        duration: 'Regular',
        score: 'V720 M650 | C1370',
        status: 'Completed',
        dateAssigned: 'Date Assigned: MM-DD-YY',
    },
    {
        id: 2,
        testName: 'Test Name',
        studentName: 'Student Name',
        assignedOn: 'MM-DD-YY',
        duration: 'Regular',
        score: 'V720 M650 | C1370',
        status: 'inCompleted',
        dateAssigned: 'Date Assigned: MM-DD-YY',
    },
    {
        id: 3,
        testName: 'Test Name',
        studentName: 'Student Name',
        assignedOn: 'MM-DD-YY',
        duration: 'Regular',
        score: 'V720 M650 | C1370',
        status: 'inProgress',
        dateAssigned: 'Date Assigned: MM-DD-YY',
    },
    {
        id: 4,
        testName: 'Test Name',
        studentName: 'Student Name',
        assignedOn: 'MM-DD-YY',
        duration: 'Regular',
        score: 'V720 M650 | C1370',
        status: 'Completed',
        dateAssigned: 'Date Assigned: MM-DD-YY',
    },
]
const AssignedTest = () => {

    const navigate = useNavigate();

    const [open, setOpen] = React.useState(false);
      const handleOpen = () => {
        console.log("open modal");
        setOpen(true);
    };
    
    const handleClose = () => {
        console.log("close modal");
        setOpen(false);
    }

    return (
        <div style={{width: '100%', display: 'flex', justifyContent: 'space-between'}} >
              
            <div style={{width: '100%'}} >
                <Details />
            </div>

            <div style={{width: '100%'}} >
                <TimerSection />
            </div>
        
        </div>
    );
};

export default AssignedTest;