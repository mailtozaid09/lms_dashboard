import React,{useEffect, useState} from 'react';
import './testDetails.scss'

//import SecondaryIconButton from '../../../components/button/SecondaryIconButton';

import {BsPlusLg}from "react-icons/bs"
import {AiFillCloseSquare}from "react-icons/ai"
import BackButton from '../../../../components/button/BackButton/BackButton';
import { useNavigate } from 'react-router-dom';
import { Colors } from '../../../../util/Colors';
import IconButton from '../../../../components/button/iconButton/IconButton';
import { TextField } from '@mui/material';


const testDetails =  {
    id: 1,
    testName: 'ACT O',
    studentName: 'Joseph Brown',
    dateAssigned: '06/24/22',
    duration: 'Regular',
    startedOn: '06/24/22',
    completedOn: '06/26/22',
};


const subjectDetails =  [
    { 
        id: 1,
        subjectName: 'English',
        obtainMarks: 55,
        totalMarks: 75,
        questions: [
            {
                id: 1,
                quesType: 'mcq',
                incorrectAns: 5,
                totalAns: 17,
            },
            {
                id: 2,
                quesType: 'mcq',
                incorrectAns: 9,
                totalAns: 17,
            },
            {
                id: 3,
                quesType: 'input',
                incorrectAns: 4,
                totalAns: 7,
            },
            {
                id: 4,
                quesType: 'input',
                incorrectAns: 5,
                totalAns: 17,
            },
            {
                id: 5,
                quesType: 'mcq',
                incorrectAns: 5,
                totalAns: 17,
            },
        ]  
    },
    { 
        id: 2,
        subjectName: 'Mathematics',
        obtainMarks: 42,
        totalMarks: 80,
        questions: [
            {
                id: 1,
                quesType: 'mcq',
                incorrectAns: 5,
                totalAns: 17,
            },
            {
                id: 2,
                quesType: 'input',
                incorrectAns: 9,
                totalAns: 17,
            },
            {
                id: 3,
                quesType: 'input',
                incorrectAns: 4,
                totalAns: 7,
            },
            {
                id: 4,
                quesType: 'input',
                incorrectAns: 5,
                totalAns: 17,
            },
            {
                id: 5,
                quesType: 'mcq',
                incorrectAns: 5,
                totalAns: 17,
            },
        ]  
    },
    { 
        id: 3,
        subjectName: 'Science',
        obtainMarks: 55,
        totalMarks: 75,
        questions: [
            {
                id: 1,
                quesType: 'input',
                incorrectAns: 5,
                totalAns: 17,
            },
            {
                id: 2,
                quesType: 'mcq',
                incorrectAns: 9,
                totalAns: 17,
            },
            {
                id: 3,
                quesType: 'input',
                incorrectAns: 4,
                totalAns: 7,
            },
            {
                id: 4,
                quesType: 'input',
                incorrectAns: 5,
                totalAns: 17,
            },
            {
                id: 5,
                quesType: 'mcq',
                incorrectAns: 5,
                totalAns: 17,
            },
        ]  
    },
    { 
        id: 4,
        subjectName: 'Reading',
        obtainMarks: 42,
        totalMarks: 80,
        questions: [
            {
                id: 1,
                quesType: 'mcq',
                incorrectAns: 5,
                totalAns: 17,
            },
            {
                id: 2,
                quesType: 'mcq',
                incorrectAns: 9,
                totalAns: 17,
            },
            {
                id: 3,
                quesType: 'mcq',
                incorrectAns: 4,
                totalAns: 7,
            },
            {
                id: 4,
                quesType: 'mcq',
                incorrectAns: 5,
                totalAns: 17,
            },
            {
                id: 5,
                quesType: 'mcq',
                incorrectAns: 5,
                totalAns: 17,
            },
        ]  
    },
];


const options = [
    {
        id: 1,
        option: 'A',
    },
    {
        id: 2,
        option: 'B',
    },
    {
        id: 3,
        option: 'C',
    },
    {
        id: 4,
        option: 'D',
    },
]

const Details = () => {

    const navigate = useNavigate();

    const [currentSub, setCurrentSub] = useState(0);

    const [answers, setAnswers] = useState([]);

    const [open, setOpen] = React.useState(false);
      const handleOpen = () => {
        console.log("open modal");
        setOpen(true);
    };
    
    const handleClose = () => {
        console.log("close modal");
        setOpen(false);
    }


    const [windowSize, setWindowSize] = useState(getWindowSize());

    useEffect(() => {
      function handleWindowResize() {
        setWindowSize(getWindowSize());
      }
  
      window.addEventListener('resize', handleWindowResize);
  
      return () => {
        window.removeEventListener('resize', handleWindowResize);
      };
    }, []);

    function getWindowSize() {
        const {innerWidth, innerHeight} = window;
        return {innerWidth, innerHeight};
    }


    const addOptionAnswer = (quesId, item) => {
        console.log("addOptionAnswer");
        console.log(quesId, item.id, item.option);
    }

    return (
        <div style={{width: '100%', display: 'flex', flexDirection: 'column',}} >

            <div className='back-button-container' >
                <BackButton
                    title="Back" 
                    onClick={() => {navigate(-1)}} 
                />
            </div>


            <div className='test-details-title'>{testDetails.testName}</div>

            <div className='fd-r d-f ai-c jc-sb full-width ' style={{width: 550}} >
                <div>
                    <div className='details-row-item' >
                    <div className='details-row-item-title' style={{width: 140}}>Student’s Name</div>
                    <div className='details-row-colon' >:</div>
                    <div className='details-row-item-subtitle'  style={{width: 140}}>Joseph Brown</div>
                    </div>

                    <div className='details-row-item' >
                    <div className='details-row-item-title'  style={{width: 140}}>Date Assigned</div>
                    <div className='details-row-colon' >:</div>
                    <div className='details-row-item-subtitle'  style={{width: 140}}>24/06/22</div>
                    </div>

                    <div className='details-row-item' >
                    <div className='details-row-item-title' style={{width: 140}}>Duration</div>
                    <div className='details-row-colon' >:</div>
                    <div className='details-row-item-subtitle' style={{width: 140}}>175 minutes</div>
                    </div>
                </div>
                <div>
                    <div className='details-row-item' >
                    <div className='details-row-item-title'>Started on</div>
                    <div className='details-row-colon' >:</div>
                    <div className='details-row-item-subtitle'>24/06/22</div>
                    </div>

                    <div className='details-row-item' >
                    <div className='details-row-item-title'>Cretaed on</div>
                    <div className='details-row-colon' >:</div>
                    <div className='details-row-item-subtitle'>24/06/22</div>
                    </div>

                    <div className='details-row-item' >
                    <div className='details-row-item-title'>Duration</div>
                    <div className='details-row-colon' >:</div>
                    <div className='details-row-item-subtitle'>175 minutes</div>
                    </div>
                </div>
              
            </div>


            <div className='subject-main-container' >
                {subjectDetails.map((subject, subIdx) => (
                    <div onClick={() => setCurrentSub(subIdx)} className='subject-container' style={{color: currentSub == subIdx ? Colors.WHITE : Colors.DARKGREY, backgroundColor: currentSub == subIdx ? Colors.PRIMARY : Colors.SECONDARY }} >{subject.subjectName}</div>
                ))}
            </div>

            <div className='' >
                {subjectDetails && subjectDetails.map((subject, subIdx) => (
                    <div>
                        {currentSub == subIdx && 
                        <div className='' >
                            {subject && subject.questions.map((ques) => (
                                    <div className='fd-r jc-sb ai-c d-f full-width' style={{width: 600, backgroundColor: 'white', borderRadius: 10, padding: 10, height: 100, paddingLeft: 20, paddingRight: 20, marginBottom: 10}} >
                                       <div className='fd-r ai-c d-f' >
                                            <div className='' style={{width: 30}} >{ques.id}</div>

                                            <div className=''>
                                                {ques.quesType == 'mcq'
                                                ?
                                                    <div className='fd-r ai-c d-f jc-se full-width' style={{}} >
                                                        {options.map((item) => (
                                                            <div onClick={() => addOptionAnswer(ques.id, item)} className='option_container' >
                                                                <div>{item.option}</div>
                                                            </div>
                                                        ))}
                                                    </div>
                                                :
                                                    <div>
                                                        <TextField style={{width: 400}} id="outlined-basic" label="Answer" variant='outlined' />
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                        <div className='' >
                                            <IconButton
                                                title='Review'
                                                buttonStyle={{backgroundColor: Colors.MUSTURD, width: 100}}
                                            />
                                        </div>
                                    </div>
                                ))}
                        </div>}
                    </div>
                ))}
            </div>

        </div>
    );
};

export default Details;