import React from 'react';
import './invoice.scss';

import Input from '../../../components/input/Input'
import IconButton from '../../../components/button/iconButton/IconButton';
import { Colors } from '../../../util/Colors';


const invoiceData = [
    {
        id: 1,
        tutorName: 'Joseph Brown',
        date: '27/06/22',
        hourlyRate: '$ 40',
        duration: '45 mins',
        totalAmt: '$ 30',
    },
    {
        id: 2,
        tutorName: 'Joseph Brown',
        date: '27/06/22',
        hourlyRate: '$ 40',
        duration: '45 mins',
        totalAmt: '$ 30',
    },
    {
        id: 3,
        tutorName: 'Joseph Brown',
        date: '27/06/22',
        hourlyRate: '$ 40',
        duration: '45 mins',
        totalAmt: '$ 30',
    },
    {
        id: 4,
        tutorName: 'Joseph Brown',
        date: '27/06/22',
        hourlyRate: '$ 40',
        duration: '45 mins',
        totalAmt: '$ 30',
    },
]

const Invoice = () => {
    return (
        <div>
            <h1>Invoice</h1>
            <h3>1st August - 15th August</h3>

            <div className='invoice-container'>
                <div className='invoice-details-container'>
                    <div>
                        <div className='invoice-details-row'>
                            <div className='invoice-details-left'>Parent Name</div>
                            <div className='invoice-details-right'>: Phill Brown</div>
                        </div>
                        <div className='invoice-details-row'>
                            <div className='invoice-details-left'>Email</div>
                            <div className='invoice-details-right'>: philbrn78@gmail.com</div>
                        </div>
                        <div className='invoice-details-row'>
                            <div className='invoice-details-left'>Number</div>
                            <div className='invoice-details-right'>: +1 1234567890</div>
                        </div>
                    </div>
                    <div>
                        <div className='invoice-details-row'>
                            <div className='invoice-details-left'>Student Name</div>
                            <div className='invoice-details-right'>: Joseph Brown</div>
                        </div>
                        <div className='invoice-details-row'>
                            <div className='invoice-details-left'>Email</div>
                            <div className='invoice-details-right'>: Josephbrn78@gmail.com</div>
                        </div>
                        <div className='invoice-details-row'>
                            <div className='invoice-details-left'>Number</div>
                            <div className='invoice-details-right'>: +1 1234567890</div>
                        </div>
                    </div>
                </div>

                <div className='invoice-table-header' >
                    <div className='all-test-heading' style={{width: '100%'}} >S No.</div>
                    <div className='all-test-heading' style={{width: '100%'}} >Tutor Name</div>
                    <div className='all-test-heading' style={{width: '100%'}} >Date</div>
                    <div className='all-test-heading' style={{width: '100%'}} >Hourly Rate</div>
                    <div className='all-test-heading' style={{width: '100%'}} >Duration</div>
                    <div className='all-test-heading' style={{width: '100%'}} >Total Amount</div>
                </div>

                <div className='all-test-table-content-container' >
                    {invoiceData.map((item, index) => (
                        <div className='all-test-table-content' style={{marginBottom: 10, border: "0.1px solid grey", backgroundColor: index % 2 == 0 ? 'white' : '#F5F7F9'}} >
                            <div className='all-test-sub-heading' style={{width: '100%', padding: 8}} >{item.id}</div>
                            <div className='all-test-sub-heading' style={{width: '100%', padding: 8}} >{item.tutorName}</div>
                            <div className='all-test-sub-heading' style={{width: '100%', padding: 8}} >{item.date}</div>
                            <div className='all-test-sub-heading' style={{width: '100%', padding: 8}} >{item.hourlyRate}</div>
                            <div className='all-test-sub-heading' style={{width: '100%', padding: 8}} >{item.duration}</div>
                            <div className='all-test-sub-heading' style={{width: '100%', padding: 8}} >{item.totalAmt}</div>
                        </div>
                    ))}

                    <div className='invoice-details-container'>
                        <div className='additional-notes-row'>
                            <div>
                                <div>Additional Notes</div>
                            </div>
                            <div>
                                <Input />
                            </div>
                        </div>
                        <div className='additional-notes-row'>
                            <div>
                                <div>Total Amount Due:   $180</div>
                            </div>
                            <div>
                                <IconButton
                                    title="Send Invoice"
                                    //onClick={handleDeleteOpen}
                                    buttonStyle={{marginTop: 20, backgroundColor: Colors.PRIMARY,  marginLeft: 5}}
                                />   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Invoice;