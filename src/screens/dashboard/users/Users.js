import React from 'react';

import Dropdown from '../../../components/accordian/Dropdown';

import Button from '../../../components/button/Button';
import Header from '../../../components/header/Header';
import AddNewTest from '../../../components/modal/AddNewTest/AddNewTest';
import { Colors } from '../../../util/Colors';
import './users.scss'

import AddNewUserModal from '../../../components/modal/addNewUser/AddNewUserModal'
import { icons } from '../../../util/Icons';
import Searchbar from '../../../components/search/Searchbar';
import DropDown from '../../../components/dropdown/DropDown';
import { useNavigate } from 'react-router-dom';

const userData = [
    {
        id: 1,
        name: 'Ram Kumar',
        email: 'RamKumar@gmail',
        number: '+919876543210',
        classType: 'Class 8',
        userType: 'Student',
        leadStatus: 'Lead Status',
        assignedTutor: 'Tutor Name',
        services: 'Services',
    },
    {
        id: 2,
        name: 'Shivam Singh',
        email: 'ShivamSingh@gmail',
        number: '+919876543210',
        classType: 'Class 9',
        userType: 'Student',
        leadStatus: 'Lead Status',
        assignedTutor: 'Tutor Name',
        services: 'Services',
    },
    {
        id: 3,
        name: 'Amit Sinha',
        email: 'AmitSinha@gmail',
        number: '+919876543210',
        classType: 'Class 10',
        userType: 'Student',
        leadStatus: 'Lead Status',
        assignedTutor: 'Tutor Name',
        services: 'Services',
    },
    {
        id: 4,
        name: 'Adarsh Sharma',
        email: 'AdarshSharma@gmail',
        number: '+919876543210',
        classType: 'Class 8',
        userType: 'Teacher',
        leadStatus: 'Lead Status',
        assignedTutor: 'Tutor Name',
        services: 'Services',
    },
]
const Users = () => {

    const navigate = useNavigate();

    const [open, setOpen] = React.useState(false);
      const handleOpen = () => {
        console.log("open modal");
        setOpen(true);
    };
    
    const handleClose = () => {
        console.log("close modal");
        setOpen(false);
    }

    const navigateToScreen = (type) => {
        console.log("navigateToScreen => ", type);

        if(type == 'Parent'){
            console.log("parentProfile");
            navigate('/parentProfile')
        } else if(type == 'Student'){
            console.log("studentProfile");
            navigate('/studentProfile')
        } else if(type == 'Tutor'){
            console.log("tutorProfile");
            navigate('/tutorProfile')
        }
    }


    

    return (
        <div style={{width: '100%', display: 'flex', flexDirection: 'column',}} >

            <Header 
                title="All Users" 
                buttonTitle="Add new user" 
                buttonIcon={icons.plus_circle} 
                onClick={handleOpen}
            />


            <div className='mt-20 mb-20 d-f fd-r ai-c jc-sb full-width' >
                <Searchbar
                    label="Type Name"
                />

                {/* <DropDown 
                    style={{width: 200}}
                    label="User Type"
                    data={[{value: 'Student', title: 'Student'}, {value: 'Parent', title: 'Parent'}, {value: 'Tutor', title: 'Tutor'}]}
                />

                <DropDown 
                    style={{width: 200}}
                    label="Lead Status"
                    data={[{value: 'Lead Status', title: 'Lead Status'}, {value: 'Lead Status', title: 'Lead Status'}, {value: 'Lead Status', title: 'Lead Status'}]}
                />

                <DropDown 
                    style={{width: 200}}
                    label="Services"
                    data={[{value: 'ACT/SAT', title: 'ACT/SAT'}, {value: 'ACT/SAT', title: 'ACT/SAT'}, {value: 'ACT/SAT', title: 'ACT/SAT'}]}
                />

                <DropDown 
                    style={{width: 200}}
                    label="Tutor Status"
                    data={[{value: 'Tutor Status', title: 'Tutor Status'}, {value: 'Tutor Status', title: 'Tutor Status'}, {value: 'Tutor Status', title: 'Tutor Status'}]}
                /> */}



            </div>

            
            <div className='user-table-header' >
                <div className='user-heading' style={{width: '100%'}} >Full Name</div>
                <div className='user-heading' style={{width: '100%'}} >User Type</div>
                <div className='user-heading' style={{width: '150%'}} >Email</div>
                <div className='user-heading' style={{width: '100%'}} >Phone</div>
                <div className='user-heading' style={{width: '100%'}} >Class</div>
                {/*
                <div className='user-heading' style={{width: '100%'}} >Lead Status</div>
                <div className='user-heading' style={{width: '100%'}} >Tutor Status</div>
                <div className='user-heading' style={{width: '100%'}} >Service(s)</div> */}
            </div>

            <div className='user-table-content-container' >
                {userData.map((item, index) => (
                    <div className='user-table-content' style={{marginBottom: 10, border: "0.1px solid grey", backgroundColor: index % 2 == 0 ? 'white' : '#F5F7F9'}} >
                        <div className='user-sub-heading title_hover' onClick={() => {navigateToScreen(item.userType)}} style={{width: '100%', color: '#2A6CFB'}} >{item.name}</div>
                        <div className='user-sub-heading' style={{width: '100%'}} >{item.userType}</div>
                        <div className='user-sub-heading' style={{width: '150%'}} >{item.email}</div>
                        <div className='user-sub-heading' style={{width: '100%'}} >{item.number}</div>
                        <div className='user-sub-heading' style={{width: '100%'}} >{item.classType}</div>
                        {/* 
                        <div className='user-sub-heading' style={{width: '100%'}} >{item.leadStatus}</div>
                        <div className='user-sub-heading' style={{width: '100%'}} >{item.assignedTutor}</div>
                        <div className='user-sub-heading' style={{width: '100%'}} >{item.services}</div> */}
                    </div>
                ))}
            </div>

            {open && <AddNewUserModal handleOpen={() => handleOpen()} handleClose={() => handleClose()} />}
                    
            
        </div>
    );
};

export default Users;